import UIKit
import AuthenticationServices


class BaseVC: UIViewController {

    @IBOutlet weak var lbl_NavigationTitle : UILabel!
    @IBOutlet weak var constraintNavigationHeight : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
//        backgroundImage.image = UIImage(named: "appBG")
//        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
//        self.view.insertSubview(backgroundImage, at: 0)
        
        if(self.constraintNavigationHeight != nil){
            self.constraintNavigationHeight?.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
        }
        
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func actionNavigationBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   

}

@available(iOS 13.0, *)
extension BaseVC: ASAuthorizationControllerPresentationContextProviding {
    //For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @IBAction func actionHome(_ sender: UIButton){
        let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    @IBAction func actionPlus(_ sender: UIButton){
        self.view.endEditing(true)
        let objVC : SetupDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: SetupDeviceVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: false)
    }
}


extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
