//
//  AppDelegate.swift
//  Impact
//
//  Created by Apple on 30/04/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    
    var window: UIWindow?
    var navigationcontroller:UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        //Push notification
        self.setUpPushnotificationSettings(application: application)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        setRootViewController()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Impact")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}


@available(iOS 13.0, *)
extension AppDelegate {
    
    class func sharedApp() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    private func setRootViewController() {
        guard let _ = ApplicationPreference.getAccessToken() else {
            navigationLoginVC()
            return
        };  navigationHomeVC()
    }
    
    private func navigationHomeVC() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        guard let homePage = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC else { return }
        //        navigationcontroller = mainStoryboard.instantiateInitialViewController() as? UINavigationController
        //        navigationcontroller?.viewControllers = [homePage]
        self.window?.rootViewController = UINavigationController(rootViewController: homePage)
        self.window?.makeKeyAndVisible()
    }
    
    func navigationLoginVC() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "OnBoard", bundle: nil)
        guard let loginPage = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else { return }
        //        navigationcontroller = mainStoryboard.instantiateInitialViewController() as? UINavigationController
        //        navigationcontroller?.viewControllers = [loginPage]
        self.window?.rootViewController = UINavigationController(rootViewController: loginPage)
        self.window?.makeKeyAndVisible()
    }
    
    func loginVC () {
        
        let story = UIStoryboard(name: "OnBoard", bundle:nil)
        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        UIApplication.shared.windows.first?.rootViewController = vc
        //  UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
}


//MARK: - ↓↓ Push Notification ↓↓
@available(iOS 13.0, *)
extension AppDelegate : MessagingDelegate {
    
    
    func setUpPushnotificationSettings(application: UIApplication)  {
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }

    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        ApplicationPreference.saveFCMToken(appToken: fcmToken ?? "")
        
    }
    
    // [START receive_message]
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//        // If you are receiving a notification message while your app is in the background,
//        // this callback will not be fired till the user taps on the notification launching the application.
//        // TODO: Handle data of notification
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//
//        // Print full message.
//        print("didReceiveRemoteNotification userInfo : \(userInfo)")
//
//    }
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        // If you are receiving a notification message while your app is in the background,
//        // this callback will not be fired till the user taps on the notification launching the application.
//        // TODO: Handle data of notification
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//
//        // Print full message.
//        print("didReceiveRemoteNotification userInfo fetchCompletionHandler : \(userInfo)")
//
//        completionHandler(UIBackgroundFetchResult.newData)
//
//    }
    
    
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
        
        //get FCM token
        if let token = Messaging.messaging().fcmToken {
            print("FCM token: \(token)")
            ApplicationPreference.saveFCMToken(appToken: token)
            self.notificationSubscribe()
        }
        
    }
    
    func notificationSubscribe () {
        
        let param = ["fcmToken":ApplicationPreference.getFCMToken(),
                     "application":"TEV"]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.subscribe, andParam: param as [String : Any], showHud: true, changeBaseUrl: false) { (responseDict) in
            
            Helper.UI {
//                
//                if let successMessage = responseDict["successMessage"] as? String {
//                    //Helper.sharedInstance.showToast(isError: false, title: successMessage)
//                }else if let errorMessage = responseDict["errorMessage"] as? String {
//                   // Helper.sharedInstance.showToast(isError: false, title: errorMessage)
//                }else {
//                    //Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
//                }
            }
        }
    }
}
