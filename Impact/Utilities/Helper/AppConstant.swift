//
//  AppConstant.swift
//

import UIKit

class AppConstant: NSObject {
    
    internal static let userDefaultAppLaunchTime = "LaunchTime"
    internal static let userDefaultAppTerminationTime = "TerminationTime"
    internal static let userDefaultAppActivityTime = "AppActivityTime"
    internal static let userDefaultAppInstalledDateTime = "AppInstalledDateTime"
    
    internal static let userDefaultAppTokenKey = "user_token"
    internal static let userDefaultAuthKey = "authToken"
    internal static let userDefaultUserNameKey = "name"
    internal static let userDefaultUserAddressKey = "userAddress"
    internal static let userDefaultUserQRCodeKey = "userQR"
    internal static let userDefaultUserWalletBalKey = "userWallet"
    internal static let userDefaultUserFirstNameKey = "userfirstname"
    internal static let userDefaultUserLastNameKey = "userlastname"
    internal static let userDefaultUserFullNameKey = "full_name"
    internal static let userDefaultUserProfilePicKey = "userProfilePic"
    internal static let userDefaultUserPhoneKey = "userphone"
    internal static let userDefaultUserIdKey = "user_id"
    internal static let userDefaultUserRollKey = "userRoll"
    internal static let userDefaultProfileCompleteKey = "profile_complete"
    internal static let userDefaultCurrentDriverIdKey = "driver_id"
    internal static let userDefaultCurrentDriverStatusKey = "driver_status"
    internal static let userDefaultUserSocialLogin = "userDefaultUserSocialLogin"
    internal static let userDefaultUserLoginInfoKey = "userLoginInfo"
    internal static let userDefaultIsEmail = "email"
    internal static let userDefaultAPNSTokenKey = "APNS_TOKEN"
    internal static let userDefaultFCMTokenKey = "FCM_TOKEN"
    internal static let kNotificationCount = "NotificationCount"
    internal static let appLaunchFirstTime = "appLaunchFirstTime"
    internal static let userCurrentLatitude = "latitude"
    internal static let userCurrentLongitude = "longitude"
    internal static let userAcType = "UpdateAccountType"
    internal static let userDefaultUserClubInfoKey = "ClubInfo"
    internal static let DEFAULT_REST_ERROR_CODE     = "default_rest_error_code"
    internal static let DEFAULT_REST_ERROR_MESSAGE  = "Unable to process your"
    internal static let kChatID                      = "group_id"
    internal static let userDefaultUserEmailKey = "email"
    internal static let userDefaultUsercompleteProfileeKey = "UsercompleteProfile"
    internal static let userDefaultLatitudeKey = "latitude"
    internal static let userDefaultLongitudeKey = "longitude"
    internal static let userDefaultAccessToken = "accessToken"
    internal static let userDefaultSignalRNegotiateUrl = "signalRNegotiateUrl"
    internal static let userDefaultStopSequenceNumber = "stopSequenceNumber"
    internal static let userDefaultOrgId = "orgId"
    internal static let userDefaultUuidString = "uuidString"
    internal static let userDefaultSwitchOnOff = "userDefaultSwitchOnOff"
    
    

    internal static let LIMIT_LOWER_PASSWORD = 8
}

extension AppConstant {
    //MARK:-  All StoryBoard
    struct Storyboard {
        internal static let onBoard = "OnBoard"
        internal static let home = "Home"
        internal static let helpCenter = "HelpCenter"
        internal static let account = "Account"
        internal static let devices = "Devices"
        internal static let setupDevice = "SetupDevice"
    }
}
