import Foundation
import UIKit

class ApplicationPreference{
    
    fileprivate static let defaults = UserDefaults.standard
    
    //////////////// Remove all info //////
    class func clearAllData(){
        self.removeLoginInfo()
        defaults.removeObject(forKey: AppConstant.userDefaultAppTokenKey)
        defaults.removeObject(forKey: AppConstant.userDefaultUserLoginInfoKey)
        defaults.removeObject(forKey: AppConstant.userDefaultUserIdKey)
        defaults.removeObject(forKey: AppConstant.userDefaultUserRollKey)
        defaults.removeObject(forKey: AppConstant.userAcType)
        defaults.removeObject(forKey: AppConstant.kChatID)
        defaults.removeObject(forKey: AppConstant.userDefaultAppLaunchTime)
        defaults.removeObject(forKey: AppConstant.userDefaultAppTerminationTime)
        defaults.removeObject(forKey: AppConstant.userDefaultAppActivityTime)
        defaults.removeObject(forKey: AppConstant.userDefaultUsercompleteProfileeKey)
        
        defaults.synchronize()
    }
    
    class func getDeviceID() -> String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
    
    //////////////// GET & SAVE FCM TOKEN //////
    
    class func saveFCMToken(appToken: String){
        defaults.set(appToken, forKey: AppConstant.userDefaultFCMTokenKey)
        defaults.synchronize()
    }
  
    class func getFCMToken()->String?{
        let appToken:String?
        appToken = defaults.object(forKey: AppConstant.userDefaultFCMTokenKey) as? String
        return appToken ?? nil
    }
    
    class func removeFCMToken(){
        defaults.removeObject(forKey: AppConstant.userDefaultFCMTokenKey)
        defaults.synchronize()
    }
    
    //////////////// GET & SAVE APP TOKEN //////
    
    class func saveUserEmail(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserEmailKey)
        defaults.synchronize()
    }
    
    class func getUserEmail()->String?{
        let userEmail:String?
        userEmail = defaults.object(forKey: AppConstant.userDefaultUserEmailKey) as? String
        return userEmail ?? nil
    }
    
    class func saveUuidString(uuidString: String){
        defaults.set(uuidString, forKey: AppConstant.userDefaultUuidString)
        defaults.synchronize()
    }
    
    class func getUuidString()->String?{
        let uuidString:String?
        uuidString = defaults.object(forKey: AppConstant.userDefaultUuidString) as? String
        return uuidString ?? nil
    }
    
    class func saveOrgId(orgId: String){
        defaults.set(orgId, forKey: AppConstant.userDefaultOrgId)
        defaults.synchronize()
    }
    
    class func getOrgId()->String?{
        let orgId:String?
        orgId = defaults.object(forKey: AppConstant.userDefaultOrgId) as? String
        return orgId ?? nil
    }
    
    class func saveAppToken(appToken: String){
        defaults.set(appToken, forKey: AppConstant.userDefaultAppTokenKey)
        defaults.synchronize()
    }
    
    class func getAppToken()->String?{
        let appToken:String?
        appToken = defaults.object(forKey: AppConstant.userDefaultAppTokenKey) as? String
        return appToken ?? nil
    }
    
   
    class func saveUserId(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserIdKey)
        defaults.synchronize()
    }
    
    class func saveUserRoll(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserRollKey)
        defaults.synchronize()
    }
    
    class func getUserId()->String?{
        let userId:String?
        userId = defaults.object(forKey: AppConstant.userDefaultUserIdKey) as? String
        return userId ?? nil
    }
    
    class func getUserRoll()->String?{
        let userRoll:String?
        userRoll = defaults.object(forKey: AppConstant.userDefaultUserRollKey) as? String
        return userRoll ?? nil
    }
    
    class func saveUserName(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserNameKey)
        defaults.synchronize()
    }
    
    class func getUserName()->String?{
        let userId:String?
        userId = defaults.object(forKey: AppConstant.userDefaultUserNameKey) as? String
        return userId ?? nil
    }
    
    class func saveUsercompleteProfile(userInfo: Bool){
        defaults.set(userInfo, forKey: AppConstant.userDefaultUsercompleteProfileeKey)
        defaults.synchronize()
    }
    
    class func getUsercompleteProfile()->Bool?{
        let userInfo:Bool?
        userInfo = defaults.object(forKey: AppConstant.userDefaultUsercompleteProfileeKey) as? Bool
        return userInfo ?? false
    }
    
    class func removeUserID(){
        defaults.removeObject(forKey: AppConstant.userDefaultUserIdKey)
        defaults.synchronize()
    }
    
    class func saveUserSelectedCategory(userId: String){
        defaults.set(userId, forKey: AppConstant.userDefaultUserNameKey)
        defaults.synchronize()
    }
    
    class func getUserSelectedCategory()->String?{
        let userId:String?
        userId = defaults.object(forKey: AppConstant.userDefaultUserNameKey) as? String
        return userId ?? nil
    }
    
    class func removeUserSelectedCategory(){
        defaults.removeObject(forKey: AppConstant.userDefaultUserIdKey)
        defaults.synchronize()
    }
    
    class func getUserLatitude()->String?{
        let latitude:String?
        latitude = defaults.object(forKey: AppConstant.userDefaultLatitudeKey) as? String
        return latitude ?? nil
    }
    
    class func saveUserLatitude(latitude: String){
        defaults.set(latitude, forKey: AppConstant.userDefaultLongitudeKey)
        defaults.synchronize()
    }
    
    class func getUserLongitude()->String?{
        let longitude:String?
        longitude = defaults.object(forKey: AppConstant.userDefaultLongitudeKey) as? String
        return longitude ?? nil
    }
    
   
    class func saveUserLongitude(longitude: String){
        defaults.set(longitude, forKey: AppConstant.userDefaultLatitudeKey)
        defaults.synchronize()
    }
    
    class func getAccessToken()->String?{
        let accessToken:String?
        accessToken = defaults.object(forKey: AppConstant.userDefaultAccessToken) as? String
        return accessToken ?? nil
    }
    
   
    class func saveAccessToken(accessToken: String){
        defaults.set(accessToken, forKey: AppConstant.userDefaultAccessToken)
        defaults.synchronize()
    }
    
    class func removeAccessToken(){
        defaults.removeObject(forKey: AppConstant.userDefaultAccessToken)
        defaults.synchronize()
    }

    ///////////////// GET & SAVE Login Info //////
    class func saveLoginInfo(loginInfo: String){
        defaults.set(loginInfo, forKey: AppConstant.userDefaultUserLoginInfoKey)
        defaults.synchronize()
    }
    
    class func getLoginInfo()->String?{
        let loginInfo:String?
        loginInfo = defaults.object(forKey: AppConstant.userDefaultUserLoginInfoKey) as? String
        return loginInfo ?? nil
    }
    
    class func removeLoginInfo(){
        defaults.removeObject(forKey: AppConstant.userDefaultUserLoginInfoKey)
        defaults.synchronize()
    }
    
    
    class func removeIsSocialLogin(){
        defaults.removeObject(forKey: AppConstant.userDefaultUserSocialLogin)
        defaults.synchronize()
    }

    
    ///////////////// GET & SAVE App launch first Info //////
    class func saveAppLaunchInfo(appLaunchInfo: String){
        defaults.set(appLaunchInfo, forKey: AppConstant.appLaunchFirstTime)
        defaults.synchronize()
    }
    
    class func getAppLaunchInfo()->String?{
        let appLaunchInfo:String?
        appLaunchInfo = defaults.object(forKey: AppConstant.appLaunchFirstTime) as? String
        return appLaunchInfo ?? nil
    }
    
    class func removeAppLaunchInfo(){
        defaults.removeObject(forKey: AppConstant.appLaunchFirstTime)
        defaults.synchronize()
    }
    
    ///////////////// GET & SAVE User Current Location Lat Long //////
    class func saveCurrentLatLong(currentLat: String,currentLong: String){
        defaults.set(currentLat, forKey: AppConstant.userCurrentLatitude)
        defaults.set(currentLong, forKey: AppConstant.userCurrentLongitude)
        defaults.synchronize()
    }
    
    class func getCurrentLatLong() -> (String?,String?) {
        let currentLatitude:String?
        let currentLongitude:String?
        currentLatitude     = defaults.object( forKey: AppConstant.userCurrentLatitude) as? String
        currentLongitude    = defaults.object( forKey: AppConstant.userCurrentLongitude) as? String
        return (currentLatitude ?? nil,currentLongitude ?? nil)
    }
    
    class func removeCurrentLatLong(){
        defaults.removeObject(forKey: AppConstant.userCurrentLatitude)
        defaults.removeObject(forKey: AppConstant.userCurrentLongitude)
        defaults.synchronize()
    }
    
    //MARK:- Update Account Type
    class func saveACType(type: String?){
        defaults.set(type, forKey: AppConstant.userAcType)
        defaults.synchronize()
    }
    
    class func getACType() -> String? {
        let type:String?
        type = defaults.object( forKey: AppConstant.userAcType) as? String
        return type
    }
    
    class func removeACType(){
        defaults.removeObject(forKey: AppConstant.userAcType)
        defaults.synchronize()
    }
    
    
    class func saveChatID(userID: String) {
        defaults.set(userID, forKey: AppConstant.kChatID)
        defaults.synchronize()
    }
    
    class func getChatID()->String? {
        let userID:String?
        userID = defaults.object(forKey: AppConstant.kChatID) as? String
        return userID ?? nil
    }
    
    class func removeChatID(){
        defaults.removeObject(forKey: AppConstant.kChatID)
        defaults.synchronize()
    }
    
    
    //MARK:- Managing Application Activity Time
    //Save App Launch Time
    class func saveAppLaunchingTime(time: String?){
        defaults.set(time, forKey: AppConstant.userDefaultAppLaunchTime)
        defaults.synchronize()
    }
    
    class func getAppLaunchingTime()->String? {
        let time:String?
        time = defaults.object(forKey: AppConstant.userDefaultAppLaunchTime) as? String
        return time ?? nil
    }
    
    //Save App Termination Time
    class func saveAppTerminationTime(time: String?){
        defaults.set(time, forKey: AppConstant.userDefaultAppTerminationTime)
        defaults.synchronize()
    }
    
    class func getAppTerminationTime()->String? {
        let time:String?
        time = defaults.object(forKey: AppConstant.userDefaultAppTerminationTime) as? String
        return time ?? nil
    }
    
    //Save App Activity Time
    class func saveAppActivityTime(time: String?){
        defaults.set(time, forKey: AppConstant.userDefaultAppActivityTime)
        defaults.synchronize()
    }

    class func getAppActivityTime()->String? {
        let time:String?
        time = defaults.object(forKey: AppConstant.userDefaultAppActivityTime) as? String
        return time ?? nil
    }
    
    
    //Save App Installed Date
    class func saveAppInstalledDateTime(time: String?){
        defaults.set(time, forKey: AppConstant.userDefaultAppInstalledDateTime)
        defaults.synchronize()
    }
    
    class func getAppInstalledDateTime()->String? {
        let time:String?
        time = defaults.object(forKey: AppConstant.userDefaultAppInstalledDateTime) as? String
        return time ?? nil
    }
    
    
    //Save Video Straming Data
    class func saveSignalRNegotiateUrl(url: String?){
        defaults.set(url, forKey: AppConstant.userDefaultSignalRNegotiateUrl)
        defaults.synchronize()
    }
    
    class func getSignalRNegotiateUrl()->String? {
        let url:String?
        url = defaults.object(forKey: AppConstant.userDefaultSignalRNegotiateUrl) as? String
        return url ?? nil
    }
    
    class func saveStopSequenceNumber(number: Int?){
        defaults.set(number, forKey: AppConstant.userDefaultStopSequenceNumber)
        defaults.synchronize()
    }
    
    class func getStopSequenceNumber()->Int? {
        let number:Int?
        number = defaults.object(forKey: AppConstant.userDefaultStopSequenceNumber) as? Int
        return number ?? nil
    }
    
    
    
    class func saveSwitchOnOff(string: String?){
        defaults.set(string, forKey: AppConstant.userDefaultSwitchOnOff)
        defaults.synchronize()
    }
    
    class func getSwitchOnOff()->String? {
        let string:String?
        string = defaults.object(forKey: AppConstant.userDefaultSwitchOnOff) as? String
        return string ?? nil
    }
    
    
    
}

