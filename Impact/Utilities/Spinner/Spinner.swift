import UIKit

open class Spinner: UIView {
    
    // MARK: - Singleton
    
    //
    // Access the singleton instance
    //
    open class var sharedInstance: Spinner {
        struct Singleton {
            static let instance = Spinner(frame: CGRect.zero)
        }
        return Singleton.instance
    }
    
    // MARK: - Init
    
    //
    // Custom init to build the spinner UI
    //
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        blurEffect = UIBlurEffect(style: blurEffectStyle)
        blurView = UIVisualEffectView(effect: blurEffect)
       // addSubview(blurView)
        
        vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        addSubview(vibrancyView)
        
        let titleScale: CGFloat = 0.1
        titleLabel.frame.size = CGSize(width: frameSize.width * titleScale, height: frameSize.height * titleScale)
        titleLabel.font = defaultTitleFont
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textColor = UIColor.white
        
        // TODO : Add Logo ImageView
        logoImageView.frame = titleLabel.frame
        blurView.contentView.addSubview(logoImageView)
        // TODO : Hide Text
        // blurView.contentView.addSubview(titleLabel)
        blurView.contentView.addSubview(vibrancyView)
        
        outerCircleView.frame.size = frameSize
        
        outerCircle.path = UIBezierPath(ovalIn: CGRect(x: 0.0, y: 0.0, width: frameSize.width, height: frameSize.height)).cgPath
        outerCircle.lineWidth = 8.0
        outerCircle.strokeStart = 0.0
        outerCircle.strokeEnd = 0.45
        outerCircle.lineCap = CAShapeLayerLineCap.round
        outerCircle.fillColor = UIColor.clear.cgColor
       // outerCircle.strokeColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        
        outerCircleView.layer.addSublayer(outerCircle)
        
        outerCircle.strokeStart = 0.0
        outerCircle.strokeEnd = 1.0
        
        blurView.contentView.addSubview(outerCircleView)
        
        innerCircleView.frame.size = frameSize
        
        let innerCirclePadding: CGFloat = 12
        innerCircle.path = UIBezierPath(ovalIn: CGRect(x: innerCirclePadding, y: innerCirclePadding, width: frameSize.width - 2*innerCirclePadding, height: frameSize.height - 2*innerCirclePadding)).cgPath
        innerCircle.lineWidth = 4.0
        innerCircle.strokeStart = 0.5
        innerCircle.strokeEnd = 0.9
        innerCircle.lineCap = CAShapeLayerLineCap.round
        innerCircle.fillColor = UIColor.clear.cgColor
        //innerCircle.strokeColor = (Utils.AppColors.Black).cgColor
        innerCircle.strokeColor = UIColor.lightGray.cgColor
        innerCircleView.layer.addSublayer(innerCircle)
        innerCircle.strokeStart = 0.0
        innerCircle.strokeEnd = 1.0
        
        self.addSubview(innerCircleView)
        
        isUserInteractionEnabled = true
    }
    
    
    // Rotation state
    @IBInspectable
    open var clockWiseRotation: Bool = true {
        didSet {
            
        }
    }
    
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return self
    }
    
    // MARK: - Public interface
    
    open lazy var titleLabel = UILabel()
    open var subtitleLabel: UILabel?
    open lazy var logoImageView = UIImageView()
    
    //
    // Custom superview for the spinner
    //
    fileprivate static weak var customSuperview: UIView? = nil
    fileprivate static func containerView() -> UIView? {
        return customSuperview ?? UIApplication.shared.keyWindow
    }
    open class func useContainerView(_ sv: UIView?) {
        customSuperview = sv
    }
    
    //
    // Show the spinner activity on screen, if visible only update the title
    //
    @discardableResult
    open class func show(_ title: String, animated: Bool = true) -> Spinner {
        
        let spinner = Spinner.sharedInstance
        
        spinner.clearTapHandler()
        
        spinner.updateFrame()
        
        if spinner.superview == nil {
            //show the spinner
            spinner.alpha = 0.0
            
            guard let containerView = containerView() else {
                fatalError("\n`UIApplication.keyWindow` is `nil`. If you're trying to show a spinner from your view controller's `viewDidLoad` method, do that from `viewWillAppear` instead. Alternatively use `useContainerView` to set a view where the spinner should show")
            }
            
            containerView.addSubview(spinner)
            
            UIView.animate(withDuration: 0.33, delay: 0.0, options: .curveEaseOut, animations: {
                spinner.alpha = 1.0
            }, completion: nil)
            
            #if os(iOS)
            
            
            // Orientation change observer
            NotificationCenter.default.addObserver(
                spinner,
                selector: #selector(Spinner.updateFrame),
                name: UIApplication.didChangeStatusBarOrientationNotification,
                object: nil)
            #endif
        }
        
        spinner.title = title
        spinner.animating = animated
        
        return spinner
    }
    
    
    //
    // Show the spinner activity on screen with duration, if visible only update the title
    //
    @discardableResult
    open class func show(_ duration: Double, title: String, animated: Bool = true) -> Spinner {
        let spinner = Spinner.show(title, animated: animated)
        spinner.delay(duration) {
            Spinner.hide()
        }
        return spinner
    }
    
    fileprivate static var delayedTokens = [String]()
    
    
    ///
    /// Show the spinner with the outer circle representing progress (0 to 1)
    ///
    @discardableResult
    open class func show(_ progress: Double, title: String) -> Spinner {
        let spinner = Spinner.show(title, animated: false)
        spinner.outerCircle.strokeEnd = CGFloat(progress)
        return spinner
    }
    //
    // Hide the spinner
    //
    public static var hideCancelsScheduledSpinners = true
    open class func hide(_ completion: (() -> Void)? = nil) {
        
        let spinner = Spinner.sharedInstance
        
        NotificationCenter.default.removeObserver(spinner)
        if hideCancelsScheduledSpinners {
            delayedTokens.removeAll()
        }
        
        DispatchQueue.main.async(execute: {
            spinner.clearTapHandler()
            
            if spinner.superview == nil {
                return
            }
            
            UIView.animate(withDuration: 0.33, delay: 0.0, options: .curveEaseOut, animations: {
                spinner.alpha = 1.0
            }, completion: {_ in
                spinner.alpha = 1.0
                spinner.removeFromSuperview()
                spinner.titleLabel.font = spinner.defaultTitleFont
                spinner.titleLabel.text = nil
                spinner.logoImageView.image = nil
                
                completion?()
            })
            
            spinner.animating = false
        })
    }
    
    //
    // Set the default title font
    //
    public class func setTitleFont(_ font: UIFont?) {
        let spinner = Spinner.sharedInstance
        
        if let font = font {
            spinner.titleLabel.font = font
        } else {
            spinner.titleLabel.font = spinner.defaultTitleFont
        }
    }
    
    //
    // The spinner title
    //
    public var title: String = "" {
        didSet {
            let spinner = Spinner.sharedInstance
            
            guard spinner.animating else {
                spinner.titleLabel.transform = CGAffineTransform.identity
                spinner.titleLabel.alpha = 1.0
                spinner.logoImageView.alpha = 1.0
                spinner.titleLabel.text = self.title
                //spinner.logoImageView.image = defaultLogo
                spinner.logoImageView.contentMode = .scaleAspectFill
                
                return
            }
            
            UIView.animate(withDuration: 0.15, delay: 0.0, options: .curveEaseOut, animations: {
                spinner.titleLabel.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
                spinner.logoImageView.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
                spinner.titleLabel.alpha = 0.2
                spinner.logoImageView.alpha = 0.2
            }, completion: {_ in
                spinner.titleLabel.text = self.title
                //spinner.logoImageView.image = self.defaultLogo
                UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 0.35, initialSpringVelocity: 0.0, options: [], animations: {
                    spinner.titleLabel.transform = CGAffineTransform.identity
                    spinner.logoImageView.transform = CGAffineTransform.identity
                    spinner.titleLabel.alpha = 1.0
                    spinner.logoImageView.alpha = 1.0
                }, completion: nil)
            })
        }
    }
    
    //
    // observe the view frame and update the subviews layout
    //
    open override var frame: CGRect {
        didSet {
            if frame == CGRect.zero {
                return
            }
            blurView.frame = bounds
            vibrancyView.frame = blurView.bounds
            titleLabel.center = vibrancyView.center
            logoImageView.center = vibrancyView.center
            outerCircleView.center = vibrancyView.center
            innerCircleView.center = vibrancyView.center
            if let subtitle = subtitleLabel {
                subtitle.bounds.size = subtitle.sizeThatFits(bounds.insetBy(dx: 20.0, dy: 0.0).size)
                subtitle.center = CGPoint(x: bounds.midX, y: bounds.maxY - subtitle.bounds.midY - subtitle.font.pointSize)
            }
        }
    }
    
    //
    // Start the spinning animation
    //
    
    public var animating: Bool = false {
        
        willSet (shouldAnimate) {
            if shouldAnimate && !animating {
                spinInner()
                spinOuter()
            }
        }
        
        didSet {
            // update UI
            if animating {
                if clockWiseRotation {
                    spinInner()
                    spinOuter()
                }
                self.outerCircle.strokeStart = 0.0
                self.outerCircle.strokeEnd = 0.45
                self.innerCircle.strokeStart = 0.5
                self.innerCircle.strokeEnd = 0.9
            } else {
                self.outerCircle.strokeStart = 0.0
                self.outerCircle.strokeEnd = 1.0
                self.innerCircle.strokeStart = 0.0
                self.innerCircle.strokeEnd = 1.0
            }
        }
    }
    
    //
    // Tap handler
    //
    public func addTapHandler(_ tap: @escaping (()->()), subtitle subtitleText: String? = nil) {
        clearTapHandler()
        
        //vibrancyView.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("didTapSpinner")))
        tapHandler = tap
        
        if subtitleText != nil {
            subtitleLabel = UILabel()
            if let subtitle = subtitleLabel {
                subtitle.text = subtitleText
                subtitle.font = UIFont(name: defaultTitleFont.familyName, size: defaultTitleFont.pointSize * 0.8)
                subtitle.textColor = UIColor.white
                subtitle.numberOfLines = 0
                subtitle.textAlignment = .center
                subtitle.lineBreakMode = .byWordWrapping
                subtitle.bounds.size = subtitle.sizeThatFits(bounds.insetBy(dx: 20.0, dy: 0.0).size)
                subtitle.center = CGPoint(x: bounds.midX, y: bounds.maxY - subtitle.bounds.midY - subtitle.font.pointSize)
                vibrancyView.contentView.addSubview(subtitle)
            }
        }
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if tapHandler != nil {
            tapHandler?()
            tapHandler = nil
        }
    }
    
    public func clearTapHandler() {
        isUserInteractionEnabled = false
        subtitleLabel?.removeFromSuperview()
        tapHandler = nil
    }
    
    // MARK: - Private interface
    
    //
    // layout elements
    //
    
    fileprivate var blurEffectStyle: UIBlurEffect.Style = .extraLight
    fileprivate var blurEffect: UIBlurEffect!
    fileprivate var blurView: UIVisualEffectView!
    fileprivate var vibrancyView: UIVisualEffectView!
    
    var defaultLogo : UIImage =  #imageLiteral(resourceName: "radio_unchecked")
    
    var defaultTitleFont = UIFont(name: "Helvetica Neue", size: 18.0)!
    let frameSize = CGSize(width: 80.0, height:80.0)
    
    fileprivate lazy var outerCircleView = UIView()
    fileprivate lazy var innerCircleView = UIView()
    
    fileprivate let outerCircle = CAShapeLayer()
    fileprivate let innerCircle = CAShapeLayer()
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("Not coder compliant")
    }
    
    fileprivate var currentOuterRotation: CGFloat = 0.0
    fileprivate var currentInnerRotation: CGFloat = 0.1
    
    fileprivate func spinOuter() {
        
        if superview == nil {
            return
        }
        
        //outer circle
        if clockWiseRotation {
            
            if self.animating {
                let rotation : CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
                
                rotation.duration = 1.0
                rotation.isRemovedOnCompletion = false
                rotation.repeatCount = HUGE
                rotation.fillMode = CAMediaTimingFillMode.forwards
                rotation.fromValue = NSNumber(value: 0.0 as Float)
                rotation.toValue = NSNumber(value: .pi * 2.0 as Float)
                
                self.outerCircleView.layer.add(rotation, forKey: "rotate")
            }
            
        } else{
            
            let duration = Double(Float(arc4random()) /  Float(UInt32.max)) * 2.0 + 1.5
            let randomRotation = Double(Float(arc4random()) /  Float(UInt32.max)) * (Double.pi / 4) + (Double.pi / 4)
            
            UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.0, options: [], animations: {
                self.currentOuterRotation -= CGFloat(randomRotation)
                self.outerCircleView.transform = CGAffineTransform(rotationAngle: self.currentOuterRotation)
            }, completion: {_ in
                let waitDuration = Double(Float(arc4random()) /  Float(UInt32.max)) * 1.0 + 1.0
                self.delay(waitDuration, completion: {
                    if self.animating {
                        self.spinOuter()
                    }
                })
            })
            
        }
        
    }
    
    fileprivate func spinInner() {
        if superview == nil {
            return
        }
        
        //inner circle
        if clockWiseRotation {
            
            if self.animating {
                let rotation : CABasicAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
                
                rotation.duration = 1.0
                rotation.isRemovedOnCompletion = false
                rotation.repeatCount = HUGE
                rotation.fillMode = CAMediaTimingFillMode.forwards
                rotation.fromValue = NSNumber(value: 0.0 as Float)
                rotation.toValue = NSNumber(value: -.pi * 2.0 as Float)
                
                self.innerCircleView.layer.add(rotation, forKey: "rotate")
            }
            
        }else{
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [], animations: {
                self.currentInnerRotation += CGFloat(Double.pi / 4)
                self.innerCircleView.transform = CGAffineTransform(rotationAngle: self.currentInnerRotation)
            }, completion: {_ in
                self.delay(0.5, completion: {
                    if self.animating {
                        self.spinInner()
                    }
                })
            })
        }
        
    }
    
    @objc public func updateFrame() {
        if let containerView = Spinner.containerView() {
            Spinner.sharedInstance.frame = containerView.bounds
        }
    }
    
    // MARK: - Util methods
    
    func delay(_ seconds: Double, completion:@escaping ()->()) {
        let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: popTime) {
            completion()
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        updateFrame()
    }
    
    // MARK: - Tap handler
    fileprivate var tapHandler: (()->())?
    func didTapSpinner() {
        tapHandler?()
    }
}
