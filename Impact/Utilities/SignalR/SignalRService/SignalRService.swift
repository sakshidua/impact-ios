import Foundation


@objc public protocol SignalRClientDelegate : AnyObject {
    func clientDidConnectSuccess(client: SignalRClient)
    func clientDidConnectFailed(client: SignalRClient, error: Error)
    func clientDidClosed(client: SignalRClient, error: Error?)
}

@objcMembers public class SignalRClient : NSObject {
    
    var url: String
    var headers: [String: String]?
    var deviceId: String
    
    private var connection: HubConnection?
    public weak var delegate: SignalRClientDelegate?
    
    public init(url: String, headers: [String: String]?, deviceId: String?) {
        self.url = url
        self.headers = headers
        self.deviceId = deviceId!
        super.init()
    }
    
    public func start() {
        let token = ApplicationPreference.getAccessToken() ?? ""
        self.connection = HubConnectionBuilder(url: URL(string: self.url)!)
            .withLogging(minLogLevel: .debug)
            .withAutoReconnect()
            .withHubConnectionDelegate(delegate: self)
            .withHttpConnectionOptions { httpConnectionOptions in
                httpConnectionOptions.accessTokenProvider = {
                    return token
                }
            }.build()
        
        self.connection?.on(method: self.deviceId, callback: { response in
            if let json = response as? String {
                print("string result ----> \(json)")
                return
            }
            
            if let json = response as? [String:Any] {
                print("dictionary result ----> \(json)")
                return
            }
            
            if let json = response as? [String:String] {
                print("dictionary result ----> \(json)")
                return
            }
            
            if let json = response as? SignalRClientModel {
                print("dictionary result ----> \(json)")
                return
            }

            
            let result = try response.getArgument(type: SignalRClientModel.self)
            print("result ----> \(result)")
        })
        
        self.connection?.start()
    }
    
    
    
    public func stop() {
        self.connection?.stop()
    }
    
    public func on(method: String, callback: @escaping (_ jsonString: String?) -> Void) {
        
        self.connection?.on(method: method, callback: {args in
            if let json = args as? String {
                callback(json)
                return
            }
            callback(nil)
        })
        
        self.connection?.on(method: method, callback: { jsonString in
            print("arguments: \(jsonString)")
        })
        
        self.connection?.on(method: method, callback: { (user: String, message: String) in
            
            do {
                //self.handleMessage(message, from: user)
                print(message)
            } catch {
                print(error)
            }
        })
        
    }
    
    public func send(method: String, arguments:[AnyObject], sendDidComplete: @escaping (_ error: Error?) -> Void) {
        // self.connection?.send(method: method, arguments: arguments, sendDidComplete: sendDidComplete)
    }
    
    public func invoke(method: String, arguments: [AnyObject], invocationDidComplete: @escaping (_ error: Error?) -> Void) {
        //  self.connection?.invoke(method: method, arguments: arguments, invocationDidComplete: invocationDidComplete)
    }
}

extension SignalRClient: HubConnectionDelegate {
    public func connectionDidOpen(hubConnection: HubConnection) {
        print(hubConnection)
    }
    
    public func connectionDidOpen(hubConnection: HubConnection!) {
        self.delegate?.clientDidConnectSuccess(client: self)
    }
    
    public func connectionDidFailToOpen(error: Error) {
        self.delegate?.clientDidConnectFailed(client: self, error: error)
    }
    
    public func connectionDidClose(error: Error?) {
        self.delegate?.clientDidClosed(client: self, error: error)
    }
}


public extension HubConnectionBuilder {
    /**
     A convenience method for configuring a `HubConnection` to use the `JSONHubProtocol`.
     */
    //    func withJSONHubProtocol() -> HubConnectionBuilder {
    //        return self.withHubProtocol(hubProtocolFactory: {logger in JSONHubProtocol(logger: logger)})
    //    }
}
