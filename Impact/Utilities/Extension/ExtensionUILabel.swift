//
//  ExtensionUILabel.swift
//  Impact
//
//  Created by Mohd Ali Khan on 27/05/21.
//

import Foundation

extension UILabel {
    func underline() {
        guard let text = self.text else { return }

        let textRange = NSRange(location: 0, length: text.length)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(.underlineStyle,
                                    value: NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        // Add other attributes if needed
        self.attributedText = attributedText
    }
}
