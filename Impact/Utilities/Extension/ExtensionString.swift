import Foundation
import UIKit

extension String {
    var length: Int {
        return self.count
    }
    
    // Returns true if the string has at least one character in common with matchCharacters.
    func containsCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet as CharacterSet) != nil
    }
    
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
        //return self.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
    }
    
    // Returns true if the string has no characters in common with matchCharacters.
    func doesNotContainCharactersIn(matchCharacters: String) -> Bool {
        let characterSet = NSCharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet as CharacterSet) == nil
    }
    
    // Returns true if the string represents a proper numeric value.
    // This method uses the device's current locale setting to determine
    // which decimal separator it will accept.
    func isNumeric() -> Bool
    {
        let scanner = Scanner(string: self)
        
        // A newly-created scanner has no locale by default.
        // We'll set our scanner's locale to the user's locale
        // so that it recognizes the decimal separator that
        // the user expects (for example, in North America,
        // "." is the decimal separator, while in many parts
        // of Europe, "," is used).
        scanner.locale = NSLocale.current
        
        return scanner.scanDecimal(nil) && scanner.isAtEnd
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        
        if (self.components(separatedBy:(NSCharacterSet.whitespacesAndNewlines)).count > 1) {
            return false
        }
        return true
    }
    
    func isValidFullName() -> Bool {
        let regularExpression = "^([a-zA-Z]{1,}\\s[a-zA-z]{0,}'?-?[a-zA-Z]{1,}\\s?([a-zA-Z]{0,})?)"
        let fullnameValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return fullnameValidation.evaluate(with: self)
    }
    
    /*var password = "@Abcd011" //string from UITextField (Password)
     password.isValidPassword() // -> true*/
    func isContainValidPassword() -> Bool {
        let regularExpression = /*"^{5,}$"*/"((?=.*[0-9])(?=.*[!@#$&*])(?=.*[a-z])(?=.*[A-Z]).{8,12})"
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: self)
    }
    //    func isPasswordValid(_ password : String) -> Bool{
    //        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].0*[a-z].*[a-z]).{8}$")
    //        return passwordTest.evaluate(with: password)
    //    }
    func isMatchConfirmPassword(password : String) -> Bool {
        return (password == self)
    }
    
    func trimmedString() -> String? {
        return (self.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines)))
    }
    
    func isValidURL() -> Bool {
        let urlRegEx = "(www).((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlText = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        return urlText.evaluate(with: self)
    }
    
    func isPasswordValidate() -> Bool{
     
        let regexPasswdPattrn  =   "(?=.*[A-Z])(?=.*[@#$%])(?=.*[0-9])(?=.*[a-z]).{8,20}"
        let texttest = NSPredicate(format:"SELF MATCHES %@", regexPasswdPattrn)
        let isValidate = texttest.evaluate(with: self)
        
        return isValidate
        
    }
    
    func isValidName() -> Bool {
        let nameRegEx = "[a-zA-z.\\s]+([ '-][a-zA-Z]+)*$"
        
        let nameText = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameText.evaluate(with: self)
    }
    
    func isValidUserName() -> Bool {
        let usernameRegEx = "^(?=.{1,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        
        let userTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        return userTest.evaluate(with: self)
    }
    
    func isValidZipcode() -> Bool {
        let zipRegEx = "([0-9]{6}|[0-9]{3}\\s[0-9]{3})"
        
        let zipText = NSPredicate(format:"SELF MATCHES %@", zipRegEx)
        return zipText.evaluate(with: self)
    }
    
    func isValidPhoneNumber() -> Bool {
        let phoneRegEx = "^+(?:[0-9] ?){6,14}[0-9]$"
        
        let phoneText = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneText.evaluate(with: self)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat(Float.greatestFiniteMagnitude))
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.width
    }
    
    var byWords: [String] {
        var result:[String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {
            guard let word = $0 else { return }
            print($1,$2,$3)
            result.append(word)
        }
        return result
    }
    var lastWord: String {
        return byWords.last ?? ""
    }
    func lastWords(_ max: Int) -> [String] {
        return Array(byWords.suffix(max))
    }

    
    func append(lastName: String) -> String {
        return self + " " + lastName
    }
    
    //MARK:- String to Int Conversion
     func toInt()->Int? {
            let val = NSString(format: "%@", self)
            return val.integerValue
    }
    
    func toNSNumber()->NSNumber? {
    
    if let myInteger = Int(self) {
        let myNumber = NSNumber(value:myInteger)
        return myNumber
      }
        return 0
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    

    func removeOptionalString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
}

//MARK: - ↓↓ Dictionary ↓↓
//MARK: -
extension Dictionary where Value: Equatable {
    func containsValue(value : Value) -> Bool {
        return self.contains { $0.1 == value }
    }
    
    
}


//MARK:- Bool to NSNumber Conversion
extension Bool {
    func toNumber() -> NSNumber {
        let num = NSNumber(booleanLiteral: self)
        return num
    }
}


//MARK:- Int to String Conversion
extension Int {
    func toString()->String? {
        let val = NSNumber(integerLiteral: self)
        return val.stringValue
    }
    
}


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
