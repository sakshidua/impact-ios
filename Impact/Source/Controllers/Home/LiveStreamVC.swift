//
//  LiveStreamVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 24/05/21.
//

import UIKit
import AVKit

var signalRData: SignalRClientModel!
@available(iOS 13.0, *)
class LiveStreamVC: UIViewController {
    
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    var seqNumber: Int!
    var deviceId: String!
    var streamURL: String!
    private let timerTime = 35
    private var timer : Timer!
    private var isTimerInvalidate = false
    private var playerViewController: AVPlayerViewController!
    
    private var hubConnection: HubConnection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
}

// MARK:- Instance Methods
@available(iOS 13.0, *)
extension LiveStreamVC {
    
    private func setup() {
        
        self.addTimer()
        self.playerViewController = AVPlayerViewController()
        self.activityIndicator.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidDismiss), name: NSNotification.Name("playerDidDismiss"), object: nil)
        self.establishConnectionForLiveStream()
    }
    
    private func addTimer() {
        var runCount = 0
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.timer = timer
            print("runCount ---> \(runCount)")
            runCount += 1
            if runCount == self.timerTime {
                self.invalidateTimerAndDismiss()
            }
        }
    }
    
    private func invalidateTimerAndDismiss() {
        self.timer.invalidate()
        self.isTimerInvalidate = true
        self.stopVideoStreamFromServer()
    }
    
    private func establishConnectionForLiveStream() {
        let token = ApplicationPreference.getAccessToken() ?? ""
        self.hubConnection = HubConnectionBuilder(url: URL(string: self.streamURL)!)
            .withLogging(minLogLevel: .debug)
            .withAutoReconnect()
            .withHubConnectionDelegate(delegate: self)
            .withHttpConnectionOptions(configureHttpOptions: { httpConnectionOptions in
                httpConnectionOptions.accessTokenProvider = {
                    return token
                }
            })
            .build()
        
        self.hubConnection!.on(method: self.deviceId) { argumentExtractor in
            if signalRData != nil {
                if let url = signalRData.arguments?.first?.hlsURL  {
                    self.startLiveStreaminInPlayer(url: url)
                    return
                }
                if let successMessage = signalRData.arguments?.first?.successMessage  {
                    Helper.sharedInstance.showToast(isError: true, title: successMessage)
                    return
                }
            }
        }
        self.hubConnection!.start()
    }
    
    private func startLiveStreaminInPlayer(url:String) {
        
        guard let videoURL = URL(string: url) else {
            Helper.sharedInstance.showToast(isError: true, title: "HLS Url not found.")
            return
        }
        self.timer.invalidate()
        self.activityIndicator.stopAnimating()
        let player = AVPlayer(url: videoURL)
        playerViewController.player = player
        present(playerViewController, animated: false) {
            self.playerViewController.player?.play()
        }
    }
    
    private func playVideoInCustomView(url:String) {
        let playerView = UIView(frame: self.view.frame)
        let videoURL = URL(string: url)
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = playerView.frame
        playerView.layer.addSublayer(playerLayer)
        self.view.addSubview(playerView)
        player.play()
    }
    
    @IBAction func backAction(_ sender: UIButton){
        self.stopVideoStreamFromServer()
    }
    
    @objc func playerDidDismiss() {
        self.playerViewController.dismiss(animated: false) {
            self.stopVideoStreamFromServer()
        }
    }
}

// MARK:- SignalR Delegate
@available(iOS 13.0, *)
extension LiveStreamVC: HubConnectionDelegate {
    func connectionDidOpen(hubConnection: HubConnection) {
        print("connectionDidOpen")
    }
    
    func connectionDidFailToOpen(error: Error) {
        print("connectionDidFailToOpen")
    }
    
    func connectionDidClose(error: Error?) {
        print("connectionDidClose")
    }
}


extension AVPlayerViewController {
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isBeingDismissed == false {
            return
        }
        
        NotificationCenter.default.post(Notification.init(name: Notification.Name("playerDidDismiss")))
    }
}

// MARK:- API Calls
@available(iOS 13.0, *)
extension LiveStreamVC {
    
    private func stopVideoStreamFromServer()  {
        self.view.endEditing(true)
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.stopVideoStraming(deviceId: self.deviceId, seqNumber:self.seqNumber), andParam: [:], showHud: false) { (responseDict) in
            print("responseDict ---> \(responseDict)")
            Helper.UI {
                self.hubConnection?.stop()
                self.navigationController?.popViewController(animated: true)
                if self.isTimerInvalidate {
                    Helper.sharedInstance.showToast(isError: true, title: "Something went wrong. Please try again.")
                }
            }
        }
    }
}
