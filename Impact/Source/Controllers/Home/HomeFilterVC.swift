//
//  HomeFilterVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit
import DropDown


enum dateType : String{
    case startDate = "startDate"
    case startTime = "startTime"
    case endDate = "endDate"
    case endTime = "endTime"
}

enum AlertType: String {
    case face = "Face"
    case mask = "Mask"
    case crowd = "Crowd"
    case crouch = "Crouch"
    case loiter = "Loiter"
    case helmet = "Helmet"
    case trespassing = "Trespassing"
}

protocol HomeFilterVCDelegate {
    func selectedData(fromDate: String, toDate: String, isReadAlert:Bool, isBookMarked: Bool, isCorrect: Bool, location: String, arrSelected: [Int], isUnreadAlert:Bool)
}

@available(iOS 13.0, *)
class HomeFilterVC: BaseVC {
    
    @IBOutlet weak var btnToDate : UIButton!
    @IBOutlet weak var btnFromDate : UIButton!
    @IBOutlet weak var btnReadAlerts : UIButton!
    @IBOutlet weak var btnUnreadAlerts : UIButton!
    @IBOutlet weak var btnMarkAsImportant : UIButton!
    @IBOutlet weak var btnReportedIncorrect : UIButton!
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var viewUnreadAlerts : UIView!
    @IBOutlet weak var tblDetection : UITableView!
    @IBOutlet weak var layoutConstraint : NSLayoutConstraint!
    
        
    var delegate: HomeFilterVCDelegate?
    var arrAlertType = [AlertTypeModel]()
    var arrLocation = [LocationModel]()
    var arrSelctedId = [Int]()
    var locationId = ""
    var deviceType = ""
    var isMarkAsImportant = false
    var isReportedIncorrect = false
    var isReadAlerts = false
    var isUnreadAlerts = false
    var devicesTypesForFilter : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblDetection.estimatedRowHeight = 28.0
        self.tblDetection.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setInitialData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func setInitialData() {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let toDate: String = dateFormatter.string(from: Date())
        let fromDate: String = dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: -7, to: Date())!)
        self.btnToDate.setTitle(toDate, for: .normal)
        self.btnFromDate.setTitle(fromDate, for: .normal)
        
        // get Location
        self.getLocationFromServer()
        
        // get Alert Type
        self.getAlertTypeFromServer()
        
        // check for device and show unread alert view
//        viewUnreadAlerts.isHidden = (deviceType != "WSD") ? true : false
    }
 
    func setupDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 34
        appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appearance.animationduration = 0.15
        appearance.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    }
    
   
    func getAlertTypeFromServer()  {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getAlertType+devicesTypesForFilter, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    for dictData in arrTempData{
                        let objData = AlertTypeModel.init(id: dictData["value"] as? Int, name: dictData["text"] as? String, isSelected: false)
                        self.arrAlertType.append(objData)
                        print(dictData)
                    }
                   // self.layoutConstraint.constant = 600
                    self.tblDetection.reloadData()
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func getLocationFromServer()  {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getLocation, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    for dictData in arrTempData{
                        let objData = LocationModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
                        self.arrLocation.append(objData)
                    }
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension HomeFilterVC {
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionFromDate(_ sender: UIButton){
        self.view.endEditing(true)
        self.openDatePicker(dateType: dateType.startDate)
    }
    
    @IBAction func actionToDate(_ sender: UIButton){
        self.view.endEditing(true)
        self.openDatePicker(dateType: dateType.endDate)
    }
    
    @IBAction func actionLocation(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrLocation.map { $0.name } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblLocation.text = item
            self.locationId = self.arrLocation[index].id ?? ""
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionClearAll(_ sender: UIButton){
        self.view.endEditing(true)
        
        self.arrSelctedId.removeAll()
        for obj in self.arrAlertType.filter({ $0.isSelected == true}){
            obj.isSelected = false
        }
        self.locationId = ""
        self.lblLocation.text = "All"
      
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let toDate: String = dateFormatter.string(from: Date())
        let fromDate: String = dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: -7, to: Date())!)
        
        self.btnToDate.setTitle(toDate, for: .normal)
        self.btnFromDate.setTitle(fromDate, for: .normal)
        
        self.btnMarkAsImportant.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        self.btnReportedIncorrect.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        self.btnReadAlerts.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        self.tblDetection.reloadData()
    }
    
    @IBAction func actionSaveSearch(_ sender: UIButton){
        self.view.endEditing(true)
        self.arrSelctedId.removeAll()
        for obj in self.arrAlertType.filter({ $0.isSelected == true}){
            self.arrSelctedId.append(obj.id ?? 0)
        }
  
        let fromDate = self.btnFromDate.currentTitle ?? ""
        let toDate = self.btnToDate.currentTitle ?? ""
       
        
        delegate?.selectedData(fromDate: fromDate, toDate: toDate, isReadAlert:self.isReadAlerts, isBookMarked: self.isMarkAsImportant, isCorrect: self.isReportedIncorrect, location: self.locationId, arrSelected: self.arrSelctedId, isUnreadAlert: self.isUnreadAlerts)
    }
    
    @IBAction func actionSearch(_ sender: UIButton){
        self.view.endEditing(true)
        self.arrSelctedId.removeAll()
        for obj in self.arrAlertType.filter({ $0.isSelected == true}){
            self.arrSelctedId.append(obj.id ?? 0)
        }
      
        let fromDate = self.btnFromDate.currentTitle ?? ""
        let toDate = self.btnToDate.currentTitle ?? ""
        
        print(self.convertDateFormater(fromDate))
        print(self.convertDateFormater(toDate))
        
        delegate?.selectedData(fromDate: self.convertDateFormater(fromDate), toDate: self.convertDateFormater(toDate), isReadAlert:self.isReadAlerts, isBookMarked: self.isMarkAsImportant, isCorrect: self.isReportedIncorrect, location: self.locationId, arrSelected: self.arrSelctedId, isUnreadAlert: self.isUnreadAlerts)
    }
    
    @IBAction func actionReportedIncorrect(_ sender: UIButton){
        self.view.endEditing(true)
        self.isReportedIncorrect.toggle()
        if isReportedIncorrect{
            self.btnReportedIncorrect.setImage(UIImage.init(named: "check_box"), for: .normal)
        }else{
            self.btnReportedIncorrect.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        }
    }
    
    @IBAction func actionMarkAsImportant(_ sender: UIButton){
        self.view.endEditing(true)
        self.isMarkAsImportant.toggle()
        if isMarkAsImportant{
            self.btnMarkAsImportant.setImage(UIImage.init(named: "check_box"), for: .normal)
        }else{
            self.btnMarkAsImportant.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        }
    }
    
    @IBAction func actionReadAlerts(_ sender: UIButton){
        self.view.endEditing(true)
        self.isReadAlerts.toggle()
        if isReadAlerts{
            self.btnReadAlerts.setImage(UIImage.init(named: "check_box"), for: .normal)
            self.isUnreadAlerts = true
            self.actionUnreadAlerts(btnUnreadAlerts)
        }else{
            self.btnReadAlerts.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        }
    }
    
    @IBAction func actionUnreadAlerts(_ sender: UIButton){
        self.view.endEditing(true)
        self.isUnreadAlerts.toggle()
        if isUnreadAlerts{
            self.btnUnreadAlerts.setImage(UIImage.init(named: "check_box"), for: .normal)
            self.isReadAlerts = true
            self.actionReadAlerts(btnReadAlerts)
        }else{
            self.btnUnreadAlerts.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        }
    }
    
    func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
}


//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension HomeFilterVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAlertType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let data = arrAlertType[indexPath.row]
            let name = data.name ?? ""
            cell.lblTitle.text = name + " Detection"
            
            if data.isSelected ?? false{
                cell.btnSetect.setImage(UIImage.init(named: "check_box"), for: .normal)
            }else{
                cell.btnSetect.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
            }
            
            cell.btnSetect.tag = indexPath.row
            cell.btnSetect.addTarget(self, action: #selector(btnSelectPressed(sender:)), for: .touchUpInside)
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
    
    @objc func btnSelectPressed(sender: UIButton){
        let obj = self.arrAlertType[sender.tag]
        obj.isSelected = !(obj.isSelected ?? false)
        self.tblDetection.reloadData()
    }
    
}


//Mark:- Open Date Picker
@available(iOS 13.0, *)
extension HomeFilterVC {
    
    func openDatePicker(dateType:dateType){
        
        let vc = UIStoryboard(name: "DatePicker", bundle: nil).instantiateViewController(withIdentifier: DatePickerVC.nameOfClass) as! DatePickerVC
        
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        
        switch dateType {
        case .startDate:
            vc.datePickerType  = .Date
            vc.minimumDate  = Calendar.current.date(byAdding: .year, value: -100, to: Date())!
            vc.maximumDate =  Date()
            
        case .startTime:
            vc.minimumDate     = Date()
            vc.maximumDate =    Calendar.current.date(byAdding: .year, value: 18, to: Date())!
            vc.datePickerType  = .Time
            
        case .endDate:
            vc.minimumDate  = Calendar.current.date(byAdding: .year, value: -100, to: Date())!
            vc.maximumDate =  Date()
            vc.datePickerType  = .Date
            
        case .endTime:
            vc.minimumDate     = Date()
            vc.maximumDate =    Calendar.current.date(byAdding: .year, value: 18, to: Date())!
            vc.datePickerType  = .Time
        }
        
        vc.onSelectDone = { timeStamp, date in
            
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            switch dateType {
            case .startDate:
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let selectedDate: String = dateFormatter.string(from: date)
                self.btnFromDate.setTitle(selectedDate, for: .normal)
                
            case .startTime:
                dateFormatter.dateFormat = "hh:mm a"
                let _: String = dateFormatter.string(from: date)
                
            case .endDate:
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let selectedDate: String = dateFormatter.string(from: date)
                self.btnToDate.setTitle(selectedDate, for: .normal)
                
            case .endTime:
                dateFormatter.dateFormat = "hh:mm a"
                let _: String = dateFormatter.string(from: date)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
}

