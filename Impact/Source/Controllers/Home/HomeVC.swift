import UIKit
import SideMenu
import SDWebImage

@available(iOS 13.0, *)
class HomeVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblAlert : UITableView!
    @IBOutlet weak var collectionList : UICollectionView!
    @IBOutlet weak var collectionDevice : UICollectionView!
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var btnLiveStream : UIButton!
    @IBOutlet weak var btnClearFilter : UIButton!
    @IBOutlet weak var btnManageSubcription : UIButton!
    @IBOutlet weak var btnAll : UIButton!
    @IBOutlet weak var btnThirdEye : UIButton!
    @IBOutlet weak var btnWSD : UIButton!
    //Subscription Expired
    @IBOutlet weak var btnRenewSubcription : UIButton!
    @IBOutlet weak var viewRenewSubscription : UIView!
    @IBOutlet weak var lblSubscriptionExpired : UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topTableConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnFilter : UIButton!
    
    var device: DeviceModel!
    var arrAlerts = [HomeModel]()
    var arrDevices = [DeviceModel]()
    var arrList : [String] = ["All","Smart AI Camera","Wireless Smoke Detector"]
    var limit : Int = 15
    var offset : Int = 0
    var reportTag : Int = 0
    var isloadMore = false
    var isFilterActivated = false
    var stringCondition : String = ""
    var urlRenewSubscription : String = ""
    var urlExpiredSubscription : String = ""
    var filterParam = [String:Any]()
    var devicesTypesForFilter : String = ""
    //MAK Changes
    var selectedTableIndex : Int = 0
    var selectedCollectionIndex : Int = -1
    var noAlertMessage = "No data available: Click on the device to add subscription or to manage the smart alerts."
    
    let titleBoldFont = UIFont(name: "HoneywellSansWeb-Bold", size: 14)
    let titleRegularFont = UIFont(name: "HoneywellSansWeb-Book", size: 14)
    let dateBoldFont = UIFont(name: "HoneywellSansWeb-Bold", size: 12)
    let dateRegularFont = UIFont(name: "HoneywellSansWeb-Book", size: 12)
    
    enum ButtonActionType: String {
        case share
        case report
        case bookmark
        case acknowledge
    }
    
    typealias UnixTimestamp = Int

    //MARK:- ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
}

//MARK:- Instance Method
@available(iOS 13.0, *)
extension HomeVC {
    
    private func setup() {
        
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = true
        self.scrollView.alwaysBounceHorizontal = true
        self.scrollView.isScrollEnabled = true
       // self.scrollView.addSubview(segControl)
        self.scrollView.isPagingEnabled = true
        self.scrollView.isDirectionalLockEnabled = false
       // self.scrollView.contentSize.height = segControl.frame.height
       // self.scrollView.contentSize.width = segControl.frame.width
        
        btnAll.backgroundColor = UIColor.appOrnageBackground()
        btnThirdEye.backgroundColor = .white
        btnWSD.backgroundColor = .white
        
        btnAll.setTitleColor(UIColor.white, for: .normal)
        btnThirdEye.setTitleColor(UIColor.black, for: .normal)
        btnWSD.setTitleColor(UIColor.black, for: .normal)
        
        
        self.filterParam = [:]
        self.isloadMore = false
        self.isFilterActivated = false
        self.selectedCollectionIndex = -1
        self.viewRenewSubscription.isHidden = true
        
        [btnLiveStream, btnManageSubcription, btnClearFilter].forEach {
            $0?.isHidden = true
        }
        //Top Constant
        self.scrollView.isHidden = true
        self.topConstraint.constant = 20
        self.topTableConstraint.constant = 0
        self.heightConstraint.constant = 0
        
        // All Alerts
        self.offset = 0
        let param = ["take": self.limit,
                     "skip": self.offset]
        self.getAllAlertsFromServer(param: param)
        
        //Device
        self.getDevicesFromServer(deviceType: "All")
    }
    
    private func startVideoStreaming(deviceId:String, seqNumber:Int) {
        guard let url = ApplicationPreference.getSignalRNegotiateUrl() else {
            Helper.sharedInstance.showToast(isError: true, title: "URL not found")
            return
        }
        self.view.endEditing(true)
        let objVC : LiveStreamVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: LiveStreamVC.nameOfClass)
        objVC.streamURL = url
        objVC.deviceId = deviceId
        objVC.seqNumber = seqNumber
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    private func reload() {
        !arrAlerts.isEmpty ? hideTableView(isHidden: false) : hideTableView(isHidden: true)
    }
    
    private func reloadAlertAt(index:Int, action:ButtonActionType) {
        switch action {
        case .acknowledge: arrAlerts[index].acknowledged = true
            
        case .bookmark:
            guard let isBookMarked = arrAlerts[index].bookMarked else {
                arrAlerts[index].bookMarked = false
                return
            };  arrAlerts[index].bookMarked = !isBookMarked
            
        default: break
        }
        tblAlert.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    private func hideTableView(isHidden: Bool) {
        self.tblAlert.isHidden = isHidden
        self.lblMessage.isHidden = !isHidden
        self.lblMessage.text = isHidden ? noAlertMessage : ""
        self.btnFilter.isHidden = isHidden
        if arrAlerts.count < 15 {
            
        }
        self.tblAlert.reloadData()
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension HomeVC {
    
    @IBAction func actionRefresh(_ sender: UIButton){
        self.view.endEditing(true)
        self.setup()
    }
    
    @IBAction func actionFilter(_ sender: UIButton){
        self.view.endEditing(true)
        
        let objVC : HomeFilterVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeFilterVC.nameOfClass)
        objVC.devicesTypesForFilter = devicesTypesForFilter
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        objVC.delegate = self
        objVC.deviceType = self.arrDevices.first?.deviceType ?? ""
        self.navigationController?.present(objVC, animated: true)
    }
    
    @IBAction func actionClearFilter(_ sender: UIButton){
        self.view.endEditing(true)
        self.setup()
    }
    
    @IBAction func actionLiveStream(_ sender: UIButton){
        self.view.endEditing(true)
        self.startVideoStreamFromServer(deviceId: arrDevices[sender.tag].id ?? "")

    }
    
    @IBAction func actionManageSubcription(_ sender: UIButton){
        self.view.endEditing(true)
        let objVC : FeatureConfigurationVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: FeatureConfigurationVC.nameOfClass)
        objVC.device = device
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    @IBAction func actionRenewSubcription(_ sender: UIButton){
        self.view.endEditing(true)
        
        if stringCondition == "renewSubscription" {
            guard let url = URL(string: urlRenewSubscription) else { return }
            UIApplication.shared.open(url)
        } else {
            guard let url = URL(string: urlExpiredSubscription) else { return }
            UIApplication.shared.open(url)
        }
        
        guard let url = URL(string: "") else { return }
        UIApplication.shared.open(url)
        
        self.viewRenewSubscription.isHidden = true
        self.tblAlert.isHidden = false
    }
    
    @IBAction func actionCancelRenewSubcription(_ sender: UIButton){
        self.view.endEditing(true)        
        self.viewRenewSubscription.isHidden = true
        self.tblAlert.isHidden = false
    }
    
    @IBAction func actionShare(_ sender: UIButton){
        self.view.endEditing(true)
        let data = self.arrAlerts[sender.tag]
        
        if let url = URL(string: data.imageUrl!),
           let data = try? Data(contentsOf: url),
           let image = UIImage(data: data) {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            let activityItem: [AnyObject] = [image as AnyObject]
            let avc = UIActivityViewController(activityItems: activityItem as [AnyObject], applicationActivities: nil)
            self.present(avc, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func actionReport(_ sender: UIButton){
        self.view.endEditing(true)
        let data = self.arrAlerts[sender.tag]
        reportTag = sender.tag
        let objVC : ReportVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: ReportVC.nameOfClass)
        objVC.delegate = self
        objVC.alertId = data.alertId ?? ""
        objVC.alertText = data.comment ?? ""
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
        
    }
    
    @IBAction func actionBookmark(_ sender: UIButton){
        self.view.endEditing(true)
        let data = self.arrAlerts[sender.tag]
        self.bookmarkAlertsFromServer(alertId: data.alertId ?? "", selctedIndex: sender.tag)
    }
    
    @IBAction func actionDelete(_ sender: UIButton){
    
        // create the alert
        let alert = UIAlertController(title: "", message: "This action can not be un done, please confirm to proceed.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: {_ in
            
            let data = self.arrAlerts[sender.tag]
            print(data.alertId!)
            APIManagerApp.shared.makeAPIRequest(ofType: DELETE, withEndPoint: APIEndPoint.alertsDelete+data.alertId!, andParam: [:], showHud: true) { (responseDict) in
                
                Helper.UI {
                    var msg = ""
                    if let errorMessage = responseDict["errorMessage"] as? String {
                        msg = errorMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                        self.dismiss(animated: true)
                    } else if let successMessage = responseDict["successMessage"] as? String {
                        msg = successMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                        self.arrAlerts.remove(at: sender.tag)
                        self.tblAlert.reloadData()
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                    }
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {_ in 
            
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
       

        
    }
    
    @IBAction func actionImageTap(_ sender: UIButton){
        
        //alertId: String
        let data = self.arrAlerts[sender.tag]
        
        var url: [String] = []
        url.append((data.imageUrl) ?? "")
        url.append((data.videoUrl) ?? "")
        
        let objVC : HomeImageVideoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeImageVideoVC.nameOfClass)
        objVC.url = url
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
        
        DispatchQueue.main.async {
            self.acknowledgeFromServer(alertId: data.alertId!, index: sender.tag)
        }
    }
    
    @IBAction func actionList(_ sender: UIButton){
        
        let tag = sender.tag
        self.offset = 0
        self.limit = 15
        
        self.lblSubscriptionExpired.isHidden = true
        self.btnRenewSubcription.isHidden = true
        
        if tag == 1 {
            
            btnAll.backgroundColor = UIColor.appOrnageBackground()
            btnThirdEye.backgroundColor = .white
            btnWSD.backgroundColor = .white
            
            btnAll.setTitleColor(UIColor.white, for: .normal)
            btnThirdEye.setTitleColor(UIColor.black, for: .normal)
            btnWSD.setTitleColor(UIColor.black, for: .normal)
            
            self.getDevicesFromServer(deviceType: "All")
            
            let param = ["take": self.limit,
                         "skip": self.offset]
            self.getAllAlertsFromServer(param: param)
            
            //Device
            
            
        }else if tag == 2 {
            
            btnAll.backgroundColor = .white
            btnThirdEye.backgroundColor = UIColor.appOrnageBackground()
            btnWSD.backgroundColor = .white
            
            btnAll.setTitleColor(UIColor.black, for: .normal)
            btnThirdEye.setTitleColor(UIColor.white, for: .normal)
            btnWSD.setTitleColor(UIColor.black, for: .normal)
            
            self.getDevicesFromServer(deviceType: "TEV")
            
            var param = ["take": self.limit,
                         "skip": self.offset,
                         "device": "TEV"] as [String:Any]
            self.getAllAlertsFromServer(param: param)
        }else{
            
            btnAll.backgroundColor = .white
            btnThirdEye.backgroundColor = .white
            btnWSD.backgroundColor = UIColor.appOrnageBackground()
            
            btnAll.setTitleColor(UIColor.black, for: .normal)
            btnThirdEye.setTitleColor(UIColor.black, for: .normal)
            btnWSD.setTitleColor(UIColor.white, for: .normal)
            
            self.getDevicesFromServer(deviceType: "WSD")
            
            var param = ["take": self.limit,
                         "skip": self.offset,
                         "device": "WSD"] as [String:Any]
            self.getAllAlertsFromServer(param: param)
        }
    }
    
    @IBAction func actionDeviceMovie(_ sender: UIButton){
        
        let data = self.arrAlerts[sender.tag]
        var device: [DeviceModel]!
        if arrDevices.count > 0  {
            device = arrDevices.filter { $0.id == data.deviceId }//arrDevices.filter { model in
            if device.count > 0{
                let objVC : DevicesDetailsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DevicesDetailsVC.nameOfClass)
                objVC.device = device[0]
                //objVC.didUpdateDevices = didUpdateDevices
                self.navigationController?.pushViewController(objVC, animated: true)
            }else{
                Helper.sharedInstance.showToast(isError: false, title: "Device not available.")
            }
            
        }else{
            Helper.sharedInstance.showToast(isError: false, title: "Device not available.")
        }
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "Test1"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return "Test2"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "Secret message"
    }
}


//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension HomeVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAlerts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let data = self.arrAlerts[indexPath.row]
            let alertType = data.alertType ?? ""
            cell.lblTitle.text = alertType + " Detection"
            cell.lblDevice.text = data.device
            cell.lblDeviceName.text = data.deviceName?.capitalized
            cell.lblDeviceName.underline()
            cell.lblLocationName.text = data.locationName
            cell.imgUrl.sd_setImage(with: URL(string: data.imageUrl ?? ""), placeholderImage: UIImage(named: "picture"))
            if data.acknowledged != true {
                cell.lblTitle.font = titleBoldFont
                cell.lblDateTime.font = dateBoldFont
                cell.lineView.isHidden = false
            } else {
                cell.lblTitle.font = titleRegularFont
                cell.lblDateTime.font = dateRegularFont
                cell.lineView.isHidden = true
            }
            
            if let alertType = data.alertType {
                if alertType == "Trespassing" {
                    cell.imgIcon.image = UIImage(named: "trespassing")
                }else if alertType == "Crouch" {
                    cell.imgIcon.image = UIImage(named: "Crouch")
                }else if alertType == "No Mask" {
                    cell.imgIcon.image = UIImage(named: "No Mask")
                }else if alertType == "Mask" {
                    cell.imgIcon.image = UIImage(named: "Mask")
                }else if alertType == "Helmet" {
                    cell.imgIcon.image = UIImage(named: "Helmet")
                }else if alertType == "Loiter" {
                    cell.imgIcon.image = UIImage(named: "Loiter")
                }else if alertType == "Crowd" {
                    cell.imgIcon.image = UIImage(named: "Crowd")
                }
            }
            
            if let timeResult = (data.occurenceTimeStamp) {
                let date = NSDate(timeIntervalSince1970: TimeInterval(timeResult))
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
                
                let dayTimePeriodFormatterTime = DateFormatter()
                dayTimePeriodFormatterTime.dateFormat = "hh:mm a"
                
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
                let timeString = dayTimePeriodFormatterTime.string(from: date as Date)
                
                cell.lblDateTime.text = dateString + " at " + timeString
            }
            
            if data.isCorrect == true {
                cell.btnReport.setImage(UIImage.init(named: "alert"), for: .normal)
            }else{
                cell.btnReport.setImage(UIImage.init(named: "red_alert"), for: .normal)
            }
            
            if data.bookMarked == true {
                cell.btnBookmark.setImage(UIImage.init(named: "bookmark_fill"), for: .normal)
            }else{
                cell.btnBookmark.setImage(UIImage.init(named: "bookmark"), for: .normal)
            }
            
            if data.acknowledged == false {
                cell.bgView.backgroundColor = .white
            }else{
                cell.bgView.backgroundColor = .white
            }
            
            cell.btnReport.tag = indexPath.row
            cell.btnReport.addTarget(self, action: #selector(self.actionReport(_:)), for: UIControl.Event.touchUpInside)
            
            cell.btnBookmark.tag = indexPath.row
            cell.btnBookmark.addTarget(self, action: #selector(self.actionBookmark(_:)), for: UIControl.Event.touchUpInside)
            
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(self.actionShare(_:)), for: UIControl.Event.touchUpInside)
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(self.actionDelete(_:)), for: UIControl.Event.touchUpInside)
            
            cell.btnDeviceName.tag = indexPath.row
            cell.btnDeviceName.addTarget(self, action: #selector(self.actionDeviceMovie(_:)), for: UIControl.Event.touchUpInside)
            
            cell.btnImage.tag = indexPath.row
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y < 0){
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            if maximumOffset - currentOffset <= 30.0 {
                if self.isloadMore {
                    self.isloadMore = false
                    self.offset = self.offset + self.limit
                    if !self.isFilterActivated {
                        let param = ["take": self.limit,
                                     "skip": self.offset]
                        self.getAllAlertsFromServer(param: param)
                    } else {
                        self.filterParam["skip"] = self.offset
                        self.getAllAlertsFromServer(param: self.filterParam)
                    }
                }
            }
        }
    }
}

//MARK: UICollectionView DataSource & Delegate Methods
@available(iOS 13.0, *)
//UICollectionViewDelegateFlowLayout
extension HomeVC:UICollectionViewDataSource, UICollectionViewDelegate{
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionList {
            return arrList.count
        }else{
            return self.arrDevices.count
        }
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as? CollectionViewCell {
            
            if collectionView == collectionList {
                cell.lblTitle.text = arrList[indexPath.row]
                return cell
            }else{
                
                let data = arrDevices[indexPath.row]
                cell.lblTitle.text = data.name
                
                cell.viewBG.layer.borderWidth = 1
                cell.viewBG.layer.borderColor = UIColor.white.cgColor
                if selectedCollectionIndex == indexPath.item {
                    cell.viewBG.layer.borderColor = UIColor.red.cgColor
                }
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let date = dateFormatter.date(from: (data.subscriptionExpiryDate)!)
                
                //Color Condition
                if data.subscriptionId == nil {
                    cell.viewBG.backgroundColor = UIColor.appGrayBackground()
                }else if (Date() > date!) {
                    cell.viewBG.backgroundColor = UIColor.appGrayBackground()
                }else{
                    cell.viewBG.backgroundColor = UIColor.white
                }
                
                //Device Type
                if let deviceType = data.deviceType {
                    if deviceType == "TEV" {
                        cell.imgDeviceType.image = UIImage.init(named: "video_cam")
                    }else{
                        cell.imgDeviceType.image = UIImage.init(named: "WSD_logo")
                    }
                }
                
                //online Condtion
                if data.subscriptionId != nil{
                    
                    if (Date() > date!) {
                        cell.viewOnline.isHidden = true
                        cell.image.isHidden = false
                    }else{
                        cell.image.isHidden = true
                        cell.viewOnline.isHidden = false
                        if (data.connected)! {
                            cell.viewOnline.backgroundColor = .green
                        }else{
                            cell.viewOnline.backgroundColor = .red
                        }
                    }
                }else{
                    cell.image.isHidden = true
                    cell.viewOnline.isHidden = false
                    if (data.connected)! {
                        cell.viewOnline.backgroundColor = .green
                    }else{
                        cell.viewOnline.backgroundColor = .red
                    }
                }
                return cell
            }
//            else{
//                return UICollectionViewCell()
//            }
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //        let cell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        //        cell.viewBG.layer.borderWidth = 1
        //        cell.viewBG.layer.borderColor = UIColor.white.cgColor
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCollectionIndex = indexPath.item
        btnLiveStream.tag = indexPath.item
        //        let cell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        //        cell.viewBG.layer.borderWidth = 1
        //        cell.viewBG.layer.borderColor = UIColor.red.cgColor
        
        let data = arrDevices[indexPath.row]
        self.tblAlert.isHidden = false
        self.viewRenewSubscription.isHidden = true
        self.btnFilter.isHidden = false
        
        let deviceId = data.id
        let param = ["take": self.limit,
                     "skip": self.offset,
                     "deviceId": deviceId!] as [String : Any]
        
        if data.disabled == true {
            btnLiveStream.isHidden = true
            btnManageSubcription.isHidden = true
            btnClearFilter.isHidden = true
            // self.getAllAlertsFromServer(param: param)
            // and clear all filters
            actionClearFilter(btnClearFilter)
            Helper.sharedInstance.showToast(isError: true, title: "This device is deleted")
            //live strean n manage button are disabled
            
        } else {
            let token = ApplicationPreference.getAccessToken() ?? ""
            let subscriptionId = data.subscriptionId
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let date = dateFormatter.date(from: (data.subscriptionExpiryDate)!)
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "dd-MM-yyyy"
            
            if subscriptionId == nil {
                //Buy Subscription
                lblSubscriptionExpired.isHidden = false
                btnRenewSubcription.isHidden = false
                
                lblSubscriptionExpired.text = "Device doesn't have active subscription"
                btnRenewSubcription.setTitle("Buy New Subscription", for: .normal)
                
                // self.getAllAlertsFromServer(param: param)
                self.viewRenewSubscription.isHidden = false
                self.tblAlert.isHidden = true
                self.btnFilter.isHidden = true
                stringCondition = "expiredSubscription"
                
                // if(subscription!=null && subscription.equals(AppConstants.BUY_SUBSCRIPTION)){
                // url = BUY_SUBSCRIPTION_URL_STAGING+accessToken+"&headless=true&deviceId="+device.getId()+"&product="+device.getDeviceType();
                // }else if(subscription!=null && subscription.equals(AppConstants.RENEW_SUBSCRIPTION)){
                // url = BUY_SUBSCRIPTION_URL_STAGING+accessToken+"&headless=true&deviceId="+device.getId()+"&subscriptionNumber="+device.getSubscriptionId()+"&product="+device.getDeviceType();
                // }
                
                let BUY_SUBSCRIPTION_URL_STAGING = "https://tev-web-staging.azurewebsites.net/tev/plan?accessToken="
                let url = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "&headless=true" + "&deviceId=\(data.id ?? "")" + "&product=\(data.deviceType ?? "")"
                self.lblMessage.isHidden = true
                btnLiveStream.isHidden = false
                btnClearFilter.isHidden = false
                btnManageSubcription.isHidden = true
                urlExpiredSubscription = url
            } else if (Date() > date!) {
                //Renew Subscription
                //live strean n manage button are disabled
                lblSubscriptionExpired.isHidden = false
                btnRenewSubcription.isHidden = false
                lblSubscriptionExpired.text = "Device subscription expired on " + "\(self.convertDateFormat(inputDate: data.subscriptionExpiryDate!))"
                btnRenewSubcription.setTitle("Renew Subscription", for: .normal)
                
                stringCondition = "renewSubscription"
                self.viewRenewSubscription.isHidden = false
                self.tblAlert.isHidden = true
                //live strean n manage button are disabled
                
                btnLiveStream.isHidden = false
                btnClearFilter.isHidden = false
                btnManageSubcription.isHidden = true
                
                let BUY_SUBSCRIPTION_URL_STAGING = "https://tev-web-staging.azurewebsites.net/tev/plan?accessToken="
                //                let url = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "headless=true" + "deviceId=\(data.id ?? "")" + "subscriptionNumber=\(data.subscriptionId ?? "")" + "product=\(data.deviceType ?? "")"
                //
                let url = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "headless=true" + "deviceId=\(data.id ?? "")" + "product=\(data.deviceType ?? "")" + "subscriptionNumber=\(data.subscriptionId ?? "")"
                urlRenewSubscription = url
            } else {
                //live strean and manage button are shown
                btnLiveStream.isHidden = false
                btnClearFilter.isHidden = false
                btnManageSubcription.isHidden = false
                if data.deviceType == "WSD" {
                    btnLiveStream.isHidden = true
                    btnManageSubcription.isHidden = true
                }
                let data = arrDevices[indexPath.row]
                device = data
                self.getAllAlertsFromServer(param: param)
            }
        }
        UIView.performWithoutAnimation {
            collectionView.reloadData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            collectionView.scrollToItem(at: IndexPath(item: self.selectedCollectionIndex, section: 0), at: .right, animated: false)
        }
    }
    
    func convertDateFormat(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let oldDate = olDateFormatter.date(from: inputDate)
        
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "dd MMM yyyy"
        
        return convertDateFormatter.string(from: oldDate!)
    }
}

@available(iOS 13.0, *)
extension HomeVC: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
}



// MARK:- Custom Delegate
@available(iOS 13.0, *)
extension HomeVC : HomeFilterVCDelegate{
    
    func selectedData(fromDate: String, toDate: String, isReadAlert:Bool, isBookMarked: Bool, isCorrect: Bool, location: String, arrSelected: [Int], isUnreadAlert: Bool) {
        let deviceType = (self.arrDevices.first?.deviceType == "TEV") ? "TEV" : "WSD"
        self.offset = 0        
        
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = "yyyy-MM-dd"
        dfmatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dfmatter.date(from: fromDate )
        let dateStamp:TimeInterval = date?.timeIntervalSince1970 ?? 0.0
        let fromDate:Int = Int(dateStamp)
        print(fromDate)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date1 = dateFormatter.date(from: "\(toDate)T23:59:00")
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date1!)
        let dateTO = dateFormatter.date(from: timeStamp)
        let dateStampTo:TimeInterval = dateTO?.timeIntervalSince1970 ?? 0.0
        let toDate:Int = Int(dateStampTo)
        print(toDate)
        
        var param = ["take": self.limit,
                     "skip": self.offset,
                     "startDate": fromDate,
                     "endDate": toDate] as [String:Any]
        
        if isReadAlert {
            param.updateValue(true, forKey: "acknowledged")
        }
        if isUnreadAlert {
            param.updateValue(false, forKey: "acknowledged")
        }
        if isBookMarked {
            param.updateValue(true, forKey: "isBookMarked")
        }
        if isCorrect{
            param.updateValue(false, forKey: "isCorrect")
        }
        if location != "" {
            param.updateValue(location, forKey: "locationId")
        }
        if !arrSelected.isEmpty {
            param.updateValue(arrSelected, forKey: "alertType")
        }
        
        self.filterParam = param
        self.isFilterActivated = true
        self.btnLiveStream.isHidden = true
        self.btnClearFilter.isHidden = false
        self.btnManageSubcription.isHidden = true
        self.getAllAlertsFromServer(param: param)
        self.dismiss(animated: true)
    }
    
    func dateStamp (dateStamp : String?) -> Int {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = "yyyy-MM-ddT00:00:00+0000"
        dfmatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dfmatter.date(from: dateStamp ?? "")
        let dateStamp:TimeInterval = date?.timeIntervalSince1970 ?? 0.0
        let dateSt:Int = Int(dateStamp)
        print(dateSt)
        return dateSt
    }
    
    func dateStampEnd (dateStamp : String?) -> Int {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = "yyyy-MM-dd T23:59:00+0000"
        dfmatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dfmatter.date(from: dateStamp ?? "")
        let dateStamp:TimeInterval = date?.timeIntervalSince1970 ?? 0.0
        let dateSt:Int = Int(dateStamp)
        print(dateSt)
        return dateSt
    }
    
    func dateTimeInterval (dateStamp : String?) -> Int {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd"
        let date = dfmatter.date(from: dateStamp ?? "")
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt:Int = Int(dateStamp)
        print(dateSt)
        return dateSt
    }
}

// MARK:- Custom Delegate
@available(iOS 13.0, *)
extension HomeVC : ReportVCDelegate{
    
    func success(text:String?){
        self.arrAlerts[reportTag].isCorrect = false
        self.arrAlerts[reportTag].comment = text
        self.tblAlert.reloadRows(at: [IndexPath(row: reportTag, section: 0)], with: .automatic)
    }
}

@available(iOS 13.0, *)
extension HomeVC: SignalRClientDelegate {
    func clientDidConnectSuccess(client: SignalRClient) {
        print(client)
    }
    
    func clientDidConnectFailed(client: SignalRClient, error: Error) {
        print(client)
    }
    
    func clientDidClosed(client: SignalRClient, error: Error?) {
        print(client)
    }
}

//MARK: - API Calls
@available(iOS 13.0, *)
extension HomeVC {
    
    func getAllAlertsFromServer(param:[String:Any])  {
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.getAllAlerts, andParam: param, showHud: true) { (responseDict) in
            
            Helper.UI {
                self.isloadMore = true
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]],
                   !arrTempData.isEmpty {
                    if self.offset == 0 {
                        self.arrAlerts.removeAll()
                    }
                    for dictData in arrTempData{
                        let objData = HomeModel.init(alertId: dictData["alertId"] as? String, alertType: dictData["alertType"] as? String, imageUrl: dictData["imageUrl"] as? String, occurenceTimeStamp: dictData["occurenceTimeStamp"] as? Int, deviceName: dictData["deviceName"] as? String, deviceId: dictData["deviceId"] as? String, locationName: dictData["locationName"] as? String, locationId: dictData["locationId"] as? String, acknowledged: dictData["acknowledged"] as? Bool ?? false, bookMarked: dictData["bookMarked"] as? Bool ?? false, isCorrect: dictData["isCorrect"] as? Bool ?? false, comment: dictData["comment"] as? String, videoUrl: dictData["videoUrl"] as? String, device: dictData["device"] as? String, smokeValue: dictData["smokeValue"] as? Int)
                        self.arrAlerts.append(objData)
                    }
                    self.reload()
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    if self.offset == 0 {
                        self.arrAlerts.removeAll()
                    }
                    self.isloadMore = false
                    self.reload()
                }
            }
        }
    }
    
    func getDevicesFromServer(deviceType: String)  {
      
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getDevices, andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                var allDevices = [DeviceModel]()
                var tevDevices = [DeviceModel]()
                var wsdDevices = [DeviceModel]()
                
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    self.arrDevices.removeAll()
                    for dictData in arrTempData{
                        let objData = DeviceModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, connected: dictData["connected"] as? Bool ?? false, locationId: dictData["locationId"] as? String, locationName: dictData["locationName"] as? String, macAddress: dictData["macAddress"] as? String, firmwareVersion: dictData["firmwareVersion"] as? String, wifiName: dictData["wifiName"] as? String, subscriptionId: dictData["subscriptionId"] as? String, availableFeatures: dictData["availableFeatures"] as? [String], subscriptionExpiryDate: dictData["subscriptionExpiryDate"] as? String, currentUserPermission: dictData["currentUserPermission"] as? String, devicePermissions: dictData["devicePermissions"] as? [[String:Any]], deviceType: dictData["deviceType"] as? String, subscriptionStatus: dictData["subscriptionStatus"] as? String, disabled: dictData["disabled"] as? Bool ?? false, unacknowledgedAlert: dictData["unacknowledgedAlert"] as? Int ?? 0)
                       
                        
                        if objData.deviceType == "TEV" {
                            tevDevices.append(objData)
                        }else if objData.deviceType == "WSD"{
                            
                            wsdDevices.append(objData)
                        }
                        
                        allDevices.append(objData)
                         
                        if tevDevices.count > 0 && wsdDevices.count > 0{
                            self.scrollView.isHidden = false
                            self.topConstraint.constant = 70
                        }else{
                            self.scrollView.isHidden = true
                            self.topConstraint.constant = 20
                        }
                    
                    }
                    if deviceType == "TEV" {
                        self.devicesTypesForFilter = "TEV"
                        self.arrDevices.append(contentsOf: tevDevices)
                    }else if deviceType == "WSD" {
                        self.devicesTypesForFilter = "WSD"
                        self.arrDevices.append(contentsOf: wsdDevices)
                    }else{
                        self.devicesTypesForFilter = "TEV,WSD"
                        self.arrDevices.append(contentsOf: allDevices)
                    }
                    
                    if self.arrDevices.count > 0 {
                        self.topTableConstraint.constant = 30
                        self.scrollView.isHidden = false
                        self.heightConstraint.constant = 50
                    }else{
                        self.topTableConstraint.constant = -50
                        self.scrollView.isHidden = true
                        self.heightConstraint.constant = 0
                    }
                    
                    self.collectionDevice.reloadData()
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    
                    if errorMessage == "Session expiration time out"{
                        ApplicationPreference.clearAllData()
                        let objVC : LoginVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: LoginVC.nameOfClass)
                        self.navigationController?.pushViewController(objVC, animated: false)
                        Helper.sharedInstance.showToast(isError: false, title: "session expiration time out")
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                    }

                    
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func bookmarkAlertsFromServer(alertId: String, selctedIndex:Int)  {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.bookmarkAlert+alertId, andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                self.reloadAlertAt(index: selctedIndex, action: .bookmark)
            }
        }
    }
    
    private func startVideoStreamFromServer(deviceId:String)  {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.startVideoStraming(deviceId: deviceId), andParam: [:], showHud: true) { (responseDict) in
            print("responseDict ---> \(responseDict)")
            Helper.UI {
                guard let data = responseDict["responseBody"] as? [String: Any] else { return }
                if let url = data["signalRNegotiateUrl"] as? String, let seqNumber = data["stopSequenceNumber"] as? Int {
                    ApplicationPreference.saveSignalRNegotiateUrl(url: url)
                    ApplicationPreference.saveStopSequenceNumber(number: seqNumber)
                    self.startVideoStreaming(deviceId: deviceId, seqNumber: seqNumber)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    private func acknowledgeFromServer(alertId:String, index:Int) {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getAcknowledge+alertId, andParam: [:], showHud: false) { (responseDict) in
            Helper.UI {
                self.reloadAlertAt(index: index, action: .acknowledge)
            }
        }
    }
}

typealias UnixTimestamp = Int

extension Date {
    // date to timestamp
    var unixTimestamp: UnixTimestamp {
        return UnixTimestamp(self.timeIntervalSince1970 * 1_000) // millisecond precision
    }
}

extension UnixTimestamp {
    // timestamp to date
    var dateObject: Date {
        return Date(timeIntervalSince1970: TimeInterval(self / 1_000)) // must take a millisecond-precision unix timestamp
    }
}
