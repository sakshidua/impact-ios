//
//  PhotoVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 30/05/21.
//

import UIKit

@available(iOS 13.0, *)
class PhotoVC: BaseVC, UIScrollViewDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imageUrl: String!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }
    
    private func setup() {
        scrollView.delegate = self
        self.scrollView.clipsToBounds = true
        self.scrollView.zoomScale = 1.0
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 5.0
        if imageUrl != nil {
            imageView.sd_setImage(with: URL(string: imageUrl ?? ""), placeholderImage: UIImage(named: "picture"))
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    @IBAction func actionDismiss(sender: UIButton) {
        self.dismissController()
    }
}
