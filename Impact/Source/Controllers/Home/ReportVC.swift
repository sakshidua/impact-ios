//
//  ReportVC.swift
//  Impact
//
//  Created by Apple on 04/05/21.
//

import UIKit

protocol ReportVCDelegate {
    func success(text:String?)
}

@available(iOS 13.0, *)
class ReportVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var txtViewReport : UITextView!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var alertId : String = ""
    var alertText : String = ""
    var delegate: ReportVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    private func setup() {
        txtViewReport.text = alertText
        txtViewReport.isUserInteractionEnabled = false
        btnSave.isHidden = true
        if alertText.count == 0 || alertText.isEmpty || alertText == "" {
            txtViewReport.isUserInteractionEnabled = true
            btnSave.isHidden = false
        }
    }

    func reportIncorrectFromServer()  {
        let param = ["alertId":self.alertId,
                     "comment": self.txtViewReport.text!]
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.reportIncorrect, andParam: param, showHud: true) { (responseDict) in
           
            Helper.UI {
              
                if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                    self.dismiss(animated: true)
                } else if let successMessage = responseDict["successMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: successMessage)
                    self.delegate?.success(text: self.txtViewReport.text!)
                    self.dismiss(animated: true)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                
            }
        }
    }
}


//MARK:- Action Method
@available(iOS 13.0, *)
extension ReportVC {
    
    @IBAction func actionCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionSave(_ sender: UIButton){
        self.view.endEditing(true)
        self.txtViewReport.text = self.txtViewReport.text.trim()
        if !self.txtViewReport.text.isEmpty{
            self.reportIncorrectFromServer()
        }
    }
}


