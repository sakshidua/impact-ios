//
//  SideMenuVC.swift
//  Impact
//
//  Created by Apple on 02/05/21.
//

import UIKit
import SideMenu

@available(iOS 13.0, *)
class SideMenuVC: BaseVC {
    
    @IBOutlet weak var tblSideMenu : UITableView!
    var arrSideMenu = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    func setInitialData(){
        var dict = [String:Any]()
        dict.updateValue("Home", forKey: "title")
        dict.updateValue("home", forKey: "icon")
        self.arrSideMenu.append(dict)
        
        dict.updateValue("Devices", forKey: "title")
        dict.updateValue("device", forKey: "icon")
        self.arrSideMenu.append(dict)
        
        dict.updateValue("Setup a Device", forKey: "title")
        dict.updateValue("add_circle", forKey: "icon")
        self.arrSideMenu.append(dict)
        
        dict.updateValue("Account", forKey: "title")
        dict.updateValue("person", forKey: "icon")
        self.arrSideMenu.append(dict)
        
        dict.updateValue("Help Center", forKey: "title")
        dict.updateValue("phone_in_talk", forKey: "icon")
        self.arrSideMenu.append(dict)
        
        self.tblSideMenu.reloadData()
        
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let menu = navigationController as? SideMenuNavigationController, menu.blurEffectStyle == nil else {
            return
        }
     
    }

}


//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension SideMenuVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrSideMenu[indexPath.row]
            
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
       
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
         let obj = self.arrSideMenu[indexPath.row]
        if obj["title"] as? String ?? "" == "Home" {
            let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "Devices"{
            let objVC : DevicesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DevicesVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "Setup a Device"{
            let objVC : SetupDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: SetupDeviceVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "Account"{
            let objVC : AccountVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: AccountVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else{
            let objVC : HelpCenterVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: HelpCenterVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
            
    }
    
}
