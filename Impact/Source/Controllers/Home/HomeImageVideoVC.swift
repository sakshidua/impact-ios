import UIKit
import AVKit
import Photos
import AVFoundation

@available(iOS 13.0, *)
class HomeImageVideoVC: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dotImage: UIImageView!
    @IBOutlet weak var playImage: UIImageView!
    
    var url: [String] = []
    fileprivate var photoLibrary: PHPhotoLibrary?
    fileprivate var collectionViewCell: CollectionViewCell?
    fileprivate var repeatVideoImage = UIImage(named: "refresh")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setup() {
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        photoLibrary = PHPhotoLibrary.shared()
        pageControl.numberOfPages = 2
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    @objc func playerDidFinishPlaying() {
        if self.collectionViewCell != nil {
            self.collectionViewCell?.btnplay.isHidden = false
            self.collectionViewCell?.btnplay.setImage(repeatVideoImage, for: .normal)
        }
    }
    
    private func playVideoInCustomView(playerView:UIView, url:String) {
        let videoURL = URL(string: url)
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = playerView.frame
        playerView.layer.addSublayer(playerLayer)
        player.play()
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func actionDwonload(_ sender: UIButton) {
        
        Spinner.show("")
        if pageControl.currentPage == 0 {
            self.saveToLibraryPhoto()
        }else{
            self.saveToLibraryVideo()
        }
    }
    
    @IBAction func actionPlayVideo(_ sender: UIButton) {
        
        guard let cell = collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? CollectionViewCell else { return }
        self.collectionViewCell = cell
        cell.viewPlay.isHidden = false
        cell.btnplay.isHidden = true
        playVideoInCustomView(playerView: cell.viewPlay, url: url[1])
    }
    
    @IBAction func actionShare(_ sender: UIButton){
        
        self.view.endEditing(true)
        if pageControl.currentPage == 0 {
            Spinner.show("")
            let data = self.url[0]
            if let url = URL(string: data),
               let data = try? Data(contentsOf: url),
               let image = UIImage(data: data) {
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let activityItem: [AnyObject] = [image as AnyObject]
                let avc = UIActivityViewController(activityItems: activityItem as [AnyObject], applicationActivities: nil)
                Spinner.hide()
                self.present(avc, animated: true, completion: nil)
            }
        }else{
            print("Video Share")
            Spinner.show("")
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: self.url[1]),
                   let urlData = NSData(contentsOf: url) {
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                    let filePath="\(documentsPath)/tempFileq1.mp4"
                    DispatchQueue.main.async {
                        urlData.write(toFile: filePath, atomically: true)
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                        }) { completed, error in
                            if completed {
                                
                        
                                print("1")
                             
                            }
                            print("2")
                            
                            DispatchQueue.main.async {
                                let videoURL = NSURL(fileURLWithPath:filePath)
                                print(videoURL)
                                let activityItems = [videoURL, "" ] as [Any]
                                let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
//                                    activityController.popoverPresentationController?.sourceView = self.view
//                                    activityController.popoverPresentationController?.sourceRect = self.view.frame
                                self.present(activityController, animated: true, completion: nil)
                                Spinner.hide()
                                
                            }
                        }
                        print("3")
                    }
                    print("4")
                }
                print("5")
            }
            print("6")
        }
        print("7")
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "Test1"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return "Test2"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "Secret message"
    }
    
    func filePath(filePath : String?) {
        
        let activityItems = [filePath!, "" ] as [Any]
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        activityController.popoverPresentationController?.sourceRect = self.view.frame
        self.present(activityController, animated: true, completion: nil)
    }
    
}

//MARK: UICollectionView DataSource & Delegate Methods
@available(iOS 13.0, *)
extension HomeImageVideoVC:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as? CollectionViewCell {
            if indexPath.row == 0 {
                self.collectionViewCell = nil
                cell.btnplay.isHidden = true
                cell.viewPlay.isHidden = true
                cell.image.sd_setImage(with: URL(string: url[indexPath.item]), placeholderImage: UIImage(named: "picture"))
            } else {
                cell.image.isHidden = true
                cell.btnplay.isHidden = false
                cell.viewPlay.isHidden = false
            }
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            let objVC : PhotoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: PhotoVC.nameOfClass)
            objVC.imageUrl = url[indexPath.item]
            objVC.modalTransitionStyle = .crossDissolve
            objVC.modalPresentationStyle = .overFullScreen
            objVC.transitioningDelegate = self
            self.present(objVC, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = collectionView.contentOffset.x/collectionView.frame.size.width
        pageControl.currentPage = Int(page)
        if Int(page) == 0 {
            dotImage.image = UIImage.init(named: "red_dot")
            playImage.image = UIImage.init(named: "white_play")
        }else{
            dotImage.image = UIImage.init(named: "white_dot")
            playImage.image = UIImage.init(named: "play_red")
        }
    }
    
    //Download Video
    func saveToLibraryVideo(){
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: self.url[1]),
               let urlData = NSData(contentsOf: url) {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile.mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, error in
                        if completed {
                            Spinner.hide()
                            Helper.sharedInstance.showToast(isError: false, title: "Video is saved")
                        }else{
                            Spinner.hide()
                        }
                    }
                }
            }
        }
    }
    
    //Download Photo
    func saveToLibraryPhoto(){
        
        if let url = URL(string: url[0]),
           let data = try? Data(contentsOf: url),
           let image = UIImage(data: data) {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            Helper.sharedInstance.showToast(isError: false, title: "Photo is saved")
            Spinner.hide()
        }
    }
}
