//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit


@available(iOS 13.0, *)
class LocationManagementVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    
    var arrDeviceType = [[String:Any]]()
    var arrLocation = [LocationModel]()
    var isEdit : Bool = false
    var index : Int = 0
    var editText : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setInitialData()
    }
    
    func setInitialData(){
        
        self.getLocationFromServer()
        
        var dict = [String:Any]()
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        self.tblDeviceType.reloadData()
        
    }
    
    func getLocationFromServer()  {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getLocation, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                self.arrLocation.removeAll()
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    for dictData in arrTempData{
                        let objData = LocationModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
                        self.arrLocation.append(objData)
                    }
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.tblDeviceType.reloadData()
            }
        }
    }
    
    @IBAction func actionAddLocation(_ sender: UIButton){
        self.view.endEditing(true)
        let objVC : AddLocationVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: AddLocationVC.nameOfClass)
        objVC.delegate = self
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
    }
    
    
    
    
}

//MARK: -
@available(iOS 13.0, *)
extension LocationManagementVC : AddLocationVCDelegate {
    func successMessage() {
        self.getLocationFromServer()
    }
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension LocationManagementVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrLocation.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            if isEdit == false {
                
                cell.lblTitle.isHidden = false
                cell.txtEdit.isHidden = true
                cell.btnRight.isHidden = true
                cell.btnCancel.isHidden = true
                cell.btnEdit.isHidden = false
                
                let obj = self.self.arrLocation[indexPath.row]
                cell.btnEdit.tag = indexPath.row
                cell.lblTitle.text = obj.name
                cell.btnEdit.addTarget(self, action: #selector(editCellAction), for: .touchUpInside)
                
            }else{
                
                if index == indexPath.row {
                    let obj = self.arrLocation[index]
                    cell.txtEdit.text = obj.name
                    
                    cell.lblTitle.isHidden = true
                    cell.txtEdit.isHidden = false
                    cell.btnRight.isHidden = false
                    cell.btnCancel.isHidden = false
                    cell.btnEdit.isHidden = true
                    
                    cell.btnRight.addTarget(self, action: #selector(RightCellAction), for: .touchUpInside)
                    cell.btnCancel.addTarget(self, action: #selector(CancelCellAction), for: .touchUpInside)
                }else{
                    cell.lblTitle.isHidden = false
                    cell.txtEdit.isHidden = true
                    cell.btnRight.isHidden = true
                    cell.btnCancel.isHidden = true
                    cell.btnEdit.isHidden = false
                }
            }
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    @objc func editCellAction(sender: UIButton!) {
        index = (sender).tag
        isEdit = true
        tblDeviceType.reloadData()
    }
    
    @objc func RightCellAction(sender: UIButton!) {
        
        isEdit = false
        //Update Location
        let obj = self.arrLocation[index]
        self.locationUpdate(locationId: obj.id)
        
    }
    
    @objc func CancelCellAction(sender: UIButton!) {
        isEdit = false
        tblDeviceType.reloadData()
    }
    
    func locationUpdate (locationId:String?) {
        
        let param = ["locationName":editText,
                     "locationId":locationId]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.locationUpdate, andParam: param as [String : Any], showHud: true, changeBaseUrl: false) { (responseDict) in
            
            Helper.UI {
                self.getLocationFromServer()
                
                if let successMessage = responseDict["successMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: successMessage)
                    self.dismiss(animated: true)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}


@available(iOS 13.0, *)
extension LocationManagementVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        editText = textField.text ?? ""
        let maxLength = 15
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
      
    }
    
}

