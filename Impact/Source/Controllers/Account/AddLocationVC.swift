//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit

protocol AddLocationVCDelegate:AnyObject {
    func successMessage()
}


@available(iOS 13.0, *)
class AddLocationVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var txtAddLocation : UITextField!
    weak var delegate : AddLocationVCDelegate?
    
    var arrSteps = ["Insert the battery in 'Wifi Smoke Detector' product", "Please use Impact App Only on one mobile phone", "Make sure 'Wifi Smoke Detector' is within 30 feet of impact App"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func setInitialData(){
       
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtAddLocation.text = txtAddLocation.text?.trim()
        
        if (txtAddLocation.text?.isEmpty)! {
            message = "Please enter the location."
            isFound = false
        }
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }

}

//MARK:- Action Method
@available(iOS 13.0, *)
extension AddLocationVC {
    
    @IBAction func actionYes(_ sender: UIButton){
        self.view.endEditing(true)
        
        if self.isValidFields() {
            
            let param = ["locationName":self.txtAddLocation.text!]
            
            APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.locationAdd, andParam: param, showHud: true) { (responseDict) in
                
                Helper.UI {
                    self.delegate?.successMessage()
                    self.txtAddLocation.text = ""
//                    let message = responseDict["successMessage"] as? String ?? ""
//                    Helper.sharedInstance.showToast(isError: false, title: message)
//                    self.dismiss(animated: true)
                    var msg = ""
                    if let errorMessage = responseDict["errorMessage"] as? String {
                        msg = errorMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                        self.dismiss(animated: true)
                    } else if let successMessage = responseDict["successMessage"] as? String {
                        msg = successMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                        self.dismiss(animated: true)
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                    }                   
                }
            }
        }
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)

    }
}




//MARK:- Action Method
@available(iOS 13.0, *)
extension AddLocationVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == txtAddLocation {
            let maxLength = 15
            let currentString: NSString = txtAddLocation.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else{
            return true
        }
    }
    
}
