//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit


@available(iOS 13.0, *)
class AppPrefererncesVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    var arrDeviceType = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        var dict = [String:Any]()
        
        dict.updateValue("Push Notification", forKey: "title")
        dict.updateValue("notification", forKey: "icon")
        self.arrDeviceType.append(dict)
        
//        dict.updateValue("Auto Media Download", forKey: "title")
//        dict.updateValue("autoDownload", forKey: "icon")
//        self.arrDeviceType.append(dict)
        
        self.tblDeviceType.reloadData()
    }
    
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension AppPrefererncesVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeviceType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrDeviceType[indexPath.row]
            
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
            cell.switchOnOff.tag = indexPath.row
            if indexPath.row == 0 {
              
                if ApplicationPreference.getSwitchOnOff() == "Off" {
                    ApplicationPreference.saveSwitchOnOff(string: "Off")
                    cell.switchOnOff.setOn(false, animated: false)
                }else{
                    ApplicationPreference.saveSwitchOnOff(string: "On")
                    cell.switchOnOff.setOn(true, animated: false)
                }
                
            }
            cell.switchOnOff.addTarget(self, action: #selector(switchStateDidChange(_:)), for: .valueChanged)
            
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func switchStateDidChange(_ sender:UISwitch){
        
        let tag = sender.tag
        
        if tag == 0 {
            if (sender.isOn == true){
                self.notificationSubscribe()
            }
            else{
                self.notificationUnsubscribe()
            }
        }else{
            
            if (sender.isOn == true){
                print("UISwitch state is now ON")
            }
            else{
                print("UISwitch state is now Off")
            }
        }
        
    }
    
    func notificationSubscribe () {
        
        let param = ["fcmToken":ApplicationPreference.getFCMToken(),
                     "application":"TEV"]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.subscribe, andParam: param as [String : Any], showHud: true, changeBaseUrl: false) { (responseDict) in
            
            Helper.UI {
                
                if let successMessage = responseDict["successMessage"] as? String {
                    ApplicationPreference.saveSwitchOnOff(string: "On")
                    Helper.sharedInstance.showToast(isError: false, title: successMessage)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    
    func notificationUnsubscribe () {
        
        let param = ["fcmToken":ApplicationPreference.getFCMToken(),
                     "application":"TEV"]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.unsubscribe, andParam: param as [String : Any], showHud: true, changeBaseUrl: false) { (responseDict) in
            
            Helper.UI {
                
                if let successMessage = responseDict["successMessage"] as? String {
                    ApplicationPreference.saveSwitchOnOff(string: "Off")
                    Helper.sharedInstance.showToast(isError: false, title: successMessage)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}





