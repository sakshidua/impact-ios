//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit
import Photos
import Alamofire

@available(iOS 13.0, *)
class MyProfileVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var btnProfile : UIButton!
    let imagePicker = UIImagePickerController()
    var arrSteps = ["Insert the battery in 'Wifi Smoke Detector' product", "Please use Impact App Only on one mobile phone", "Make sure 'Wifi Smoke Detector' is within 30 feet of impact App"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setInitialData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.imagePicker.delegate = self
        self.txtName.text = txtName.text?.trim()
        self.txtLastName.text = self.txtLastName.text?.trim()
        self.txtEmail.text = self.txtEmail.text?.trim()
        self.txtPhone.text = self.txtPhone.text?.trim()
        let validPhone = txtPhone.text?.isValidPhoneNumber()
        
        if (txtName.text?.isEmpty)! {
            message = "Please enter the name."
            isFound = false
        } else  if (txtLastName.text?.isEmpty)! {
            message = "Please enter last name."
            isFound = false
        }  else if (txtEmail.text?.isEmpty)! {
            message = "Please enter the email."
            isFound = false
        } else if (txtPhone.text?.isEmpty)! {
            message = "Please enter the phone number."
            isFound = false
        }else if (validPhone == false) {
            message = "Please enter the valid phone number."
            isFound = false
        }
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }
    
    func setInitialData(){
        self.getUserDetails()
    }
    
    func getUserDetails()  {
        //call API userDetails
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.userDetails, andParam: [:], showHud: true, changeBaseUrl: true) { (responseDict) in
            Helper.UI {
                print(responseDict["orgId"] as? String ?? "")
                
                self.txtName.text = responseDict["firstName"] as? String ?? ""
                self.txtLastName.text = responseDict["lastName"] as? String ?? ""
                self.txtEmail.text = responseDict["email"] as? String ?? ""
                self.txtPhone.text = responseDict["phoneNumber"] as? String ?? ""
                
                DispatchQueue.global(qos: .background).async {
                    self.btnProfile.sd_setImage(with: URL(string: responseDict["pictureUrl"] as? String ?? ""), for: .normal, placeholderImage: UIImage(named: ""))
                }
            }
        }
    }
    
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension MyProfileVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == txtPhone {
            let maxLength = 10
            let currentString: NSString = txtPhone.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else{
            return true
        }
    }
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension MyProfileVC {
    
    @IBAction func actionProfile(_ sender: UIButton){
        DispatchQueue.main.async {
            self.OpenGalleyandCamra()
        }
    }
    
    @IBAction func actionYes(_ sender: UIButton){
        self.view.endEditing(true)
        
        if self.isValidFields() {
            
            let param = ["firstName":self.txtName.text!,
                         "lastName": self.txtLastName.text!,
                         "phoneNumber": self.txtPhone.text!]
            
            APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.updateProfile, andParam: param, showHud: true, changeBaseUrl: true) { (responseDict) in
                
                Helper.UI {
//                    let message = responseDict["successMessage"] as? String ?? ""
//                    Helper.sharedInstance.showToast(isError: false, title: message)
                    var msg = ""
                    if let errorMessage = responseDict["errorMessage"] as? String {
                        msg = errorMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                    } else if let successMessage = responseDict["successMessage"] as? String {
                        msg = successMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                    }
                }
            }
        }
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UIImagePickerControllerDelegate Methods
@available(iOS 13.0, *)
extension MyProfileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
                
        if let editedImage = info[.editedImage] as? UIImage {
            self.btnProfile.setImage(editedImage, for: .normal)
            self.uploadProfileData(image: editedImage)
        } else if let originalImage = info[.originalImage] as? UIImage {
            self.btnProfile.setImage(originalImage, for: .normal)
            self.uploadProfileData(image: originalImage)
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    private func imagePickerControllerDidCancel(picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        DispatchQueue.main.async {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func openGallary() {
        
        DispatchQueue.main.async {
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image"]
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    //Camra and Gallery
    func OpenGalleyandCamra(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.selectImageFromCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default){
            UIAlertAction in
            self.checkPhotoLibraryPermission()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectImageFromCamera() //to Access Camera
    {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.denied {
            
            let alert = UIAlertController(title: "Unable to access the Camera",
                                          message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            present(alert, animated: true, completion: nil)
        }
        else if (authStatus == AVAuthorizationStatus.notDetermined) {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    }
                }
            })
        } else {
            self.openCamera()
        }
        
    }
    
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            //handle authorized status
            self.openGallary()
        case .denied, .restricted :
            //handle denied status
            let alert = UIAlertController(title: "Unable to access the Gallary",
                                          message: "To enable access, go to Settings > Privacy > Gallary and turn on Gallary access for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                    })
                }
            })
            alert.addAction(settingsAction)
            
            self.present(alert, animated: true, completion: nil)
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized:
                    self.openGallary()
                    break
                case .denied, .restricted: break
                case .notDetermined: break
                case .limited:
                    break
                @unknown default:
                    break
                }
            }
        case .limited:
            break
        @unknown default:
            break
        }
    }
    
    func uploadProfileData (image: UIImage) {
        
        let token = ApplicationPreference.getAccessToken() ?? ""
        let bearerToken: String = "Bearer " + (token)
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Authorization" : bearerToken
        ]
        print(SERVICE_URL_ONBOARD+APIEndPoint.uploadProfilePicture)
        AF.upload(multipartFormData: { multipartFormData in
            
            let imageData: Data = (image.pngData()! as NSData) as Data
            multipartFormData.append(imageData, withName: "file", fileName: "file.png", mimeType: "image/png")
        }, to: SERVICE_URL_ONBOARD+APIEndPoint.uploadProfilePicture, method: .post , headers: headers)
        .responseJSON(completionHandler: { (responseData) in
            
            if let err = responseData.error{
                print(err)
                return
            }
            print("Succesfully uploaded")
            let json = responseData.data
            
            if (json != nil) {
                print(json ?? "")
            }else{
                Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
            }
        })
    }
}
