//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit

protocol SignOutVCDelegate:AnyObject {
    func dismiss()
}


@available(iOS 13.0, *)
class SignOutVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    weak var delegate : SignOutVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension SignOutVC {
    
    @IBAction func actionYes(_ sender: UIButton){
        self.view.endEditing(true)
        ApplicationPreference.clearAllData()
        self.dismiss(animated: true)
        delegate?.dismiss()
        /* let param = ["fcmToken": "",
         "application": "TEV"]
         
         APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.notificationUnsubscribe, andParam: param, showHud: true) { (responseDict) in
         
         Helper.UI {
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         appDelegate.loginVC()
         //let message = responseDict["successMessage"] as? String ?? ""
         // Helper.sharedInstance.showToast(isError: false, title: message)
         self.dismiss(animated: true)
         }
         }
         */
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
}

