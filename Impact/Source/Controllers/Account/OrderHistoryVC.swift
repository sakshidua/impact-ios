//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit
import PDFKit
import ClockKit
import MobileCoreServices
import Alamofire

@available(iOS 13.0, *)
class OrderHistoryVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    var pdfView = PDFView()
    var arrDeviceType = [[String:Any]]()
    var arrSubscriptionHistory = [SubscriptionHistoryModel]()
    var invoiceDownload : Int = 0
    var invoiceDataPDF : String = ""
    var pdfFilename: String!
    var containerUrl: URL? {
        return FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents")
    }
    
    var query: NSMetadataQuery!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        
        self.getSubscriptionHistory()
        
        var dict = [String:Any]()
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("On106", forKey: "title")
        dict.updateValue("smoke_detector", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        self.tblDeviceType.reloadData()
        
    }
    
    func getSubscriptionHistory()  {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.subscriptionHistory, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    print(arrTempData)
                    for dictData in arrTempData{
                        //  let objData = SubscriptionHistoryModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
                        let objData = SubscriptionHistoryModel.init(id: (dictData["id"] as? Int), subscriptionId: dictData["subscriptionId"] as? String,
                                                                    productName: dictData["productName"] as? String,
                                                                    deviceId: dictData["deviceId"] as? String,
                                                                    deviceName: dictData["deviceName"] as? String,
                                                                    status: dictData["status"] as? String,
                                                                    createdTime: dictData["createdTime"] as? String,
                                                                    eventType: dictData["eventType"] as? String,
                                                                    planCode: dictData["planCode"] as? String,
                                                                    planName: dictData["planName"] as? String,
                                                                    planPrice: dictData["planPrice"] as? String,
                                                                    descriptions: dictData["description"] as? String,
                                                                    subTotal: dictData["subTotal"] as? String,
                                                                    amount: (dictData["amount"] as? Int),
                                                                    cgstName: dictData["cgstName"] as? String,
                                                                    sgstName: dictData["sgstName"] as? String,
                                                                    cgstAmount: dictData["cgstAmount"] as? String,
                                                                    taxPercentage: dictData["taxPercentage"] as? String,
                                                                    email: dictData["email"] as? String,
                                                                    invoiceId: (dictData["invoiceId"] as? Int),
                                                                    interval: (dictData["interval"] as? Int)!,
                                                                    intervalUnit: dictData["intervalUnit"] as? String,
                                                                    currency: dictData["currency"] as? String,
                                                                    orgId: dictData["orgId"] as? String,
                                                                    nextBillingAt: dictData["nextBillingAt"] as? String,
                                                                    createdDate: (dictData["createdDate"] as? Int),
                                                                    createdBy: dictData["createdBy"] as? String,
                                                                    modifiedDate: dictData["modifiedDate"] as? String,
                                                                    modifiedBy: dictData["modifiedBy"] as? String,
                                                                    successMessage: dictData["successMessage"] as? String,
                                                                    errorMessage: dictData["errorMessage"] as? String,
                                                                    features: dictData["features"] as? [[String:Any]])
                        self.arrSubscriptionHistory.append(objData)
                        
                    }
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.tblDeviceType.isHidden = false
                self.tblDeviceType.reloadData()
            }
        }
    }
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension OrderHistoryVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubscriptionHistory.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrSubscriptionHistory[indexPath.row]
            
            if let nextBillingAt = obj.nextBillingAt {
                cell.lblFrequency.text = "Renewal date: \(self.convertDateFormats(inputDate: nextBillingAt))"
            }
            
            if let deviceType = obj.productName {
                if deviceType == "TEV" {
                    cell.imgIconSubscription.image = UIImage.init(named: "video_cam")
                }else{
                    cell.imgIconSubscription.image = UIImage.init(named: "WSD_logo")
                }
            }
            
            if let event = obj.eventType {
                var eventType = ""
                if event == "Subscription created" {
                    eventType = "created"
                }else if event == "Subscription cancelled" {
                    cell.lblFrequency.isHidden = true
                    eventType = "cancelled"
                }else if event == "Subscription activation" {
                    eventType = "activation"
                }else if event == "Subscription downgraded" {
                    eventType = "downgraded"
                }else if event == "Subscription deleted" {
                    eventType = "deleted"
                }else if event == "Subscription modified" {
                    eventType = "modified"
                }else if event == "Subscription scheduled cancellation removed"{
                    eventType = "Subscription scheduled cancellation removed"
                }else if event == "Subscription cancellation scheduled"{
                    eventType = "Subscription cancellation scheduled"
                }
                
                if let deviceName = obj.deviceName {
                    cell.lblSubscriptionTitle.text = "Subscription \(eventType.capitalizingFirstLetter()) for \(deviceName)"
                }
                cell.lblPlan.text = "Plan : Promotional Plan"
            }
            
            if let event = obj.eventType {
                var eventType = ""
                if event == "Subscription modified" {
                    eventType = "Modified on:"
                }else if event == "Subscription created" {
                    //  cell.lblFrequency.isHidden = true
                    eventType = "Purchased on:"
                }else if event == "Subscription cancelled" {
                    eventType = "Cancelled on:"
                }else if event == "Subscription expired" {
                    eventType = "Expired on:"
                }else if event == "Subscription cancellation" {
                    eventType = "Scheduled date:"
                }else if event == "Subscription scheduled" {
                    eventType = "Scheduled date:"
                }
                
                if let timeResult = obj.createdDate {
                    
                    let date = Date(timeIntervalSince1970: TimeInterval(timeResult))
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeStyle = DateFormatter.Style.medium
                        dateFormatter.dateStyle = DateFormatter.Style.medium
                        dateFormatter.timeZone = .current
                        let localDate = dateFormatter.string(from: date)
                        print(localDate)
                        cell.lblPurchasedDate.text = "\(eventType.capitalizingFirstLetter()) \(localDate)"
                }
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.arrSubscriptionHistory[indexPath.row]
        let amount : Int = obj.amount ?? 0
        let currency : String = obj.currency ?? ""
        invoiceDownload = obj.invoiceId ?? 0
        
        if let timeResult = obj.createdDate {
            
            let date = Date(timeIntervalSince1970: TimeInterval(timeResult))
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.medium
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeZone = .current
            let localDate = dateFormatter.string(from: date)
            print(localDate)
            let objVC : OrderDetailsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: OrderDetailsVC.nameOfClass)
            objVC.delegate = self
            objVC.orderNoString = obj.id ?? 0
            objVC.orderDateString = localDate//obj.createdTime ?? ""
            objVC.orderTotalString = currency + " " + String(amount)
            objVC.features = obj.features
            objVC.invoiceId = obj.invoiceId ?? 0
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
        }
    }
    
    func convertDateFormat(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let oldDate = olDateFormatter.date(from: inputDate)
        
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "dd MMM yyyy hh:mm:a"
        
        return convertDateFormatter.string(from: oldDate!)
    }
    
    func convertDateFormats(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd"
        
        let oldDate = olDateFormatter.date(from: inputDate)
        
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "dd MMM yyyy"
        
        return convertDateFormatter.string(from: oldDate!)
    }
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension OrderHistoryVC : OrderDetailsVCDelegate {
    
    func loadHTML(_ html:String) {
        print(html.htmlToAttributedString!)
    }
    
    func downloadInvoice(invoceId: String) {
        
        let url = URL(string: SERVICE_URL + APIEndPoint.downloadInvoice + (invoceId))
        FileDownloader.loadFileAsync(url: url!, invoceId: invoceId) { (path, error) in
            print("PDF File downloaded to : \(path!)")
        }
    }
}

class FileDownloader {
    
    static func loadFileAsync(url: URL, invoceId: String, completion: @escaping (String?, Error?) -> Void) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent("\("invoice_"+invoceId).pdf")
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
                                            {
                                                data, response, error in
                                                if error == nil
                                                {
                                                    if let response = response as? HTTPURLResponse
                                                    {
                                                        if response.statusCode == 200
                                                        {
                                                            if let data = data
                                                            {
                                                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                                                {
                                                                    completion(destinationUrl.path, error)
                                                                }
                                                                else
                                                                {
                                                                    completion(destinationUrl.path, error)
                                                                }
                                                            }
                                                            else
                                                            {
                                                                completion(destinationUrl.path, error)
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    completion(destinationUrl.path, error)
                                                }
                                            })
            task.resume()
        }
    }
}
