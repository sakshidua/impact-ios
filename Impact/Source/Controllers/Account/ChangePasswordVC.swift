//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit


@available(iOS 13.0, *)
class ChangePasswordVC: BaseVC {
    
    @IBOutlet weak var txtOldPassword : UITextField!
    @IBOutlet weak var txtNewPassword : UITextField!
    @IBOutlet weak var txtConfirmPassword : UITextField!
    @IBOutlet weak var btnOldShowHide: UIButton!
    @IBOutlet weak var btnNewShowHide: UIButton!
    @IBOutlet weak var btnCOnfirmShowHide: UIButton!
    
    var isOldShowPassword = false
    var isNewShowPassword = false
    var isConfirmShowPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtOldPassword.text = txtOldPassword.text?.trim()
        self.txtNewPassword.text = self.txtNewPassword.text?.trim()
        self.txtConfirmPassword.text = self.txtConfirmPassword.text?.trim()
        
        if (txtOldPassword.text?.isEmpty)! {
            message = "Please enter the name."
            isFound = false
        } else  if (txtNewPassword.text?.isEmpty)! {
            message = "Please enter last name."
            isFound = false
        }  else if (txtConfirmPassword.text?.isEmpty)! {
            message = "Please enter the email."
            isFound = false
        } else if txtNewPassword.text != txtConfirmPassword.text{
            message = "Password and Confirm Password doesn't match."
            isFound = false
        }
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }
    
    func setInitialData(){
        
    }
    
    @IBAction func actionOldPasswordShowHide(_ sender: UIButton){
        if(isOldShowPassword == true) {
            txtOldPassword.isSecureTextEntry = false
            btnOldShowHide.setImage(UIImage.init(named: "visibility_on"), for: .normal)
        } else {
            txtOldPassword.isSecureTextEntry = true
            btnOldShowHide.setImage(UIImage.init(named: "visibility_off"), for: .normal)
        }
        isOldShowPassword = !isOldShowPassword
    }
    
    @IBAction func actionNewPasswordShowHide(_ sender: UIButton){
        if(isNewShowPassword == true) {
            txtNewPassword.isSecureTextEntry = false
            btnNewShowHide.setImage(UIImage.init(named: "visibility_on"), for: .normal)
        } else {
            txtNewPassword.isSecureTextEntry = true
            btnNewShowHide.setImage(UIImage.init(named: "visibility_off"), for: .normal)
        }
        isNewShowPassword = !isNewShowPassword
    }
    
    @IBAction func actionConfirmPasswordShowHide(_ sender: UIButton){
        if(isConfirmShowPassword == true) {
            txtConfirmPassword.isSecureTextEntry = false
            btnCOnfirmShowHide.setImage(UIImage.init(named: "visibility_on"), for: .normal)
        } else {
            txtConfirmPassword.isSecureTextEntry = true
            btnCOnfirmShowHide.setImage(UIImage.init(named: "visibility_off"), for: .normal)
        }
        isConfirmShowPassword = !isConfirmShowPassword
    }
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ChangePasswordVC {
    
    @IBAction func actionYes(_ sender: UIButton){
        self.view.endEditing(true)
        //self.navigationController?.popToRootViewController(animated: false)
        
        if self.isValidFields() {
            
            let param = ["currentPassword":self.txtOldPassword.text!,
                         "newPassword": self.txtNewPassword.text!]
            
            APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.passwordChange, andParam: param, showHud: true, changeBaseUrl: true) { (responseDict) in
                
                Helper.UI {
//                    let message = responseDict["successMessage"] as? String ?? ""
//                    Helper.sharedInstance.showToast(isError: false, title: message)
//                    self.dismiss(animated: true)
                    var msg = ""
                    if let errorMessage = responseDict["errorMessage"] as? String {
                        msg = errorMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                        self.dismiss(animated: true)
                    } else if let successMessage = responseDict["successMessage"] as? String {
                        msg = successMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                        self.dismiss(animated: true)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                    }
                    
                }
            }
        }
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}




