//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit


@available(iOS 13.0, *)
class AccountVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    var arrDeviceType = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
 
    
    
    func setInitialData(){
        var dict = [String:Any]()
        dict.updateValue("My Profile", forKey: "title")
        dict.updateValue("myProfile", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Change Password", forKey: "title")
        dict.updateValue("changePassword", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Location Management", forKey: "title")
        dict.updateValue("locationManagment", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("App Preferernces", forKey: "title")
        dict.updateValue("appPre", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Order History", forKey: "title")
        dict.updateValue("order", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Sign Out", forKey: "title")
        dict.updateValue("signOut", forKey: "icon")
        self.arrDeviceType.append(dict)
      
        self.tblDeviceType.reloadData()
        
    }

}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension AccountVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeviceType.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrDeviceType[indexPath.row]
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
       
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
         let obj = self.arrDeviceType[indexPath.row]
        if obj["title"] as? String ?? "" == "My Profile" {
            let objVC : MyProfileVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: MyProfileVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "Change Password" {
            
            let objVC : ChangePasswordVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: ChangePasswordVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
           
        }else if obj["title"] as? String ?? "" == "Location Management" {
            
            let objVC : LocationManagementVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: LocationManagementVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "App Preferernces" {
            
            let objVC : AppPrefererncesVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: AppPrefererncesVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "Sign Out" {
            let objVC : SignOutVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: SignOutVC.nameOfClass)
            objVC.delegate = self
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
            
        }else if obj["title"] as? String ?? "" == "Order History" {
         
            let objVC : OrderHistoryVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: OrderHistoryVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
        
            
    }
    
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension AccountVC : SignOutVCDelegate {
    
    func dismiss() {
        
        let objVC : LoginVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: LoginVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
}




