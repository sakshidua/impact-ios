//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit


protocol OrderDetailsVCDelegate:AnyObject {
    func downloadInvoice(invoceId: String)
}

@available(iOS 13.0, *)
class OrderDetailsVC: BaseVC {
    
    weak var delegate : OrderDetailsVCDelegate?
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var orderTotal : UILabel!
    @IBOutlet weak var orderNo : UILabel!
    @IBOutlet weak var orderDate : UILabel!
    @IBOutlet weak var orderTbl : UITableView!
    @IBOutlet weak var btnInvoice : UIButton!
    @IBOutlet weak var viewConstant : NSLayoutConstraint!
    
    var arrDeviceType = [[String:Any]]()
    var subscriptionHistoryModel = [SubscriptionHistoryModel]()
    
    var features = [Features]()
    
    var orderTotalString : String = ""
    var orderNoString : Int = 0
    var orderDateString : String = ""
    var invoiceId: Int = 0
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if invoiceId == 0 {
            btnInvoice.isHidden = true
            viewConstant.constant = 140
        }else{
            btnInvoice.isHidden = false
        }
       
        self.orderTotal.text = String(orderTotalString)
        self.orderNo.text = String(orderNoString)
        self.orderDate.text = orderDateString
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    
    func setInitialData(){
        self.orderTbl.separatorStyle = .none
        self.orderTbl.reloadData()
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionDownloadInvoice(_ sender: UIButton){
        self.view.endEditing(true)
        self.delegate?.downloadInvoice(invoceId: "")
        self.dismiss(animated: true)
    }

}


//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension OrderDetailsVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.features.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.features[indexPath.row]
            cell.lblTitle.text = obj.name
       
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
}

