//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit

protocol WifiStepsVCDelegate:AnyObject {
    func dismiss()
}

@available(iOS 13.0, *)
class WifiStepsVC: BaseVC {
    
    @IBOutlet weak var tblSteps : UITableView!
    @IBOutlet weak var viewBottom : UIView!
    weak var delegate : WifiStepsVCDelegate?
    
    var arrSteps = ["Insert the battery in 'Wifi Smoke Detector' product", "Please use Impact App Only on one mobile phone", "Make sure 'Wifi Smoke Detector' is within 30 feet of impact App"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func setInitialData(){
       
    }


}

//MARK:- Action Method
@available(iOS 13.0, *)
extension WifiStepsVC {
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionReady(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
        delegate?.dismiss()
       
        
    }
}



//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension WifiStepsVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSteps.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrSteps[indexPath.row]
            cell.lblTitle.text = obj
            cell.lblCount.text = String(indexPath.row+1)
           
       
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
    
}
