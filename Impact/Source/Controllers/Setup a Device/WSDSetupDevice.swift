//
//  ConnectDeviceVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//
import Foundation
import UIKit
import DropDown
import SystemConfiguration.CaptiveNetwork
import NetworkExtension
import Foundation
import SwiftSocket
import Network

@available(iOS 13.0, *)
class WSDSetupDevice: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var txtChooseDevice : UITextField!
    @IBOutlet weak var txtChooseWifi : UITextField!
    @IBOutlet weak var txtWifiPassword : UITextField!
    @IBOutlet weak var txtWifiConfirmPassword : UITextField!
    @IBOutlet weak var txtLocation : UITextField!
    @IBOutlet weak var txtDeviceName : UITextField!
    @IBOutlet weak var btnWifiShowHidePassword : UIButton!
    @IBOutlet weak var deviceLocationLabelView : UIView!
    @IBOutlet weak var deviceLocationTextView : UIView!
    @IBOutlet weak var deviceNameLabelView : UIView!
    @IBOutlet weak var deviceNameTextView : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnConnect : UIButton!
    @IBOutlet weak var btnConnectPosstion: NSLayoutConstraint!
    @IBOutlet weak var imgLoder : UIImageView!
    @IBOutlet weak var loderView : UIView!
    @IBOutlet weak var lblLoderTitle : UILabel!
    
    var arrLocation = [LocationModel]()
    var arrWifiList = [String]()
    var isShowPassword = false
    var locationId: String?
    var nameVC: String = ""
    var connection: NWConnection?
    
    internal static var COMMON_SOCKET_CUSTOM_PING_EVENT = "t_ping"
    internal static var COMMON_ON_SOCKET_CONNECT_EVENT = "connect"
    internal static var COMMON_ON_SOCKET_CONNECTING_EVENT = "connecting"
    internal static var COMMON_ON_SOCKET_RECONNECT_EVENT = "reconnect"
    internal static var COMMON_ON_SOCKET_DISCONNECT_EVENT = "disconnect"
    internal static var COMMON_ON_SOCKET_ERROR_EVENT = "error"
    
    let socketConnectionModelArrayList: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLocationFromServer()
    }
    
    func setInitialData(){
        
//        txtChooseDevice.text = "TEV_Honeywell"
//        txtChooseWifi.text = "Hemu_5G"
//        txtWifiPassword.text = "ios@HJ123"
//        txtWifiConfirmPassword.text = "ios@HJ123"
//        txtDeviceName.text = "TEV_Honeywell_Device"
        
        if nameVC == "EditWSDDeviceVC" {
            self.btnConnect.setTitle("Update", for: .normal)
            self.lblTitle.text = "Edit Device"
            self.deviceLocationLabelView.isHidden = true
            self.deviceLocationTextView.isHidden = true
            self.deviceNameLabelView.isHidden = true
            self.deviceNameTextView.isHidden = true
            btnConnectPosstion.constant = -100
        }else{
            self.lblTitle.text = "Setup a Device"
            self.btnConnect.setTitle("Connect", for: .normal)
            self.deviceLocationLabelView.isHidden = false
            self.deviceLocationTextView.isHidden = false
            self.deviceNameLabelView.isHidden = false
            self.deviceNameTextView.isHidden = false
            btnConnectPosstion.constant = 50
        }
        
        let ssid = self.getWiFiName()
        print("SSID: \(String(describing: ssid))")
        
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtChooseDevice.text = txtChooseDevice.text?.trim()
        self.txtChooseWifi.text = self.txtChooseWifi.text?.trim()
        self.txtWifiPassword.text = self.txtWifiPassword.text?.trim()
        self.txtLocation.text = self.txtLocation.text?.trim()
        self.txtDeviceName.text = self.txtDeviceName.text?.trim()
        
        
        if nameVC == "EditWSDDeviceVC" {
            
            if (txtChooseDevice.text?.isEmpty)! {
                message = "Please enter the device."
                isFound = false
            } else  if (txtChooseWifi.text?.isEmpty)! {
                message = "Please enter valid wifi name."
                isFound = false
            }  else if (txtWifiPassword.text?.isEmpty)! {
                message = "Please enter the wifi password."
                isFound = false
            } else if (txtWifiConfirmPassword.text?.isEmpty)! {
                message = "Please enter the wifi confirm password."
                isFound = false
            } else if txtWifiPassword.text != txtWifiConfirmPassword.text{
                message = "Password and Confirm Password doesn't match."
                isFound = false
            } 
            
        }else{
            
            if (txtChooseDevice.text?.isEmpty)! {
                message = "Please enter the device."
                isFound = false
            } else  if (txtChooseWifi.text?.isEmpty)! {
                message = "Please enter valid wifi name."
                isFound = false
            }  else if (txtWifiPassword.text?.isEmpty)! {
                message = "Please enter the wifi password."
                isFound = false
            } else if (txtWifiConfirmPassword.text?.isEmpty)! {
                message = "Please enter the wifi confirm password."
                isFound = false
            } else if txtWifiPassword.text != txtWifiConfirmPassword.text{
                message = "Password and Confirm Password doesn't match."
                isFound = false
            } else if (txtLocation.text?.isEmpty)! {
                message = "Please select the location."
                isFound = false
            }else if (txtDeviceName.text?.isEmpty)! {
                message = "Please enter the device name."
                isFound = false
            }
            
        }
        
        
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }
    
    func setupDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 34
        appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appearance.animationduration = 0.15
        appearance.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        
    }
    
    func getWiFiName() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    arrWifiList.append(ssid!)
                    self.txtChooseWifi.text = ssid
                    print(ssid as Any)
                }
            }
        }
        
        return ssid
    }
    
    
    func getLocationFromServer()  {
        self.arrLocation.removeAll()
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getLocation, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    for dictData in arrTempData{
                        let objData = LocationModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
                        self.arrLocation.append(objData)
                        self.txtLocation.text = self.arrLocation[0].name
                        self.locationId = self.arrLocation[0].id
                    }
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}



//MARK:- Action Method
@available(iOS 13.0, *)
extension WSDSetupDevice {
    
    @IBAction func actionLocation(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrLocation.map { $0.name } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtLocation.text = item
            locationId = self.arrLocation[index].id
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionDevice(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = ["iphone 6", "iphone 8", "iphone X"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtChooseDevice.text = item
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionWifi(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = arrWifiList//["abc", "xyz", "123"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtChooseWifi.text = item
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionConnect(_ sender: UIButton){
        self.view.endEditing(true)
        
        
        loderView.isHidden = false
        lblLoderTitle.text = "We are getting device ready\nDon’t turn off your device"
        let url = Bundle.main.url(forResource: "Loader_No-Internet", withExtension: "gif")
        DispatchQueue.main.async {
            self.imgLoder.sd_setImage(with: url!)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0) {
            self.loderView.isHidden = true
            Helper.sharedInstance.showToast(isError: true, title: "Device not connected.")
        }
        
        //Helper.sharedInstance.showToast(isError: true, title: "This functionality under construction.")
        /*     Spinner.show("")
         if self.isValidFields() {
         let object = HotspotHelper()
         object.connectToWifi(wifiName: txtChooseDevice.text ?? "TEV_Honeywell", wifiPassword: "e2JdeNZs", wep: false, completion: { result in
         
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) { [self] in
         if let ipAddress = self.getWiFiAddress() {
         print(ipAddress)
         Helper.sharedInstance.showToast(isError: true, title: "ip address: " + ipAddress)
         
         object.connectToWifi(wifiName: self.txtChooseWifi.text!, wifiPassword: self.txtWifiPassword.text!, wep: false, completion: { result in
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
         self.socketIOClient(url: "192.168.4.1")
         Spinner.hide()
         }
         })
         
         } else {
         print("No WiFi address")
         Spinner.hide()
         }
         }
         })
         }
         */
    }
    
    @IBAction func actionRefreshDevice(_ sender: UIButton){
        self.view.endEditing(true)
    }
    
    @IBAction func actionRefreshWifi(_ sender: UIButton){
        self.view.endEditing(true)
        
    }
    
    @IBAction func actionRefreshLocation(_ sender: UIButton){
        self.view.endEditing(true)
        self.getLocationFromServer()
    }
    
    @IBAction func actionPasswordShowHide(_ sender: UIButton){
        
        if (isShowPassword == true) {
            txtWifiPassword.isSecureTextEntry = false
            btnWifiShowHidePassword.setImage(UIImage.init(named: "visibility_on"), for: .normal)
            // imgVisibility.image = UIImage.init(named: "eye")
        } else {
            txtWifiPassword.isSecureTextEntry = true
            btnWifiShowHidePassword.setImage(UIImage.init(named: "visibility_off"), for: .normal)
            // imgVisibility.image = UIImage.init(named: "invisible")
        }
        
        isShowPassword = !isShowPassword
    }
    
    @IBAction func actionAddLocation(_ sender: UIButton){
        let objVC : LocationManagementVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: LocationManagementVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension WSDSetupDevice {
    
    //Gwt local ip address
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        return address
    }
    
    func socketIOClient (url: String) {
        
        //TCP Client
        //        let result = url.split(separator: ".")
        //        if result.count > 2 {
        //            let ipAddress = result[0] + "." + result[1] + "." + result[2] + "." + "1"
        //            print(ipAddress)
        DispatchQueue.global(qos: .background).async {
            let client = TCPClient(address: "192.168.4.1", port: 2020)
            switch client.connect(timeout: 1) {
            case .success:
                switch client.send(string: "0000" ) {
                case .success:
                    guard let data = client.read(1024*10) else { return }
                    
                    if let response = String(bytes: data, encoding: .utf8) {
                        print(response)
                    }
                case .failure(let error):
                    print(error)
                }
            case .failure(let error):
                print(error)
            }
            DispatchQueue.main.async {
                //This is run on the main queue, after the previous
                //code executes.  Update UI here.
            }
        }
        // }
        
        
        
        //UDP Client
        
        /*   self.SocketConnection()
         let result = url.split(separator: ".")
         if result.count > 2 {
         let ipAddress = result[0] + "." + result[1] + "." + result[2] + "." + "1"
         print(ipAddress)
         DispatchQueue.global(qos: .background).async {
         let udpClient = UDPClient(address: ipAddress, port: 2020)
         self.echoService(client: udpClient)
         //udpClient.close()
         DispatchQueue.main.async {
         //This is run on the main queue, after the previous
         //code executes.  Update UI here.
         }
         }
         }
         
         */
        //NWConnection
        /*    let result = url.split(separator: ".")
         if result.count > 2 {
         let ipAddress: String = result[0] + "." + result[1] + "." + result[2] + "." + "1"
         print(ipAddress)
         //  let hostUDP: NWHostEndpoint = ipAddress
         //  let portUDP: NWEndpoint.Port =  UInt16(ipAddress)
         
         DispatchQueue.global(qos: .background).async {
         
         self.connection = NWConnection(host: "192.168.4.1" , port: 2020, using: .udp)
         
         self.connection?.stateUpdateHandler = { (newState) in
         switch (newState) {
         case .ready:
         print("ready")
         self.send()
         self.receive()
         case .setup:
         print("setup")
         case .cancelled:
         print("cancelled")
         case .preparing:
         print("Preparing")
         default:
         print("waiting or failed")
         
         }
         }
         self.connection?.start(queue: .global())
         
         DispatchQueue.main.async {
         //This is run on the main queue, after the previous
         //code executes.  Update UI here.
         }
         }
         }*/
    }
    
    func send() {
        
        let dataStr = "0000"
        
        self.connection?.send(content: dataStr.data(using: String.Encoding.utf8), completion: NWConnection.SendCompletion.contentProcessed(({ (NWError) in
            print(NWError)
        })))
    }
    
    func receive() {
        self.connection?.receiveMessage { (data, context, isComplete, error) in
            print("Got it")
            print(data)
        }
    }
    
    func echoService(client: UDPClient) {
        
        // for object in socketConnectionModelArrayList{
        
        // let socketConnectionModel = object as? SocketConnectionModel
        let buf = [UInt8](("0000".utf8))
        print(buf)
        // let result = client.send(string: socketConnectionModel?.requestMessage ?? "")
        //switch client.send(string: socketConnectionModel?.requestMessage ?? "") {
        switch client.send(data: buf) {
        case .success:
            print("Client sent message to server.")
            
        case .failure(let error):
            print("Client failed to send message to server: \(error)")
        }
        
        let(byteArray, senderIPAddress, senderPort) = client.recv(1024)
        print("client's recv() returned")
        
        if let byteArray = byteArray, let string = String(data: Data(byteArray), encoding: .utf8) {
            print("Cient received: \(string)\nsender: \(senderIPAddress)\nport: \(senderPort)")
        } else {
            print("error in client while trying to recv()")
        }
        
        print("closing client")
        // }
    }
    
    func byteArray(string: String) -> String {
        
        var byteArray = [Byte]()
        for char in string.utf8{
            byteArray += [char]
        }
        print(byteArray)
        return String(bytes: byteArray, encoding: .utf8)!
    }
    
    func SocketConnection () {
        
        let openModel = SocketConnectionModel()
        openModel.action = "open"
        openModel.requestMessage = "0000"
        openModel.requestType = AppConstants.OPEN_CONNECTION
        openModel.verifyResultWith = AppConstants.NOT_EMPTY
        socketConnectionModelArrayList.add(openModel)
        
        let ackModel = SocketConnectionModel()
        ackModel.action = "write"
        ackModel.requestMessage = "ACK_"
        ackModel.requestType = AppConstants.GET_CIPHER
        ackModel.verifyResultWith = AppConstants.NOT_EMPTY
        socketConnectionModelArrayList.add(ackModel)
        
        let receiveCipherModel = SocketConnectionModel()
        receiveCipherModel.action = "write"
        receiveCipherModel.requestMessage = "ACK_"
        receiveCipherModel.requestType = AppConstants.RECEIVED_CIPHER;
        receiveCipherModel.verifyResultWith = ""
        socketConnectionModelArrayList.add(receiveCipherModel)
        
        let verifyDeCipherModel = SocketConnectionModel()
        verifyDeCipherModel.action = "write"
        verifyDeCipherModel.requestMessage = "0001"
        verifyDeCipherModel.requestType = AppConstants.VERIFY_DECIPHER
        verifyDeCipherModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(verifyDeCipherModel)
        
        let deCipherLengthModel = SocketConnectionModel()
        deCipherLengthModel.action = "write"
        deCipherLengthModel.requestMessage = ""
        deCipherLengthModel.requestType = AppConstants.DECIPHER_LENGTH
        deCipherLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deCipherLengthModel)
        
        let deCipherMessageModel = SocketConnectionModel()
        deCipherMessageModel.action = "write"
        deCipherMessageModel.requestMessage = ""
        deCipherMessageModel.requestType = AppConstants.DECIPHER_MESSAGE
        deCipherMessageModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deCipherMessageModel)
        
        let credentialModel = SocketConnectionModel()
        credentialModel.action = "write"
        credentialModel.requestMessage = "0010"
        credentialModel.requestType = AppConstants.NETWORK_CREDENTIALS;
        credentialModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(credentialModel)
        
        let ssidLengthModel = SocketConnectionModel()
        ssidLengthModel.action = "write"
        ssidLengthModel.requestMessage = String (self.byteArray(string: self.txtChooseWifi.text!).length)
        ssidLengthModel.requestType = AppConstants.SSID_LENGTH
        ssidLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(ssidLengthModel)
        
        let ssidModel = SocketConnectionModel()
        ssidModel.action = "write"
        ssidModel.requestMessage = self.byteArray(string: self.txtChooseWifi.text!)
        ssidModel.requestType = AppConstants.SSID
        ssidModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(ssidModel)
        
        let pskLengthModel = SocketConnectionModel()
        pskLengthModel.action = "write"
        pskLengthModel.requestMessage = String (self.byteArray(string: self.txtWifiPassword.text!).length)
        pskLengthModel.requestType = AppConstants.PSK_LENGTH
        pskLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(pskLengthModel)
        
        let pskModel = SocketConnectionModel()
        pskModel.action = "write"
        pskModel.requestMessage = self.byteArray(string: self.txtWifiPassword.text!)
        pskModel.requestType = AppConstants.PSK
        pskModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(pskModel)
        
        // SocketConnectionModel emailIDLengthModel = new SocketConnectionModel();
        // emailIDLengthModel.action = "write";
        // emailIDLengthModel.requestMessage = "";
        // emailIDLengthModel.requestType = AppConstants.EMAIL_ID_LENGTH;
        // emailIDLengthModel.verifyResultWith = "ACK_";
        // socketConnectionModelArrayList.add(emailIDLengthModel);
        //
        // SocketConnectionModel emailIDModel = new SocketConnectionModel();
        // emailIDModel.action = "write";
        // emailIDModel.requestMessage = "";
        // emailIDModel.requestType = AppConstants.EMAIL_ID;
        // emailIDModel.verifyResultWith = "ACK_";
        // socketConnectionModelArrayList.add(emailIDModel);
        
        let deviceNameLengthModel = SocketConnectionModel()
        deviceNameLengthModel.action = "write"
        deviceNameLengthModel.requestMessage = String (self.byteArray(string: self.txtDeviceName.text!).length)
        deviceNameLengthModel.requestType = AppConstants.DEVICE_NAME_LENGTH
        deviceNameLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceNameLengthModel)
        
        let deviceNameModel = SocketConnectionModel()
        deviceNameModel.action = "write"
        deviceNameModel.requestMessage = self.byteArray(string: self.txtDeviceName.text!) // text filed value
        deviceNameModel.requestType = AppConstants.DEVICE_NAME
        deviceNameModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceNameModel)
        
        let deviceLocationLengthModel = SocketConnectionModel()
        deviceLocationLengthModel.action = "write"
        deviceLocationLengthModel.requestMessage = self.byteArray(string: AppConstants.DEVICE_LOCATION_LENGTH)
        deviceLocationLengthModel.requestType = AppConstants.DEVICE_LOCATION_LENGTH
        deviceLocationLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceLocationLengthModel)
        
        let deviceLocationModel = SocketConnectionModel()
        deviceLocationModel.action = "write"
        deviceLocationModel.requestMessage = self.byteArray(string: locationId!) // location id
        deviceLocationModel.requestType = AppConstants.DEVICE_LOCATION
        deviceLocationModel.verifyResultWith = "ACK_";
        socketConnectionModelArrayList.add(deviceLocationModel)
        
        let deviceLocationNameLengthModel = SocketConnectionModel()
        deviceLocationNameLengthModel.action = "write"
        deviceLocationNameLengthModel.requestMessage = String (self.byteArray(string: self.txtLocation.text!).length)
        deviceLocationNameLengthModel.requestType = AppConstants.DEVICE_LOCATION_NAME_LENGTH
        deviceLocationNameLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceLocationNameLengthModel)
        
        let deviceLocationNameModel = SocketConnectionModel()
        deviceLocationNameModel.action = "write"
        deviceLocationNameModel.requestMessage = self.byteArray(string: self.txtLocation.text!)
        deviceLocationNameModel.requestType = AppConstants.DEVICE_LOCATION_NAME
        deviceLocationNameModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceLocationNameModel)
        
        let deviceTypLengtheModel = SocketConnectionModel()
        deviceTypLengtheModel.action = "write"
        deviceTypLengtheModel.requestMessage = self.byteArray(string: AppConstants.DEVICE_TYPE_LENGTH)
        deviceTypLengtheModel.requestType = AppConstants.DEVICE_TYPE_LENGTH
        deviceTypLengtheModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceTypLengtheModel)
        
        let deviceTypeModel = SocketConnectionModel()
        deviceTypeModel.action = "write"
        deviceTypeModel.requestMessage = self.byteArray(string: AppConstants.TEV)
        //Check condtion AppConstants.TEV and AppConstants.WIRELESS
        
        deviceTypeModel.requestType = AppConstants.DEVICE_TYPE
        deviceTypeModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceTypeModel)
        
        let orgLengthModel = SocketConnectionModel()
        orgLengthModel.action = "write"
        orgLengthModel.requestMessage = String (self.byteArray(string: AppConstants.ORG_ID_LENGTH).length)
        orgLengthModel.requestType = AppConstants.ORG_ID_LENGTH;
        orgLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(orgLengthModel)
        
        let orgModel = SocketConnectionModel()
        orgModel.action = "write"
        orgModel.requestMessage = self.byteArray(string: ApplicationPreference.getOrgId()!)
        orgModel.requestType = AppConstants.ORG_ID
        orgModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(orgModel)
        
        let logicaDeviceIdLengthModel =  SocketConnectionModel()
        logicaDeviceIdLengthModel.action = "write"
        logicaDeviceIdLengthModel.requestMessage = String (self.byteArray(string: AppConstants.LOGICAL_DEVICE_ID_LENGTH).length)
        logicaDeviceIdLengthModel.requestType =  self.byteArray(string: AppConstants.LOGICAL_DEVICE_ID_LENGTH)
        logicaDeviceIdLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(logicaDeviceIdLengthModel)
        
        let logicaDeviceIdModel = SocketConnectionModel()
        logicaDeviceIdModel.action = "write"
        logicaDeviceIdModel.requestType = AppConstants.LOGICAL_DEVICE_ID
        logicaDeviceIdModel.verifyResultWith = "ACK_"
        logicaDeviceIdModel.requestMessage = self.byteArray(string: ApplicationPreference.getUuidString() ?? "")
        socketConnectionModelArrayList.add(logicaDeviceIdModel)
        
    }
}
