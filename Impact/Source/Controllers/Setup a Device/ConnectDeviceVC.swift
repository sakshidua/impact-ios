//
//  ConnectDeviceVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit
import DropDown
import SystemConfiguration.CaptiveNetwork
import NetworkExtension
import Foundation
import SwiftSocket
import CoreLocation
import NetworkExtension


@available(iOS 13.0, *)
class ConnectDeviceVC: BaseVC, CLLocationManagerDelegate {
    
    @IBOutlet weak var txtWifiName : UITextField!
    @IBOutlet weak var txtWifiPassword : UITextField!
    @IBOutlet weak var txtWifiConfirmPassword : UITextField!
    @IBOutlet weak var txtLocation : UITextField!
    @IBOutlet weak var txtDeviceName : UITextField!
    @IBOutlet weak var btnWifiShowHidePassword : UIButton!
    @IBOutlet weak var btnGenerateQR : UIButton!
    @IBOutlet weak var deviceLocationLabelView : UIView!
    @IBOutlet weak var deviceLocationTextView : UIView!
    @IBOutlet weak var deviceNameLabelView : UIView!
    @IBOutlet weak var deviceNameTextView : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblPoint : UILabel!
    @IBOutlet weak var btnGenerateQRPosstion: NSLayoutConstraint!
    
    internal static var COMMON_SOCKET_CUSTOM_PING_EVENT = "t_ping"
    internal static var COMMON_ON_SOCKET_CONNECT_EVENT = "connect"
    internal static var COMMON_ON_SOCKET_CONNECTING_EVENT = "connecting"
    internal static var COMMON_ON_SOCKET_RECONNECT_EVENT = "reconnect"
    internal static var COMMON_ON_SOCKET_DISCONNECT_EVENT = "disconnect"
    internal static var COMMON_ON_SOCKET_ERROR_EVENT = "error"
    
    let socketConnectionModelArrayList: NSMutableArray = []
    var nameVC: String = ""
    var arrLocation = [LocationModel]()
    var arrWifiList = [String]()
    var isShowPassword = false
    var locationId: String?
    var device: DeviceModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLocationFromServer()
    }
    
    
    func setInitialData(){
        
        if nameVC == "EditDeviceVC" {
            self.btnGenerateQR.setTitle("Generate edit device QR", for: .normal)
            lblTitle.text = "Edit Device"
            lblPoint.text = "Welcome to edit device wizard."
            self.deviceLocationLabelView.isHidden = true
            self.deviceLocationTextView.isHidden = true
            self.deviceNameLabelView.isHidden = true
            self.deviceNameTextView.isHidden = true
            btnGenerateQRPosstion.constant = -100
        }else{
            self.btnGenerateQR.setTitle("Generate setup QR", for: .normal)
            lblTitle.text = "Setup a Device"
            lblPoint.text = "Welcome to add new device wizard."
            deviceLocationLabelView.isHidden = false
            deviceLocationTextView.isHidden = false
            deviceNameLabelView.isHidden = false
            deviceNameTextView.isHidden = false
            btnGenerateQRPosstion.constant = 50
        }
        
        
        let ssid = self.getWiFiName()
        print("SSID: \(String(describing: ssid))")
        
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtWifiName.text = txtWifiName.text?.trim()
        self.txtWifiPassword.text = self.txtWifiPassword.text?.trim()
        self.txtWifiConfirmPassword.text = self.txtWifiConfirmPassword.text?.trim()
        self.txtLocation.text = self.txtLocation.text?.trim()
        self.txtDeviceName.text = self.txtDeviceName.text?.trim()
        
        if nameVC == "EditDeviceVC" {
            
            if (txtWifiName.text?.isEmpty)! {
                message = "Please enter the wifi name."
                isFound = false
            } else  if (txtWifiPassword.text?.isEmpty)! {
                message = "Please enter valid wifi password."
                isFound = false
            }  else if (txtWifiConfirmPassword.text?.isEmpty)! {
                message = "Please enter the wifi confirm password."
                isFound = false
            } else if txtWifiPassword.text != txtWifiConfirmPassword.text{
                message = "Password and Confirm Password doesn't match."
                isFound = false
            }
            
        }else{
            
            if (txtWifiName.text?.isEmpty)! {
                message = "Please enter the wifi name."
                isFound = false
            } else  if (txtWifiPassword.text?.isEmpty)! {
                message = "Please enter valid wifi password."
                isFound = false
            }  else if (txtWifiConfirmPassword.text?.isEmpty)! {
                message = "Please enter the wifi confirm password."
                isFound = false
            } else if txtWifiPassword.text != txtWifiConfirmPassword.text{
                message = "Password and Confirm Password doesn't match."
                isFound = false
            } else if (txtLocation.text?.isEmpty)! {
                message = "Please select the location."
                isFound = false
            } else if (txtDeviceName.text?.isEmpty)! {
                message = "Please enter the device name."
                isFound = false
            }
            
        }
        
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }
    
    func setupDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 34
        appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appearance.animationduration = 0.15
        appearance.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        
    }
    
    func getWiFiName() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    self.txtWifiName.text = ssid
                    break
                }
            }
        }
        return ssid
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            if #available(iOS 14.0, *) {
                NEHotspotNetwork.fetchCurrent { hotspotNetwork in
                    if let ssid = hotspotNetwork?.ssid {
                        print(ssid)
                        self.txtWifiName.text = ssid
                    }
                }
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    
    func getLocationFromServer()  {
        self.arrLocation.removeAll()
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getLocation, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    for dictData in arrTempData{
                        let objData = LocationModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
                        self.arrLocation.append(objData)
                        self.txtLocation.text = self.arrLocation[0].name
                        self.locationId = self.arrLocation[0].id
                    }
                    
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}



//MARK:- Action Method
@available(iOS 13.0, *)
extension ConnectDeviceVC {
    
    @IBAction func actionLocation(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrLocation.map { $0.name } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtLocation.text = item
            locationId = self.arrLocation[index].id
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionDevice(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = ["iphone 6", "iphone 8", "iphone X"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            // self.txtChooseDevice.text = item
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionWifi(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = arrWifiList//["abc", "xyz", "123"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtWifiName.text = item
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionConnect(_ sender: UIButton){
        self.view.endEditing(true)
        
        let uuidString = UIDevice.current.identifierForVendor!.uuidString
        self.getNotificationDeviceSetup(uuidString: uuidString)
        ApplicationPreference.saveUuidString(uuidString: uuidString)
        var param:[String:Any] = [:]
        
        if self.isValidFields() {
            
            let objVC : SacnDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: SacnDeviceVC.nameOfClass)
            
            if nameVC == "EditDeviceVC" {
                objVC.device = device
                objVC.nameVC = "EditDeviceVC"
                param = ["a":"s",
                         "ldi": device.id ?? "",
                         "s": self.txtWifiName.text ?? "",
                         "p": self.txtWifiPassword.text ?? "" ] as [String : Any]
            }else{
                objVC.nameVC = "Setup Device"
                param = ["a":"s",
                         "dn": self.txtDeviceName.text ?? "",
                         "li": self.locationId ?? "",
                         "ln": self.txtLocation.text ?? "",
                         "oi": ApplicationPreference.getOrgId() ?? "",
                         "ldi": uuidString,
                         "s": self.txtWifiName.text ?? "",
                         "p": self.txtWifiPassword.text ?? "" ] as [String : Any]
            }           
            objVC.param = param
            self.navigationController?.pushViewController(objVC, animated: false)
        }
        
        /*
         if nameVC == "EditDeviceVC" {
         
         if self.isValidFields() {
         param = ["a":"s",
         "oi": ApplicationPreference.getOrgId() ?? "",
         "ldi": uuidString,
         "s": self.txtWifiName.text ?? "",
         "p": self.txtWifiPassword.text ?? "" ] as [String : Any]
         
         let objVC : SacnDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: SacnDeviceVC.nameOfClass)
         objVC.nameVC = "Setup Device"
         objVC.param = param
         self.navigationController?.pushViewController(objVC, animated: false)
         
         }
         
         
         }else{
         if self.isValidFields() {
         param = ["a":"s",
         "dn": self.txtDeviceName.text ?? "",
         "li": self.locationId ?? "",
         "ln": self.txtLocation.text ?? "",
         "oi": ApplicationPreference.getOrgId() ?? "",
         "ldi": uuidString,
         "s": self.txtWifiName.text ?? "",
         "p": self.txtWifiPassword.text ?? "" ] as [String : Any]
         let objVC : SacnDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: SacnDeviceVC.nameOfClass)
         objVC.nameVC = "Setup Device"
         objVC.param = param
         self.navigationController?.pushViewController(objVC, animated: false)
         
         }
         
         }
         */
        
        /* if self.isValidFields() {
         let object = HotspotHelper()
         object.connectToWifi(wifiName: "Hemu", wifiPassword: "hemu@HJ123", wep: true, completion: { result in
         
         if let ipAddress = self.getWiFiAddress() {
         print(ipAddress)
         Helper.sharedInstance.showToast(isError: true, title: "ip address: " + ipAddress)
         self.socketIOClient(url: ipAddress)
         } else {
         print("No WiFi address")
         }
         })
         }
         */
    }
    
    @IBAction func actionRefreshDevice(_ sender: UIButton){
        self.view.endEditing(true)
    }
    
    @IBAction func actionRefreshWifi(_ sender: UIButton){
        self.view.endEditing(true)
        
    }
    
    @IBAction func actionRefreshLocation(_ sender: UIButton){
        self.view.endEditing(true)
        self.getLocationFromServer()
    }
    
    @IBAction func actionPasswordShowHide(_ sender: UIButton){
        
        if (isShowPassword == true) {
            txtWifiPassword.isSecureTextEntry = false
            btnWifiShowHidePassword.setImage(UIImage.init(named: "visibility_on"), for: .normal)
            // imgVisibility.image = UIImage.init(named: "eye")
        } else {
            txtWifiPassword.isSecureTextEntry = true
            btnWifiShowHidePassword.setImage(UIImage.init(named: "visibility_off"), for: .normal)
            // imgVisibility.image = UIImage.init(named: "invisible")
        }
        
        isShowPassword = !isShowPassword
    }
    
    @IBAction func actionAddLocation(_ sender: UIButton){
        let objVC : LocationManagementVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.account, viewControllerName: LocationManagementVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ConnectDeviceVC {
    
    //Gwt local ip address
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        return address
    }
    
    func socketIOClient (url: String) {
        
        let server = TCPServer(address: url, port: 2020)
        switch server.listen() {
        case .success:
            while true {
                Helper.sharedInstance.showToast(isError: true, title: "Success")
                if let client = server.accept() {
                    self.SocketConnection()
                    echoService(client: client)
                } else {
                    Helper.sharedInstance.showToast(isError: true, title: "error")
                    print("accept error")
                }
            }
        case .failure(let error):
            print(error)
        }
    }
    
    func echoService(client: TCPClient) {
        
        for object in socketConnectionModelArrayList {
            
            let socketConnectionModel = object as? SocketConnectionModel
            let byteArray: [UInt8] = (socketConnectionModel?.requestMessage.utf8.map{UInt8($0)})!
            let result = client.send(data: byteArray)
            
            print(result)
            
            if result.isSuccess{
                
                guard let data = client.read(1024*10) else { return }
                if let response = String(bytes: data, encoding: .utf8) {
                    print(response)
                }
                
                Helper.sharedInstance.showToast(isError: true, title: "Success")
            }else if (result.isFailure){
                Helper.sharedInstance.showToast(isError: true, title: "Failure")
            }else{
                Helper.sharedInstance.showToast(isError: true, title: "Error")
            }
            
        }
        
        // client.close()
    }
    
    func byteArray(string: String) -> String {
        
        var byteArray = [Byte]()
        for char in string.utf8{
            byteArray += [char]
        }
        print(byteArray)
        return String(bytes: byteArray, encoding: .utf8)!
    }
    
    func byteArrayData(string: String) -> [Byte] {
        
        var byteArray = [Byte]()
        for char in string.utf8{
            byteArray += [char]
        }
        print(byteArray)
        return byteArray
    }
    
    func SocketConnection () {
        
        let openModel = SocketConnectionModel()
        openModel.action = "open"
        openModel.requestMessage = "0000"
        openModel.requestType = AppConstants.OPEN_CONNECTION
        openModel.verifyResultWith = AppConstants.NOT_EMPTY
        socketConnectionModelArrayList.add(openModel)
        
        let ackModel = SocketConnectionModel()
        ackModel.action = "write"
        ackModel.requestMessage = "ACK_"
        ackModel.requestType = AppConstants.GET_CIPHER
        ackModel.verifyResultWith = AppConstants.NOT_EMPTY
        socketConnectionModelArrayList.add(ackModel)
        
        let receiveCipherModel = SocketConnectionModel()
        receiveCipherModel.action = "write"
        receiveCipherModel.requestMessage = "ACK_"
        receiveCipherModel.requestType = AppConstants.RECEIVED_CIPHER;
        receiveCipherModel.verifyResultWith = ""
        socketConnectionModelArrayList.add(receiveCipherModel)
        
        let verifyDeCipherModel = SocketConnectionModel()
        verifyDeCipherModel.action = "write"
        verifyDeCipherModel.requestMessage = "0001"
        verifyDeCipherModel.requestType = AppConstants.VERIFY_DECIPHER
        verifyDeCipherModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(verifyDeCipherModel)
        
        let deCipherLengthModel = SocketConnectionModel()
        deCipherLengthModel.action = "write"
        deCipherLengthModel.requestMessage = ""
        deCipherLengthModel.requestType = AppConstants.DECIPHER_LENGTH
        deCipherLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deCipherLengthModel)
        
        let deCipherMessageModel = SocketConnectionModel()
        deCipherMessageModel.action = "write"
        deCipherMessageModel.requestMessage = ""
        deCipherMessageModel.requestType = AppConstants.DECIPHER_MESSAGE
        deCipherMessageModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deCipherMessageModel)
        
        let credentialModel = SocketConnectionModel()
        credentialModel.action = "write"
        credentialModel.requestMessage = "0010"
        credentialModel.requestType = AppConstants.NETWORK_CREDENTIALS;
        credentialModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(credentialModel)
        
        let ssidLengthModel = SocketConnectionModel()
        ssidLengthModel.action = "write"
        ssidLengthModel.requestMessage = String (self.byteArray(string: self.txtWifiName.text!).length)
        ssidLengthModel.requestType = AppConstants.SSID_LENGTH
        ssidLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(ssidLengthModel)
        
        let ssidModel = SocketConnectionModel()
        ssidModel.action = "write"
        ssidModel.requestMessage = self.byteArray(string: self.txtWifiName.text!)
        ssidModel.requestType = AppConstants.SSID
        ssidModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(ssidModel)
        
        let pskLengthModel = SocketConnectionModel()
        pskLengthModel.action = "write"
        pskLengthModel.requestMessage = String (self.byteArray(string: self.txtWifiPassword.text!).length)
        pskLengthModel.requestType = AppConstants.PSK_LENGTH
        pskLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(pskLengthModel)
        
        let pskModel = SocketConnectionModel()
        pskModel.action = "write"
        pskModel.requestMessage = self.byteArray(string: self.txtWifiPassword.text!)
        pskModel.requestType = AppConstants.PSK
        pskModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(pskModel)
        
        // SocketConnectionModel emailIDLengthModel = new SocketConnectionModel();
        // emailIDLengthModel.action = "write";
        // emailIDLengthModel.requestMessage = "";
        // emailIDLengthModel.requestType = AppConstants.EMAIL_ID_LENGTH;
        // emailIDLengthModel.verifyResultWith = "ACK_";
        // socketConnectionModelArrayList.add(emailIDLengthModel);
        //
        // SocketConnectionModel emailIDModel = new SocketConnectionModel();
        // emailIDModel.action = "write";
        // emailIDModel.requestMessage = "";
        // emailIDModel.requestType = AppConstants.EMAIL_ID;
        // emailIDModel.verifyResultWith = "ACK_";
        // socketConnectionModelArrayList.add(emailIDModel);
        
        let deviceNameLengthModel = SocketConnectionModel()
        deviceNameLengthModel.action = "write"
        deviceNameLengthModel.requestMessage = String (self.byteArray(string: self.txtDeviceName.text!).length)
        deviceNameLengthModel.requestType = AppConstants.DEVICE_NAME_LENGTH
        deviceNameLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceNameLengthModel)
        
        let deviceNameModel = SocketConnectionModel()
        deviceNameModel.action = "write"
        deviceNameModel.requestMessage = self.byteArray(string: self.txtDeviceName.text!) // text filed value
        deviceNameModel.requestType = AppConstants.DEVICE_NAME
        deviceNameModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceNameModel)
        
        let deviceLocationLengthModel = SocketConnectionModel()
        deviceLocationLengthModel.action = "write"
        deviceLocationLengthModel.requestMessage = self.byteArray(string: AppConstants.DEVICE_LOCATION_LENGTH)
        deviceLocationLengthModel.requestType = AppConstants.DEVICE_LOCATION_LENGTH
        deviceLocationLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceLocationLengthModel)
        
        let deviceLocationModel = SocketConnectionModel()
        deviceLocationModel.action = "write"
        deviceLocationModel.requestMessage = self.byteArray(string: locationId!) // location id
        deviceLocationModel.requestType = AppConstants.DEVICE_LOCATION
        deviceLocationModel.verifyResultWith = "ACK_";
        socketConnectionModelArrayList.add(deviceLocationModel)
        
        let deviceLocationNameLengthModel = SocketConnectionModel()
        deviceLocationNameLengthModel.action = "write"
        deviceLocationNameLengthModel.requestMessage = String (self.byteArray(string: self.txtLocation.text!).length)
        deviceLocationNameLengthModel.requestType = AppConstants.DEVICE_LOCATION_NAME_LENGTH
        deviceLocationNameLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceLocationNameLengthModel)
        
        let deviceLocationNameModel = SocketConnectionModel()
        deviceLocationNameModel.action = "write"
        deviceLocationNameModel.requestMessage = self.byteArray(string: self.txtLocation.text!)
        deviceLocationNameModel.requestType = AppConstants.DEVICE_LOCATION_NAME
        deviceLocationNameModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceLocationNameModel)
        
        let deviceTypLengtheModel = SocketConnectionModel()
        deviceTypLengtheModel.action = "write"
        deviceTypLengtheModel.requestMessage = self.byteArray(string: AppConstants.DEVICE_TYPE_LENGTH)
        deviceTypLengtheModel.requestType = AppConstants.DEVICE_TYPE_LENGTH
        deviceTypLengtheModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceTypLengtheModel)
        
        let deviceTypeModel = SocketConnectionModel()
        deviceTypeModel.action = "write"
        deviceTypeModel.requestMessage = self.byteArray(string: AppConstants.TEV)
        //Check condtion AppConstants.TEV and AppConstants.WIRELESS
        
        deviceTypeModel.requestType = AppConstants.DEVICE_TYPE
        deviceTypeModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(deviceTypeModel)
        
        let orgLengthModel = SocketConnectionModel()
        orgLengthModel.action = "write"
        orgLengthModel.requestMessage = String (self.byteArray(string: AppConstants.ORG_ID_LENGTH).length)
        orgLengthModel.requestType = AppConstants.ORG_ID_LENGTH;
        orgLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(orgLengthModel)
        
        let orgModel = SocketConnectionModel()
        orgModel.action = "write"
        orgModel.requestMessage = self.byteArray(string: ApplicationPreference.getOrgId()!)
        orgModel.requestType = AppConstants.ORG_ID
        orgModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(orgModel)
        
        let logicaDeviceIdLengthModel =  SocketConnectionModel()
        logicaDeviceIdLengthModel.action = "write"
        logicaDeviceIdLengthModel.requestMessage = String (self.byteArray(string: AppConstants.LOGICAL_DEVICE_ID_LENGTH).length)
        logicaDeviceIdLengthModel.requestType =  self.byteArray(string: AppConstants.LOGICAL_DEVICE_ID_LENGTH)
        logicaDeviceIdLengthModel.verifyResultWith = "ACK_"
        socketConnectionModelArrayList.add(logicaDeviceIdLengthModel)
        
        let logicaDeviceIdModel = SocketConnectionModel()
        logicaDeviceIdModel.action = "write"
        logicaDeviceIdModel.requestType = AppConstants.LOGICAL_DEVICE_ID
        logicaDeviceIdModel.verifyResultWith = "ACK_"
        logicaDeviceIdModel.requestMessage = self.byteArray(string: ApplicationPreference.getUuidString()!)
        socketConnectionModelArrayList.add(logicaDeviceIdModel)
        
    }
}


class SocketConnectionModel: NSObject {
    
    var action: String = ""
    var requestMessage: String = ""
    var requestType: String = ""
    var verifyResultWith: String = ""
    
}




//MARK:- Action Method
@available(iOS 13.0, *)
extension ConnectDeviceVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == txtWifiName {
            let maxLength = 32
            let currentString: NSString = txtWifiName.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else if textField == txtWifiPassword {
            let maxLength = 48
            let currentString: NSString = txtWifiPassword.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else if textField == txtWifiConfirmPassword {
            let maxLength = 48
            let currentString: NSString = txtWifiConfirmPassword.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else if textField == txtLocation {
            let maxLength = 20
            let currentString: NSString = txtLocation.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else if textField == txtDeviceName {
            let maxLength = 20
            let currentString: NSString = txtDeviceName.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        else{
            return true
        }
    }
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ConnectDeviceVC {
    
    func getNotificationDeviceSetup(uuidString: String)  {
        //call API Notification DeviceSetup
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.NotificationDeviceSetup+uuidString, andParam: [:], showHud: true, changeBaseUrl: false) { (responseDict) in
            Helper.UI {
                print(responseDict)
                ApplicationPreference.saveUuidString(uuidString: uuidString)
            }
        }
    }
    
}
