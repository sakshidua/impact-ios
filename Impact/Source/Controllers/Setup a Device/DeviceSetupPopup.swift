import UIKit

protocol DeviceSetupVCDelegate:AnyObject {
    func dismissVC()
}

@available(iOS 13.0, *)
class DeviceSetupPopup: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewBottom : UIView!
    weak var delegate : DeviceSetupVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension DeviceSetupPopup {
    
    @IBAction func actionReady(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
        delegate?.dismissVC()
        
        
    }
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
    }

}

