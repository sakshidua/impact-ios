//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit
import CoreLocation

@available(iOS 13.0, *)
class SetupDeviceVC: BaseVC, UIViewControllerTransitioningDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    var arrDeviceType = [[String:Any]]()
    var locationManager: CLLocationManager?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        
        //Notification DeviceSetup
        //self.getNotificationDeviceSetup()
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        
        var dict = [String:Any]()
        dict.updateValue("Smart AI Camera", forKey: "title")
        dict.updateValue("video_cam", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Wireless Smoke Detector", forKey: "title")
        dict.updateValue("WSD_logo", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        self.tblDeviceType.reloadData()
        
    }
    
    func getNotificationDeviceSetup()  {
        
        let uuidString = UIDevice.current.identifierForVendor!.uuidString
        ApplicationPreference.saveUuidString(uuidString: uuidString)
        
        //call API Notification DeviceSetup
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.NotificationDeviceSetup+uuidString, andParam: [:], showHud: true, changeBaseUrl: false) { (responseDict) in
            Helper.UI {
                print(responseDict)
            }
        }
    }
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension SetupDeviceVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeviceType.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrDeviceType[indexPath.row]
            
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
            
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.arrDeviceType[indexPath.row]
        if obj["title"] as? String ?? "" == "Smart AI Camera" {
            
            let objVC : DeviceSetupPopup = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: DeviceSetupPopup.nameOfClass)
            objVC.delegate = self
            // objVC.device = self.device
            // objVC.parentViewType = parentViewType
            objVC.modalTransitionStyle = .crossDissolve
            objVC.modalPresentationStyle = .overFullScreen
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
                        
        }else{
            let objVC : WifiStepsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: WifiStepsVC.nameOfClass)
            objVC.delegate = self
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
        }
        
    }
    
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension SetupDeviceVC : WifiStepsVCDelegate {
    func dismiss() {
        let objVC : WSDSetupDevice = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: WSDSetupDevice.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}


//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension SetupDeviceVC : DeviceSetupVCDelegate {
   
    func dismissVC()
    {
        let objVC : ConnectDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: ConnectDeviceVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
   
    
}

