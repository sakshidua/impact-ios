//
//  ForgotPasswordVC.swift
//  Impact
//
//  Created by Apple on 01/05/21.
//

import UIKit


@available(iOS 13.0, *)
class ForgotPasswordVC: BaseVC {
    
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    var screenType = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    
    func setInitialData(){
                    
        if screenType == "forgotPassword" {
            self.lblTitle.text = "Forgot Password?"
            self.lblSubTitle.text = "Please enter your registered email to receive a password reset link"
        }else{
            self.lblTitle.text = "Resend Confirmation Email"
            self.lblSubTitle.text = "Please enter your email which was used during the registration"
        }
    }
    
    func forgotPasswordFromServer()  {
        let param = ["email":self.txtEmail.text!,
                     "redirectUri":"https://tev-web-staging.azurewebsites.net/reset-password"]
   
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.forgotPassword, andParam: param, showHud: true, changeBaseUrl: false) { (responseDict) in
            print("password-->",responseDict)

            Helper.UI {
                //if let responseBody = responseDict["responseBody"] as? [String: Any]{
                    var msg = ""
                    if let errorMessage = responseDict["errorMessage"] as? String {
                        msg = errorMessage
                    } else if let successMessage = responseDict["successMessage"] as? String {
                        msg = successMessage
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                    }
                    
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                //}
            }
        }
    }
    
    func resendConfirmationEmailFromServer()  {
           
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.resendConfirmationEmail+self.txtEmail.text!, andParam: [:], showHud: true, changeBaseUrl: false) { (responseDict) in
            print("password-->",responseDict)
            Helper.UI {
              //  if let responseBody = responseDict["responseBody"] as? [String: Any]{
                    var msg = ""
                    if let errorMessage = responseDict["errorMessage"] as? String {
                        msg = errorMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                    }else if let successMessage = responseDict["successMessage"] as? String {
                        msg = successMessage
                        Helper.sharedInstance.showToast(isError: false, title: msg)
                    }else{
                        Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                    }
                  self.dismiss(animated: true)
               // }
            }
        }
    }
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ForgotPasswordVC {
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionSend(_ sender: UIButton){
        self.view.endEditing(true)
        
        if screenType == "forgotPassword" {
            if isFormValid() {
                self.forgotPasswordFromServer()
            }
        }else{
            if isFormValid() {
                self.resendConfirmationEmailFromServer()
            }
        }
      
    }
    
    @IBAction func actionGoBack(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

@available(iOS 13.0, *)
extension ForgotPasswordVC {
    func isFormValid()-> Bool{
        
        self.txtEmail.text = txtEmail.text?.trim()
        
        var mssg = ""
        if self.txtEmail.text?.trimmedString() == "" {
            mssg = "Please enter the email."
        }else if !(self.txtEmail.text?.isValidEmail())! {
            mssg = "Please enter valid email."
        }
        if mssg != "" {
            Helper.sharedInstance.showToast(isError: true, title: mssg)
            return false
        }
        
        return true
    }
}

