//
//  CreateAccountVC.swift
//  Impact
//
//  Created by Apple on 01/05/21.
//

import UIKit

@available(iOS 13.0, *)
class CreateAccountVC: BaseVC {
    
    @IBOutlet weak var btnCorporate : UIButton!
    @IBOutlet weak var btnIndividual : UIButton!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtMobileNumber : UITextField!
    @IBOutlet weak var txtConfirmPassword : UITextField!
    @IBOutlet weak var txtCompanyName : UITextField!
    @IBOutlet weak var txtGSTIN : UITextField!
    @IBOutlet weak var viewCompanyName : UIView!
    @IBOutlet weak var viewGSTIN : UIView!
    @IBOutlet weak var imgVisibility: UIImageView!
    @IBOutlet weak var btnShowHide: UIButton!
    @IBOutlet weak var btnConfirmShowHide: UIButton!
    @IBOutlet weak var btnTermCondtion: UIButton!
    @IBOutlet weak var lblTC: UILabel!
    
    
    var isShowPassword = false
    var isConfirmShowPassword = false
    var isTermCondtion = false
    var accountType = ""
    let radioAccountType: RadioButtonController = RadioButtonController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    func setInitialData(){
        
        let attributedString = NSMutableAttributedString.init(string: "I agree to T&C and Legal")
        // Add Underline Style Attribute.
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSRange.init(location: 11, length: 3));
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSRange.init(location: 18, length: 6));
        
        lblTC.attributedText = attributedString
        
        self.radioAccountType.buttonsArray = [btnCorporate,btnIndividual]
        self.radioAccountType.defaultButton = btnIndividual
        
        self.accountType = "individual"
        self.viewGSTIN.isHidden = true
        self.viewCompanyName.isHidden = true
        
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtFirstName.text = txtFirstName.text?.trim()
        self.txtLastName.text = txtLastName.text?.trim()
        self.txtEmail.text = txtEmail.text?.trim()
        self.txtMobileNumber.text = txtMobileNumber.text?.trim()
        self.txtPassword.text = self.txtPassword.text?.trim()
        
        let passwdValidate = self.txtPassword.text?.trim()
        let strText = txtEmail.text?.trim()
        
        if (txtEmail.text?.isEmpty)! {
            message = "Please enter the email."
            isFound = false
        } else if !(strText?.isValidEmail())! && !(strText?.contains("@"))! {
            message = "Please enter valid email."
            isFound = false
        } else if (txtPassword.text?.isEmpty)! {
            message = "Please enter the password."
            isFound = false
        } else if ((txtPassword.text?.length)! < AppConstant.LIMIT_LOWER_PASSWORD) {
            message = "please_enter_valid_password_between_8_20_characters"
            isFound = false
        }else if txtPassword.text != txtConfirmPassword.text{
            message = "Password and Confirm Password doesn't match."
            isFound = false
        } else if !(passwdValidate?.isPasswordValidate())!{
            message = "Password should content at least 1 uppercase, 1 lowercase , and numeric value."
            isFound = false
        } else if (txtFirstName.text?.isEmpty)! {
            message = "Please enter the first name."
            isFound = false
        }else if (txtLastName.text?.isEmpty)! {
            message = "Please enter the last name."
            isFound = false
        }else if (txtMobileNumber.text?.isEmpty)! {
            message = "Please enter the mobile number.."
            isFound = false
        }else if(isTermCondtion == false) {
            message = "Please select terms and conditions."
            isFound = false
        }
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }
    
    @IBAction func actionPasswordShowHide(_ sender: UIButton){
        if(isShowPassword == true) {
            txtPassword.isSecureTextEntry = false
            btnShowHide.setImage(UIImage.init(named: "visibility_on"), for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            btnShowHide.setImage(UIImage.init(named: "visibility_off"), for: .normal)
        }
        isShowPassword = !isShowPassword
    }
    
    @IBAction func actionConfirmPasswordShowHide(_ sender: UIButton){
        if(isConfirmShowPassword == true) {
            txtConfirmPassword.isSecureTextEntry = false
            btnConfirmShowHide.setImage(UIImage.init(named: "visibility_on"), for: .normal)
        } else {
            txtConfirmPassword.isSecureTextEntry = true
            btnConfirmShowHide.setImage(UIImage.init(named: "visibility_off"), for: .normal)
        }
        isConfirmShowPassword = !isConfirmShowPassword
    }
    
    @IBAction func actionTermCOndtion(_ sender: UIButton){
        
        if(isTermCondtion == true) {
            btnTermCondtion.setImage(UIImage.init(named: "uncheck_box"), for: .normal)
        } else {
            btnTermCondtion.setImage(UIImage.init(named: "check_box"), for: .normal)
        }
        isTermCondtion = !isTermCondtion
    }
    
    @IBAction func actionTC(_ sender: UIButton){
        let objVC : TermConditionsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: TermConditionsVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionLegal(_ sender: UIButton){
        let objVC : LegalVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: LegalVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
}


@available(iOS 13.0, *)
extension CreateAccountVC {
    
    @IBAction func actionCreateAccount(_ sender: UIButton){
        self.view.endEditing(true)
        self.registerFromServer()
    }
    
    @IBAction func corporateAction(_ sender: UIButton) {
        self.accountType = "corporate"
        self.viewGSTIN.isHidden = false
        self.viewCompanyName.isHidden = false
        self.radioAccountType.buttonArrayUpdated(buttonSelected: sender)
    }
    
    @IBAction func individualAction(_ sender: UIButton) {
        self.accountType = "individual"
        self.viewGSTIN.isHidden = true
        self.viewCompanyName.isHidden = true
        self.radioAccountType.buttonArrayUpdated(buttonSelected: sender)
    }
    
    func registerFromServer()  {
        
        /*
         "orgName":self.txtCompanyName.text!,
         "orgDescription": "",
         "gstin":self.txtGSTIN.text!,
         */
        
        if isValidFields() {
            
            let param = ["firstName":self.txtFirstName.text!,
                         "lastName":self.txtLastName.text!,
                         "mobileNumber":self.txtMobileNumber.text!,
                         "password":self.txtPassword.text!,
                         "redirectUri":self.txtEmail.text!,
                         "email":self.txtEmail.text!] as [String : Any]
            
            // let org = ["orgName":"",
            // "orgDescription": "",
            // "gstin":""] as [String : Any]
            
            let user = ["user":param, "appName":"TEV", "registerAs":"Individual"] as [String : Any]
            
            APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.register, andParam: user, showHud: true, changeBaseUrl: true) { (responseDict) in
                print("password-->",responseDict)
                
                Helper.UI {
                  //  if let responseBody = responseDict["responseBody"] as? [String: Any]{
                        var msg = ""
                        if let errorMessage = responseDict["errorMessage"] as? String {
                            msg = errorMessage
                            Helper.sharedInstance.showToast(isError: false, title: msg)
                            self.dismiss(animated: true)
                        } else if let successMessage = responseDict["successMessage"] as? String {
                            msg = successMessage
                            Helper.sharedInstance.showToast(isError: false, title: msg)
                            self.dismiss(animated: true)
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                        }
                        
                        
                  //  }
                }
            }
            
        }
    }
}


class RadioButtonController: NSObject {
    var buttonsArray: [UIButton]! {
        didSet {
            for b in buttonsArray {
                b.setImage(UIImage(named: "radio_unchecked"), for: .normal)
                b.setImage(UIImage(named: "radio_checked"), for: .selected)
            }
        }
    }
    var selectedButton: UIButton?
    var defaultButton: UIButton = UIButton() {
        didSet {
            buttonArrayUpdated(buttonSelected: self.defaultButton)
        }
    }
    
    func buttonArrayUpdated(buttonSelected: UIButton) {
        for b in buttonsArray {
            if b == buttonSelected {
                selectedButton = b
                b.isSelected = true
            } else {
                b.isSelected = false
            }
        }
    }
}


//MARK:- Action Method
@available(iOS 13.0, *)
extension CreateAccountVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == txtMobileNumber {
            let maxLength = 10
            let currentString: NSString = txtMobileNumber.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else{
            return true
        }
    }
    
}

