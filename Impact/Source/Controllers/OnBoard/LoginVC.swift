import UIKit

@available(iOS 13.0, *)
class LoginVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var imgVisibility: UIImageView!
    @IBOutlet weak var btnShowHide: UIButton!
    
    var isShowPassword = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    func setInitialData(){
        self.txtEmail.text = "yoyoyo@yopmail.com"//"iosapp@mailinator.com"
        //"yoyoyo@yopmail.com"
       self.txtPassword.text = "Pass@123"//"Pass@123"
        ApplicationPreference.removeAccessToken()
    }
    
    func isValidFields()->Bool{
        
        self.view.endEditing(true)
        var isFound = true
        var message = ""
        
        self.txtEmail.text = txtEmail.text?.trim()
        self.txtPassword.text = self.txtPassword.text?.trim()
        let passwdValidate = self.txtPassword.text?.trim()
        let strText = txtEmail.text?.trim()
        
        if (txtEmail.text?.isEmpty)! {
            message = "Please enter the email."
            isFound = false
        } else if !(strText?.isValidEmail())! && !(strText?.contains("@"))! {
            message = "Please enter valid email."
            isFound = false
        }  else if (txtPassword.text?.isEmpty)! {
            message = "Please enter the password."
            isFound = false
        } else if ((txtPassword.text?.length)! < AppConstant.LIMIT_LOWER_PASSWORD) {
            message = "please_enter_valid_password_between_8_20_characters"
            isFound = false
        }else if !(passwdValidate?.isPasswordValidate())!{
            message = "Password should content at least 1 uppercase, 1 lowercase , and numeric value."
            isFound = false
        }
        
        if !isFound {
            Helper.sharedInstance.showToast(isError: true, title: message)
        }
        return isFound
    }
    
    @IBAction func actionPasswordShowHide(_ sender: UIButton){
        if(isShowPassword == true) {
            txtPassword.isSecureTextEntry = false
            btnShowHide.setImage(UIImage.init(named: "visibility_on"), for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            btnShowHide.setImage(UIImage.init(named: "visibility_off"), for: .normal)
        }
        isShowPassword = !isShowPassword
    }
    
    func loginFromServer()  {
        let param = ["email":self.txtEmail.text!,
                     "password":self.txtPassword.text!,
                     "clientId":"tev.mobileApp"]
   
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.login, andParam: param, showHud: true, changeBaseUrl:true) { (responseDict) in
            
            Helper.UI {
                if let responseBody = responseDict["responseBody"] as? [String: Any]{
               
                    let accessToken = responseBody["accessToken"] as? String ?? ""
                    ApplicationPreference.saveAccessToken(accessToken: accessToken)
                    //Get User Details
                    self.getUserDetails()                    
                    let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
                    self.navigationController?.pushViewController(objVC, animated: true)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            } 
        }
    }
    
    func getUserDetails()  {
        //call API userDetails
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.userDetails, andParam: [:], showHud: true, changeBaseUrl: true) { (responseDict) in
            Helper.UI {
                print(responseDict["orgId"] as? String ?? "")
                ApplicationPreference.saveOrgId(orgId: responseDict["orgId"] as? String ?? "")
            }
        }
    }
    
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension LoginVC {
    
    @IBAction func actionLogin(_ sender: UIButton){
        self.view.endEditing(true)
        
        if self.isValidFields() {
            self.loginFromServer()
        }
    }
    
    @IBAction func actionCreateAccount(_ sender: UIButton){
        self.view.endEditing(true)
        let objVC : CreateAccountVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: CreateAccountVC.nameOfClass)
        
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionForgotPassword(_ sender: UIButton){
        self.view.endEditing(true)
        let objVC : ForgotPasswordVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: ForgotPasswordVC.nameOfClass)
        
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        objVC.screenType = "forgotPassword"
        self.navigationController?.present(objVC, animated: true)
        //self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func actionResendConfirmation(_ sender: UIButton){
        self.view.endEditing(true)
        let objVC : ForgotPasswordVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.onBoard, viewControllerName: ForgotPasswordVC.nameOfClass)
        
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        objVC.screenType = "resetEmail"
        self.navigationController?.present(objVC, animated: true)
    }
}

