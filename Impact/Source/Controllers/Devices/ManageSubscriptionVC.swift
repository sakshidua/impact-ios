//
//  ManageSubscriptionVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 28/05/21.
//

import UIKit

protocol ManageSubscriptionVCDelegate: AnyObject {
    func makeAPICallFor(parentViewType:ParentViewType)
}

@available(iOS 13.0, *)
class ManageSubscriptionVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    
    @IBOutlet weak var btnModify : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var cnsBottomViewHeight: NSLayoutConstraint!
    
    let fullHeight: CGFloat = 440
    let halfHeight: CGFloat = 200
    
    var device: DeviceModel!
    weak var delegate: ManageSubscriptionVCDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setInitialData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func setInitialData() {
        self.cnsBottomViewHeight.constant = fullHeight
        if device.subscriptionStatus != "live" {
            self.cnsBottomViewHeight.constant = halfHeight
            btnModify.setTitle("Reactivate Subscription", for: .normal)
            btnCancel.isHidden = true
        }
        
        if device.deviceType == "TEV" {
            self.cnsBottomViewHeight.constant = halfHeight
        }
        self.tapToDismiss()
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ManageSubscriptionVC {
    
    @IBAction func actionUpgradeDowngrade(_ sender: UIButton){
        let token = ApplicationPreference.getAccessToken() ?? ""
        let BUY_SUBSCRIPTION_URL_STAGING = "https://tev-web-staging.azurewebsites.net/tev/plan?accessToken="
        let strUrl = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "&headless=true" + "&deviceId=\(device.id ?? "")" + "&subscriptionNumber=\(device.subscriptionId ?? "")" + "&product=\(device.deviceType ?? "")"
        if let url = URL(string: strUrl), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:]) { _ in
                print("call back")
            }
        }
    }
    
    @IBAction func actionModify(_ sender: UIButton) {
        if device.subscriptionStatus != "live" { // reactivate subscription
            self.delegate?.makeAPICallFor(parentViewType: .reactivate)
            self.dismissController()
        } else { // modify subscription
            let token = ApplicationPreference.getAccessToken() ?? ""
            let BUY_SUBSCRIPTION_URL_STAGING = "https://tev-web-staging.azurewebsites.net/tev/plan?accessToken="
            let strUrl = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "&headless=true" + "&deviceId=\(device.id ?? "")" + "&subscriptionNumber=\(device.subscriptionId ?? "")" + "&product=\(device.deviceType ?? "")"

            if let url = URL(string: strUrl), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:]) { _ in
                    print("call back")
                }
            }
        }
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        let objVC : ConfirmationVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ConfirmationVC.nameOfClass)
        objVC.delegate = self
        objVC.device = self.device
        objVC.parentViewType = .subscription
        objVC.modalTransitionStyle = .crossDissolve
        objVC.modalPresentationStyle = .overFullScreen
        objVC.transitioningDelegate = self
        self.present(objVC, animated: true)
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.dismissController()
    }
}

//MARK:- Custom Delegate
@available(iOS 13.0, *)
extension ManageSubscriptionVC: ConfirmationVCDelegate {
    func makeAPICallFor(parentViewType: ParentViewType) {
        
        self.delegate?.makeAPICallFor(parentViewType: parentViewType)
        DispatchQueue.main.async {
            self.dismissController()
        }
    }
}
