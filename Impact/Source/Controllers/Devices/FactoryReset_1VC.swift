import UIKit

protocol FactoryReset_1VCDelegate:AnyObject {
    func dismissView()
}

@available(iOS 13.0, *)
class FactoryReset_1VC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewBottom : UIView!
    weak var delegate : FactoryReset_1VCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension FactoryReset_1VC {
    
    @IBAction func actionReady(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
        delegate?.dismissView()
    }
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
    }

}

