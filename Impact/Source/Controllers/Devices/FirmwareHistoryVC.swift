//
//  FirmwareHistoryVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 03/06/21.
//

import UIKit

@available(iOS 13.0, *)
class FirmwareHistoryVC: BaseVC {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var device: DeviceModel!
    private let cellIdentifier = "TableViewCell"
    var noAlertMessage = "No data available"
    var arrFirmwareHistory = [[String:Any]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}

//MARK:- Instance Method
@available(iOS 13.0, *)
extension FirmwareHistoryVC {
    
    private func setup() {
        self.firmwareHistoryFromServer()
    }
    
    private func reload() {
        !arrFirmwareHistory.isEmpty ? hideTableView(isHidden: false) : hideTableView(isHidden: true)
    }
    
    private func hideTableView(isHidden: Bool) {
        self.tableView.isHidden = isHidden
        self.lblMessage.isHidden = !isHidden
        self.lblMessage.text = isHidden ? noAlertMessage : ""
        self.tableView.reloadData()
    }
    
    private func installedDate(timestamp:Int?) -> String {
        if let timeResult = timestamp {
            let date = NSDate(timeIntervalSince1970: TimeInterval(timeResult))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
            let dayTimePeriodFormatterTime = DateFormatter()
            dayTimePeriodFormatterTime.dateFormat = "hh:mm a"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            let timeString = dayTimePeriodFormatterTime.string(from: date as Date)
            return "Installed on: " + dateString + " at " + timeString
        } else {
            return "-"
        }
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension FirmwareHistoryVC {
  
}



//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension FirmwareHistoryVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFirmwareHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell {
            let data = self.arrFirmwareHistory[indexPath.row]
            cell.lblTitle.text = "Version: " + (data["version"] as? String ?? "-")
            cell.lblDateTime.text = installedDate(timestamp: data["installedOn"] as? Int)
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK:- API Calls
@available(iOS 13.0, *)
extension FirmwareHistoryVC {
        
    private func firmwareHistoryFromServer() {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.firmwareHistory + self.device.id!, andParam: [:], showHud: false) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]] {
                    self.arrFirmwareHistory = arrTempData
                    print("Software History ---> \(self.arrFirmwareHistory.count)")
                    self.reload()
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}
