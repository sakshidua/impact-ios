import UIKit

@available(iOS 13.0, *)
class EditDeviceReadyVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    var device: DeviceModel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension EditDeviceReadyVC {
    
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionReady(_ sender: UIButton){
        let deviceDict:[String: DeviceModel] = ["device": device]

        NotificationCenter.default.post(name: Notification.Name("MoveVC"), object: "", userInfo: deviceDict)
        self.dismiss(animated: true)
    }
    
}

