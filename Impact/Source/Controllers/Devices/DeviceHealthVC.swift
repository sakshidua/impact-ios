//
//  DeviceHealthVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 03/06/21.
//

import UIKit

@available(iOS 13.0, *)
class DeviceHealthVC: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

//MARK:- Instance Method
@available(iOS 13.0, *)
extension DeviceHealthVC {
    
    private func setup() {
        
    }

}

//MARK:- Action Method
@available(iOS 13.0, *)
extension DeviceHealthVC {
    
}

//MARK:- API Calls
@available(iOS 13.0, *)
extension DeviceHealthVC {
    
}
