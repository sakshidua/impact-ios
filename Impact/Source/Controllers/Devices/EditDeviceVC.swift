import UIKit

protocol EditDeviceVCDelegate:AnyObject {
    func dismiss(VC:String)
}

@available(iOS 13.0, *)
class EditDeviceVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewBottom : UIView!
    weak var delegate : EditDeviceVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension EditDeviceVC {
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionUpdateWiFi(_ sender: UIButton){
        self.dismiss(animated: true)
        delegate?.dismiss(VC: "EditDeviceReadyVC")
    }
    
    @IBAction func actionUpdateLocation(_ sender: UIButton){
        self.dismiss(animated: true)
        delegate?.dismiss(VC:"UpdateDeviceVC")
    }
}

