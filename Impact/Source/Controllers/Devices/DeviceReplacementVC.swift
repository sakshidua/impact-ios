//
//  DeviceReplacementVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 29/05/21.
//

import UIKit

@available(iOS 13.0, *)
class DeviceReplacementVC: BaseVC {
    
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnHome : UIButton!
    @IBOutlet weak var btnSubmit : UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewReplace: UIView!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var tableView : UITableView!
    
    var device: DeviceModel!
    let cellIdentifier = "TableViewCell"
    var replacements = [DeviceReplacementModel]()
    weak var delegate: ConfirmationVCDelegate?
    var parentViewType: ParentViewType = .subscription
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}

//MARK:- Instance Method
@available(iOS 13.0, *)
extension DeviceReplacementVC {
    
    private func setInitialData() {
        viewReplace.isHidden = true
        tableView.isHidden = true
        [btnBack, btnHome, btnSubmit].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        self.deviceReplacementFromServer()
    }
    
    private func hideTable(hidden:Bool) {
        self.tableView.isHidden = hidden
        self.viewReplace.isHidden = !hidden
        self.tableView.reloadData()
    }
    
    private func validate() {
        if txtComment.text.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            Helper.sharedInstance.showToast(isError: true, title: "Please enter your comment")
            return
        }
        self.createDeviceReplacementFromServer()
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension DeviceReplacementVC {

    @objc func buttonPressed(_ button:UIButton) {
        switch button {
        case btnBack:
            self.navigationController?.popViewController(animated: true)
        case btnHome:
            self.navigationController?.popViewController(animated: true)
        case btnSubmit:
            self.validate()
        default:
            break
        }
    }
}



//MARK:- Tableview Delegates
@available(iOS 13.0, *)
extension DeviceReplacementVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return replacements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell {
            let data = self.replacements[indexPath.row]
            cell.lblEmail.text = data.email
            cell.lblComment.text = data.comments
            cell.lblReplacementId.text = "\(data.deviceReplacementId ?? 0)"
            return cell
        } else {
            return UITableViewCell()
        }
    }
}


//MARK: - API Calls
@available(iOS 13.0, *)
extension DeviceReplacementVC {
    
    private func deviceReplacementFromServer() {

        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.deviceReplacement + (device.id ?? ""), andParam: [:] , showHud: true) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]], arrTempData.count != 0 {
                    for dictData in arrTempData{
                        let objData = DeviceReplacementModel.init(email: dictData["email"] as? String, orgId: dictData["orgId"] as? String, comments: dictData["comments"] as? String, deviceId: dictData["deviceId"] as? String, replaceStatus: dictData["replaceStatus"] as? String, deviceReplacementId: dictData["deviceReplacementId"] as? Int)
                        self.replacements.append(objData)
                    }
                    self.hideTable(hidden: false)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                } else {
                    self.hideTable(hidden: true)
                };
            }
        }
    }
    
    private func createDeviceReplacementFromServer() {
        let param = [ "deviceId": (device.id ?? ""),
                      "comment": txtComment.text ?? "" ] as [String : Any]

        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.createDeviceReplacement, andParam: param , showHud: true) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [String: Any] {
                    print("arrTempData ---> \(arrTempData)")
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.dismissController()
            }
        }
    }
}

