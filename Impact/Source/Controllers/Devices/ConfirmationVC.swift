//
//  ConfirmationVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 29/05/21.
//

import UIKit

enum ParentViewType: String {
    case subscription
    case reactivate
    case updateFirmware
    case deviceRestore
    case deleteDevice
    case factoryReset
}

protocol ConfirmationVCDelegate: AnyObject {
    func makeAPICallFor(parentViewType:ParentViewType)
}

@available(iOS 13.0, *)
class ConfirmationVC: BaseVC {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    
    @IBOutlet weak var viewBottom : UIView!
    
    var device: DeviceModel!
    weak var delegate: ConfirmationVCDelegate?
    var parentViewType: ParentViewType = .subscription
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTitleDescription()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Instance Method
@available(iOS 13.0, *)
extension ConfirmationVC {
    private func setInitialData() {
        self.tapToDismiss()
    }
    
    private func setTitleDescription() {
        switch parentViewType {
        case .subscription:
            self.lblTitle.text = "Cancel Subscription"
            self.lblDescription.text = "Are you sure you want to cancel subscription?"
        case .updateFirmware:
            self.lblTitle.text = "Update software"
            self.lblDescription.text = "Are you sure you want to update software?"
        case .deviceRestore:
            self.lblTitle.text = "Restore Factory Version"
            self.lblDescription.text = "Are you sure you want to reset device to factory settings?"
        case .deleteDevice:
            self.lblTitle.text = "Delete Device"
            self.lblDescription.text = "Are you sure you want to delete device?"
        case .factoryReset:
            self.lblTitle.text = "Factory reset"
            self.lblDescription.text = "Are you sure you want to factory reset?"
            
            
        default:
            break
        }
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ConfirmationVC {
    
    @IBAction func actionYes(_ sender: UIButton) {
        self.delegate?.makeAPICallFor(parentViewType: parentViewType)
        self.dismissController()
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.dismissController()
    }
}
