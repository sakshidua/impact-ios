import UIKit
import DropDown
import EasyTipView

@available(iOS 13.0, *)
class FeatureConfigurationVC: BaseVC {
    
    @IBOutlet weak var btnToolTipLoitering : UIButton!
    @IBOutlet weak var btnToolTipCrowd : UIButton!
    @IBOutlet weak var btnToolTipTrespassing : UIButton!
    @IBOutlet weak var btnCrowd : UIButton!
    @IBOutlet weak var btnToTime : UIButton!
    @IBOutlet weak var btnFromTime : UIButton!
    @IBOutlet weak var lblErrorMsg : UILabel!
    @IBOutlet weak var lineView : UIView!
    @IBOutlet weak var viewZoneFencing : UIView!
    @IBOutlet weak var txtLoitering : UITextField!
    @IBOutlet weak var cnsLineViewLeading: NSLayoutConstraint!
    @IBOutlet weak var lblTrespassingDuration: UILabel!
    
    private var hubConnection: HubConnection?
    var streamURL: String!
    var device: DeviceModel!
    var arrLocation : [String] = ["3","4","5","6","7","8","9","10"]
    var preferences = EasyTipView.Preferences()
    var tipView: EasyTipView?
    var startTime : String = ""
    var endTime : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.getDeviceConfigurationFromServer()
    }
    
    override func viewWillDisappear(_ animated: Bool){
        tipView?.dismiss()
    }

    override func viewDidDisappear(_ animated: Bool){
        tipView?.dismiss()
    }

    func setup() {
        
        preferences.drawing.font = UIFont(name: "HoneywellSansWeb-Medium", size: 12)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = UIColor.appOrnageBackground()
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 0.5
        preferences.animating.dismissDuration = 0.5
        /*
         * Optionally you can make these preferences global for all future EasyTipViews
         */
        EasyTipView.globalPreferences = preferences
    }
    
    private func establishConnectionForLiveStream() {
        let token = ApplicationPreference.getAccessToken() ?? ""
        self.hubConnection = HubConnectionBuilder(url: URL(string: self.streamURL)!)
            .withLogging(minLogLevel: .debug)
            .withAutoReconnect()
            .withHubConnectionDelegate(delegate: self)
            .withHttpConnectionOptions(configureHttpOptions: { httpConnectionOptions in
                httpConnectionOptions.accessTokenProvider = {
                    return token
                }
            })
            .build()
        
        self.hubConnection!.on(method: self.device.id!) { argumentExtractor in
            if signalRData != nil {
                if let url = signalRData.arguments?.first?.hlsURL  {
                    return
                }
                if let successMessage = signalRData.arguments?.first?.successMessage  {
                    Helper.sharedInstance.showToast(isError: true, title: successMessage)
                    return
                }
            }
        }
        self.hubConnection!.start()
    }
    
    @IBAction func actionResetToDefault(_ sender: UIButton){
        tipView?.dismiss()
        self.btnCrowd.setTitle("5", for: .normal)
        self.btnFromTime.setTitle("12:00", for: .normal)
        self.btnToTime.setTitle("11:59", for: .normal)
        self.txtLoitering.text = "2"
    }
    
    @IBAction func actionSave(_ sender: UIButton){
        tipView?.dismiss()
        let crowd = Int ((txtLoitering?.text)!)
        if crowd! > 1 && crowd! < 100 {
            lblErrorMsg.isHidden = true
            self.updateDeviceConfiguration()
        }else{
            // Helper.sharedInstance.showToast(isError: false, title: "Interval should be between 2 to 99")
            lblErrorMsg.isHidden = false
        }
    }
    
    @IBAction func actionFromTime(_ sender: UIButton){
        tipView?.dismiss()
        self.view.endEditing(true)
        self.openDatePicker(dateType: dateType.startTime)
    }
    
    @IBAction func actionToTime(_ sender: UIButton){
        tipView?.dismiss()
        self.view.endEditing(true)
        self.openDatePicker(dateType: dateType.endTime)
    }
    
    @IBAction func actionCrowd(_ sender: UIButton){
        tipView?.dismiss()
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrLocation.map { $0 } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnCrowd.setTitle(item, for: .normal)
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionZoneFencing(_ sender: UIButton){
        tipView?.dismiss()
        self.view.endEditing(true)
        self.viewZoneFencing.isHidden = false
        self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
    }
    
    @IBAction func actionCustomizeFeature(_ sender: UIButton){
        tipView?.dismiss()
        self.view.endEditing(true)
        self.viewZoneFencing.isHidden = true
        self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
    }
    
    @IBAction func actionSaveZoneFencing(_ sender: UIButton){
        tipView?.dismiss()
        self.view.endEditing(true)
        self.setResetZoneFencing()
    }
    
    @IBAction func actionOpenCrowdToolTip(_ sender: UIButton){
        tipView?.dismiss()
        tipView = EasyTipView(text: "Select number of person to be detected for crowd threshold", preferences: preferences)
        tipView?.show(forView: self.btnToolTipCrowd, withinSuperview: nil)
    }
    
    @IBAction func actionOpenLoiteringToolTip(_ sender: UIButton){
        tipView?.dismiss()
        tipView = EasyTipView(text: "Loitering threshold is set in minutes, select minutes from dropdown to detect loitering", preferences: preferences)
        tipView?.show(forView: self.btnToolTipLoitering, withinSuperview: nil)
    }
    
    @IBAction func actionOpenTrespassingToolTip(_ sender: UIButton){
        tipView?.dismiss()
        tipView = EasyTipView(text: "Set from and to time to enable trespassing during this time", preferences: preferences)
        tipView?.show(forView: self.btnToolTipTrespassing, withinSuperview: nil)
    }
    
}

//Mark:- API Calls
@available(iOS 13.0, *)
extension FeatureConfigurationVC {
    
    func getDeviceConfigurationFromServer()  {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getDeviceConfiguration+device.id!, andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                if let responseBody = responseDict["responseBody"] as? [String: Any] {
                    print(responseBody)
                    
                    if let crowd = responseBody["crowd"] as? [String: Any] {
                        if let crowdLimit = crowd["crowdLimit"] as? String {
                            print(crowdLimit)
                            self.btnCrowd.setTitle(crowdLimit, for: .normal)
                        }
                    }
                    
                    if let trespassing = responseBody["trespassing"] as? [String: Any] {
                        
                        self.btnFromTime.setTitle(self.timeFormat(time: (trespassing["trespassingStartTime"] as? String)!), for: .normal)
                        self.btnToTime.setTitle(self.timeFormat(time: ((trespassing["trespassingEndTime"] as? String)!)), for: .normal)
                        
                    }
                    
                    if let loiter = responseBody["loiter"] as? [String: Any] {
                        if let time = loiter["time"] as? Int {
                            print(time)
                            self.txtLoitering.text = String(time)
                        }
                    }
                    
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func updateDeviceConfiguration () {
        
        let fromTime = self.convertToUTC(dateToConvert: self.btnFromTime.titleLabel?.text! ?? "")
        let toTime = self.convertToUTC(dateToConvert: self.btnToTime.titleLabel?.text! ?? "")
        let loiterTime = self.txtLoitering.text!
        let crowdPersonLimit = self.btnCrowd.titleLabel?.text! ?? ""
        
        let param = ["trespassingStartTime":fromTime ?? "",
                     "trespassingEndTime":toTime ?? "",
                     "loiterTime": Int(loiterTime)!,
                     "crowdPersonLimit":Int(crowdPersonLimit)!] as [String : Any]
        print(param)
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.updateDeviceConfiguration+device.id!, andParam: param, showHud: true) { (responseDict) in
            
            Helper.UI {
                
                var msg = ""
                if let errorMessage = responseDict["errorMessage"] as? String {
                    msg = errorMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                } else if let successMessage = responseDict["successMessage"] as? String {
                    msg = successMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func setResetZoneFencing () {
        let zone: [String:Any] = [
            "x1": 0,
            "y1": 0,
            "x2": 0,
            "y2": 0
        ]
        let param = ["enabled":true,
                     "zone": zone,
                     "clientImageWidth": 0,
                     "clientImageHeight": 0 ] as [String : Any]
        
        print(param)
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.setResetZoneFencing + device.id!, andParam: param, showHud: true) { (responseDict) in
            
            Helper.UI {
                Helper.sharedInstance.showToast(isError: false, title: "API call done")
                var msg = ""
                if let errorMessage = responseDict["errorMessage"] as? String {
                    msg = errorMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                } else if let successMessage = responseDict["successMessage"] as? String {
                    msg = successMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                } else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func convertToUTC(dateToConvert: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = dateFormatter.date(from: dateToConvert)
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            
            let localTime = dateFormatter.string(from: timeUTC!)
            return localTime
        }
        
        return nil
    }
    
    func timeFormat (time: String) -> String? {
        
        let dateAsString = time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"

        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        print("12 hour formatted Date:",Date12)
        return Date12
         
    }
}

//Mark:- Open Date Picker
@available(iOS 13.0, *)
extension FeatureConfigurationVC {
    
    func openDatePicker(dateType:dateType){
        
        let vc = UIStoryboard(name: "DatePicker", bundle: nil).instantiateViewController(withIdentifier: DatePickerVC.nameOfClass) as! DatePickerVC
        
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        
        
        
        switch dateType {
        case .startDate:
            vc.datePickerType  = .Date
            vc.minimumDate  = Calendar.current.date(byAdding: .year, value: -100, to: Date())!
            vc.maximumDate =  Date()
            
        case .startTime:
            vc.datePickerType = .Time
            
        case .endDate:
            vc.minimumDate  = Calendar.current.date(byAdding: .year, value: -100, to: Date())!
            vc.maximumDate =  Date()
            vc.datePickerType  = .Date
            
        case .endTime:
            vc.datePickerType  = .Time
        }
        
        vc.onSelectDone = { timeStamp, date in
            
            let dateFormatter: DateFormatter = DateFormatter()
            
            switch dateType {
            case .startDate:
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let _: String = dateFormatter.string(from: date)
                
                
            case .startTime:
                
                dateFormatter.dateFormat = "hh:mm a"
                let selectedTime : String = dateFormatter.string(from: date)
                self.btnFromTime.setTitle(selectedTime, for: .normal)
                self.startTime = selectedTime
                self.endTime = self.btnToTime.currentTitle ?? ""
              //  self.findTimeDifference(time1Str: self.startTime, time2Str: self.endTime)
            
            case .endDate:
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let _: String = dateFormatter.string(from: date)
                
                
            case .endTime:
                dateFormatter.dateFormat = "hh:mm a"
                let selectedTime: String = dateFormatter.string(from: date)
                self.btnToTime.setTitle(selectedTime, for: .normal)
                self.endTime = selectedTime
                self.startTime = self.btnFromTime.currentTitle ?? ""
                self.findTimeDifference(time1Str: self.startTime, time2Str: self.endTime)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func setupDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 34
        appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appearance.animationduration = 0.15
        appearance.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    }
    
    func timeConversion12(time24: String) -> String {
        let dateAsString = time24
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.dateFormat = "h:mm a"
        let date24 = dateFormatter.string(from: date!)
        print(date24)
        return date24
    }
}


// MARK:- SignalR Delegate
@available(iOS 13.0, *)
extension FeatureConfigurationVC: HubConnectionDelegate {
    func connectionDidOpen(hubConnection: HubConnection) {
        print("connectionDidOpen")
    }
    
    func connectionDidFailToOpen(error: Error) {
        print("connectionDidFailToOpen")
    }
    
    func connectionDidClose(error: Error?) {
        print("connectionDidClose")
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension FeatureConfigurationVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtLoitering {
            let maxLength = 2
            let currentString: NSString = txtLoitering.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else{
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tipView?.dismiss()
    }
}

//MARK:-
@available(iOS 13.0, *)
extension FeatureConfigurationVC: EasyTipViewDelegate {
    
    func easyTipViewDidTap(_ tipView: EasyTipView) {
    }
    
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        
    }
    
    func findTimeDifference(time1Str: String, time2Str: String) {
        //let time1 = time1Str
        //let time2 = time2Str
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        let date1 = formatter.date(from: time1Str)!
        let date2 = formatter.date(from: time2Str)!
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "hh:mm a"
        
        if let startDate = dateFormatter.date(from: time1Str),
           let endDate = dateFormatter.date(from: time2Str) {
            let hr = Calendar.current.dateComponents([.hour], from: startDate, to: endDate < startDate ? Calendar.current.date(byAdding: .day, value: 1, to: endDate) ?? endDate : endDate).hour ?? 0
            let elapsedTime = date2.timeIntervalSince(date1)
            // convert from seconds to hours, rounding down to the nearest hour
            let hours = floor(elapsedTime / 60 / 60)
            let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
            if Int(hours) == 0 && Int(hours) == 0 {
                lblTrespassingDuration.text = "Trespassing duration set off"
            }else {
                lblTrespassingDuration.text = "Trespassing duration set for \(Int(hr)) Hrs and  \(Int(minutes)) Mins (Everyday)"
            }
            
        }
    }
}

