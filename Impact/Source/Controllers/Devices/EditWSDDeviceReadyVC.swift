import UIKit

@available(iOS 13.0, *)
class EditWSDDeviceReadyVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension EditWSDDeviceReadyVC {
    
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionReady(_ sender: UIButton){
        NotificationCenter.default.post(name: Notification.Name("MoveWSD_VC"), object: "", userInfo: nil)
        self.dismiss(animated: true)
    }
    
}

