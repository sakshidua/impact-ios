import UIKit
import DropDown

@available(iOS 13.0, *)
class UpdateDeviceVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var btnLocation : UIButton!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var txtDeviceType : UITextField!
    
    var arrLocation = [LocationModel]()
    var locationId: String?
    var device: DeviceModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtDeviceType.text = device.name
        lblLocation.text = device.locationName
        locationId = device.locationId
        self.getLocationFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func getLocationFromServer()  {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getLocation, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                self.arrLocation.removeAll()
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    for dictData in arrTempData{
                        let objData = LocationModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
                        self.arrLocation.append(objData)
                    }
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension UpdateDeviceVC {
    
    func setupDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 34
        appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appearance.animationduration = 0.15
        appearance.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        
    }
    
    @IBAction func actionLocation(_ sender: UIButton){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrLocation.map { $0.name } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
           // self.txtLocation.text = item
            self.lblLocation.text = item
            locationId = self.arrLocation[index].id
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionUpdate(_ sender: UIButton){
        self.updateDeviceNameOrLocation()
    }
    
    func updateDeviceNameOrLocation () {
        
        let param = ["deviceName": self.lblLocation.text!,
                     "locationId": locationId!] as [String : Any]
        print(param)
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.updateDeviceNameOrLocation+device.id!, andParam: param, showHud: true) { (responseDict) in
            
            Helper.UI {
                var msg = ""
                if let errorMessage = responseDict["errorMessage"] as? String {
                    msg = errorMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                } else if let successMessage = responseDict["successMessage"] as? String {
                    msg = successMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}




//MARK:- Action Method
@available(iOS 13.0, *)
extension UpdateDeviceVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == txtDeviceType {
            let maxLength = 15
            let currentString: NSString = txtDeviceType.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else{
            return true
        }
    }
    
}
