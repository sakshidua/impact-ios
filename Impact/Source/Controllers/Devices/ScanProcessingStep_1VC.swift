import UIKit

//protocol EditDeviceVCDelegate:AnyObject {
//    func dismiss(VC:String)
//}

@available(iOS 13.0, *)
class ScanProcessingStep_1VC: BaseVC, UIViewControllerTransitioningDelegate {
    
    // weak var delegate : EditDeviceVCDelegate?
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageLoading : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    
    var arrImage : [String] = ["final_1", "final_2"]
    var param:[String:Any] = [:]
    var device: DeviceModel!
    var nameVC: String = ""
    
    private let itemsPerRow: CGFloat = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if nameVC == "DevicesDetailsVC" {
            lblTitle.text = "Do you see Red light on the device?"//"Do you see red light blinking on the Impact device?"
        }else{
            lblTitle.text = "Do you see Green light on the device?"
        }
        
        let url = Bundle.main.url(forResource: "loading", withExtension: "gif")
        DispatchQueue.main.async {
            self.imageLoading.sd_setImage(with: url!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ScanProcessingStep_1VC {
    
    @IBAction func actionYes(_ sender: UIButton){
        if nameVC == "DevicesDetailsVC" {
            let objVC : ScanProcessingStep_2VC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ScanProcessingStep_2VC.nameOfClass)
            objVC.device = device
            objVC.nameVC = nameVC
            self.navigationController?.pushViewController(objVC, animated: false)
        }else if nameVC == "EditDeviceVC"{
            let objVC : ScanProcessingStep_2VC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ScanProcessingStep_2VC.nameOfClass)
            objVC.nameVC = nameVC
            self.navigationController?.pushViewController(objVC, animated: false)
        }else{
            //self.addDevice()
            let objVC : ScanProcessingStep_2VC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ScanProcessingStep_2VC.nameOfClass)
            objVC.nameVC = nameVC
            objVC.param = param
            self.navigationController?.pushViewController(objVC, animated: false)
        }
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
    }
    
    @IBAction func action_Home(_ sender: UIButton){
        if nameVC == "DevicesDetailsVC" {
            Helper.sharedInstance.showToast(isError: false, title: "Click ‘Done’ to complete factory reset.")
        }else if nameVC == "EditDeviceVC"{
            Helper.sharedInstance.showToast(isError: false, title: "Please wait edit device is in progress.")
        } else{
            let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: false)
        }
    }
    
    
    @IBAction func actionNavigation_No(_ sender: Any) {
        //Helper.sharedInstance.showToast(isError: false, title: "Please scan the QR code.")
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: UICollectionView DataSource & Delegate Methods
@available(iOS 13.0, *)
extension ScanProcessingStep_1VC:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as? CollectionViewCell {
            cell.imgScan.image = UIImage.init(named: "final_\(indexPath.row+1)")
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
}
