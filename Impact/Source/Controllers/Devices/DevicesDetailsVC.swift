//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

protocol DevicesDetailsVCDelegate:AnyObject {
    func reloadDeviceVC()
}

enum ButtonAction: String {
    case search
    case setting
    case bookmark
    case firmware
    case peopleCount
}
//
@available(iOS 13.0, *)
class DevicesDetailsVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var lblFirmware : UILabel!
    @IBOutlet weak var lblPlanName : UILabel!
    @IBOutlet weak var lblRenewDate : UILabel!
    @IBOutlet weak var lblDeviceName : UILabel!
    @IBOutlet weak var lblDeviceLocation : UILabel!
    @IBOutlet weak var lblSubscriptionExpired : UILabel!
    @IBOutlet weak var lblSubscribedFeatures : UILabel!
    
    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var btnSetting : UIButton!
    @IBOutlet weak var btnBookmark : UIButton!
    @IBOutlet weak var btnFirmware : UIButton!
    @IBOutlet weak var btnPeopleCount : UIButton!
    @IBOutlet weak var btnRenewSubcription : UIButton!
    
    @IBOutlet weak var lineView   : UIView!
    @IBOutlet weak var searchType : UIView!
    @IBOutlet weak var othersType : UIView!
    @IBOutlet weak var viewSubscriptionInfo : UIView!
    @IBOutlet weak var viewRenewSubscription : UIView!
    
    @IBOutlet weak var tblOthersType : UITableView!
    @IBOutlet weak var tblSubscribeType : UITableView!
    @IBOutlet weak var cnsLineViewLeading: NSLayoutConstraint!
    
    var arrFirmware = [[String:Any]]()
    var arrSettingType = [[String:Any]]()
    var arrPeopleCount = [[String:Any]]()
    var arrSubscribeFeatures = [[String:Any]]()
    
    var buttonAction: ButtonAction = .search
    var device: DeviceModel!
    var arrAlerts = [HomeModel]()
    
    let bookmarkCellIdentifier = "TableViewCell"
    let subscriptionCellIdentifier = "TableViewCell"
    let settingCellIdentifier = "SettingFirmwareCell"
    let firmwareCellIdentifier = "SettingFirmwareCell"
    let peopleCountCellIdentifier = "SettingFirmwareCell"
    
    var stringCondition : String = ""
    var urlRenewSubscription : String = ""
    var urlExpiredSubscription : String = ""
    
    var limit : Int = 15
    var offset : Int = 0
    var isloadMore = false
    var didUpdateDevices: (() -> Void)?
    weak var delegate : DevicesDetailsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
}

//MARK: - Instance Methods
@available(iOS 13.0, *)
extension DevicesDetailsVC {
    
    func setup() {
        self.prepareStaticArray()
        self.populateDeviceDetail()
        self.showSearchType(true)
        self.buttonAction = .search
        self.lblMessage.isHidden = true
    }
    
    private func populateDeviceDetail() {
        
        self.viewSubscriptionInfo.isHidden = true
        self.viewRenewSubscription.isHidden = true
        self.tblSubscribeType.isHidden = true
        
        self.lblDeviceName.text = device.name
        self.lblDeviceLocation.text = device.locationName
        
        self.lblName.text = device.name?.capitalized
        self.lblLocation.text = device.locationName
        self.lblFirmware.text = device.firmwareVersion ?? "-"
        self.lblStatus.text = "Offline"
        if device.connected == true {
            self.lblStatus.text = "Online"
        }
        
        self.lblPlanName.text = "\(device.deviceType ?? "")-Promotional Plan"
        self.lblRenewDate.text = self.getExpiryDate(expiryDate: device.subscriptionExpiryDate)
        self.manageDeviceSubscription()
    }
    
    private func getExpiryDate(expiryDate: String?) -> String {
        if let timeResult = expiryDate {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            guard let date = dateFormatter.date(from: timeResult) else {
                return "-"
            }
            dateFormatter.dateFormat = "dd-MMM-YYYY"
            return dateFormatter.string(from: date)
        } else {
            return "-"
        }
    }
    
    private func manageDeviceSubscription() {
        let token = ApplicationPreference.getAccessToken() ?? ""
        var date = Date()
        if let strDate = device.subscriptionExpiryDate {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            date = dateFormatter.date(from: strDate) ?? Date()
        }
        
        if device.subscriptionId == nil {
            //Buy Subscription
            lblSubscribedFeatures.isHidden = true
            lblSubscriptionExpired.text = "Device doesn't have active subscription"
            btnRenewSubcription.setTitle("Buy New Subscription", for: .normal)
            
            stringCondition = "expiredSubscription"
            self.viewRenewSubscription.isHidden = false
            
            let BUY_SUBSCRIPTION_URL_STAGING = "https://tev-web-staging.azurewebsites.net/tev/plan?accessToken="
            let url = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "&headless=true" + "&deviceId=\(device.id ?? "")" + "&product=\(device.deviceType ?? "")"
            urlExpiredSubscription = url
            
        } else if (Date() > date) {
            //Renew Subscription
            lblSubscriptionExpired.text = "Device subscription expired on " + "\(self.convertDateFormat(inputDate: device.subscriptionExpiryDate!))"
            btnRenewSubcription.setTitle("Renew Subscription", for: .normal)
            
            stringCondition = "renewSubscription"
            self.viewRenewSubscription.isHidden = false
            
            let BUY_SUBSCRIPTION_URL_STAGING = "https://tev-web-staging.azurewebsites.net/tev/plan?accessToken="
            let url = BUY_SUBSCRIPTION_URL_STAGING + "\(token)" + "&headless=true" + "&deviceId=\(device.id ?? "")" + "&product=\(device.deviceType ?? "")" + "&subscriptionNumber=\(device.subscriptionId ?? "")"
            urlRenewSubscription = url
            
        } else {
            self.viewRenewSubscription.isHidden = true
            self.viewSubscriptionInfo.isHidden = false
            self.tblSubscribeType.isHidden = false
            guard let id = device.subscriptionId else { return }
            self.subscriptionDetailFromServer(subscriptionId:id)
        }
    }
    
    func convertDateFormat(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let oldDate = olDateFormatter.date(from: inputDate)
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "dd MMM yyyy"
        return convertDateFormatter.string(from: oldDate!)
    }
    
    private func prepareStaticArray() {
        
        // Settings
        arrSettingType.removeAll()
        var dict = [String:Any]()
        if device.deviceType != "WSD" {
            dict.updateValue("Feature configuration", forKey: "title")
            dict.updateValue("features-devices", forKey: "icon")
            self.arrSettingType.append(dict)
        }
        
        if device.deviceType == "WSD" {
            dict.updateValue("Delete device", forKey: "title")
            dict.updateValue("delete-device", forKey: "icon")
            self.arrSettingType.append(dict)
        }else{
            
            dict.updateValue("Update software", forKey: "title")
            dict.updateValue("replace-device", forKey: "icon")
            self.arrSettingType.append(dict)
            
            dict.updateValue("Factory reset", forKey: "title")
            dict.updateValue("delete-device", forKey: "icon")
            self.arrSettingType.append(dict)
            
            //             dict.updateValue("Replace this device", forKey: "title")
            //             dict.updateValue("replace-device", forKey: "icon")
            //             self.arrSettingType.append(dict)
        }
        
        // Firmware
        arrFirmware.removeAll()
        
        dict.updateValue("Software Version History", forKey: "title")
        dict.updateValue("firmware-version", forKey: "icon")
        self.arrFirmware.append(dict)
        
        if device.deviceType == "WSD" {
            dict.updateValue("Emergency Call History", forKey: "title")
            dict.updateValue("emergency_call", forKey: "icon")
            self.arrFirmware.insert(dict, at: 0)
            
            dict.updateValue("Device Health Details", forKey: "title")
            dict.updateValue("device_health", forKey: "icon")
            self.arrFirmware.append(dict)
        }
        
        // People Counting
        btnPeopleCount.isHidden = true
        if device.deviceType != "WSD" {
            arrPeopleCount.removeAll()
            btnPeopleCount.isHidden = false
            
            dict.updateValue("People Counting", forKey: "title")
            dict.updateValue("people_counting", forKey: "icon")
            self.arrPeopleCount.append(dict)
        }
    }
    
    private func showSearchType(_ hidden:Bool) {
        searchType.isHidden = !hidden
        othersType.isHidden = hidden
        tblOthersType.isHidden = hidden
    }
    
    private func reload() {
        switch buttonAction {
        case .search:
            tblOthersType.reloadData()
        case .bookmark:
            arrAlerts = arrAlerts.filter({ alert in
                alert.bookMarked == true
            })
            !arrAlerts.isEmpty ? hideTableView(isHidden: false) : hideTableView(isHidden: true)
        default:
            break
        }
    }
    
    private func hideTableView(isHidden: Bool) {
        self.tblOthersType.isHidden = isHidden
        self.lblMessage.isHidden = !isHidden
        self.tblOthersType.reloadData()
    }
    
    
    private func subscriptionCell(indexPath:IndexPath) -> UITableViewCell {
        if let cell = tblSubscribeType.dequeueReusableCell(withIdentifier: subscriptionCellIdentifier, for: indexPath) as? TableViewCell {
            let obj = self.arrSubscribeFeatures[indexPath.row]
            cell.lblTitle.text = obj["name"] as? String ?? ""
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    private func settingsCell(indexPath:IndexPath) -> UITableViewCell {
        if let cell = tblOthersType.dequeueReusableCell(withIdentifier: settingCellIdentifier, for: indexPath) as? TableViewCell{
            let obj = self.arrSettingType[indexPath.row]
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    private func bookmarkCell(indexPath:IndexPath) -> UITableViewCell {
        
        if let cell = tblOthersType.dequeueReusableCell(withIdentifier: bookmarkCellIdentifier, for: indexPath) as? TableViewCell {
            let data = self.arrAlerts[indexPath.row]
            cell.lblTitle.text = (data.alertType ?? "").capitalized + " Detection"
            if let timeResult = (data.occurenceTimeStamp) {
                let date = NSDate(timeIntervalSince1970: TimeInterval(timeResult))
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
                
                let dayTimePeriodFormatterTime = DateFormatter()
                dayTimePeriodFormatterTime.dateFormat = "hh:mm a"
                
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
                let timeString = dayTimePeriodFormatterTime.string(from: date as Date)
                
                cell.lblDateTime.text = dateString + " at " + timeString
            }
            cell.imgUrl.sd_setImage(with: URL(string: data.imageUrl ?? ""), placeholderImage: UIImage(named: "picture"))
            cell.btnImage.tag = indexPath.row
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    private func firmwareCell(indexPath:IndexPath) -> UITableViewCell {
        if let cell = tblOthersType.dequeueReusableCell(withIdentifier: firmwareCellIdentifier, for: indexPath) as? TableViewCell {
            let obj = self.arrFirmware[indexPath.row]
            cell.lblTitle.text = obj["title"] as? String
            cell.imgIcon.image = UIImage(named: obj["icon"] as! String)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    private func peopleCountCell(indexPath:IndexPath) -> UITableViewCell {
        if let cell = tblOthersType.dequeueReusableCell(withIdentifier: peopleCountCellIdentifier, for: indexPath) as? TableViewCell {
            let obj = self.arrPeopleCount[indexPath.row]
            cell.lblTitle.text = obj["title"] as? String
            cell.imgIcon.image = UIImage(named: obj["icon"] as! String)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    private func showConfirmationView(parentViewType: ParentViewType) {
        let objVC : ConfirmationVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ConfirmationVC.nameOfClass)
        objVC.delegate = self
        objVC.device = self.device
        objVC.parentViewType = parentViewType
        objVC.modalTransitionStyle = .crossDissolve
        objVC.modalPresentationStyle = .overFullScreen
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
    }
    
    private func showFactoryResetView() {
        let objVC : FactoryReset_1VC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: FactoryReset_1VC.nameOfClass)
        objVC.delegate = self
        //      objVC.device = self.device
        //     objVC.parentViewType = parentViewType
        objVC.modalTransitionStyle = .crossDissolve
        objVC.modalPresentationStyle = .overFullScreen
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
    }
    
    private func showViewFeatureConfiguration() {
        let objVC : FeatureConfigurationVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: FeatureConfigurationVC.nameOfClass)
        objVC.device = device
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    private func navigateToDeviceRaplacement() {
        let objVC : DeviceReplacementVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DeviceReplacementVC.nameOfClass)
        objVC.device = self.device
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    private func navigateToEmergencyCallHistory() {
        let objVC : EmergencyCallVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: EmergencyCallVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    private func navigateToFirmwareVersionHistory() {
        let objVC : FirmwareHistoryVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: FirmwareHistoryVC.nameOfClass)
        objVC.device = self.device
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    private func navigateToDeviceHealthDetails() {
        let objVC : DeviceHealthVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DeviceHealthVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    private func openPeopleCount() {
        let STAGING_URL = "https://tev-web-staging.azurewebsites.net/tev/people-count?"
        let strUrl = STAGING_URL + "deviceId=\(self.device.id ?? "")" + "&accessToken=\(ApplicationPreference.getAccessToken() ?? "")"
        guard let url = URL(string: strUrl) else {
            print("can not open url") ; return }
        UIApplication.shared.open(url)
    }
}

//MARK: - Button Action
@available(iOS 13.0, *)
extension DevicesDetailsVC {
    @IBAction func actionSearchType(_ sender: UIButton) {
        if self.buttonAction != .search {
            self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
            self.lblMessage.isHidden = true
            self.buttonAction = .search
            self.showSearchType(true)
        }
    }
    
    @IBAction func actionSettingType(_ sender: UIButton) {
        if self.buttonAction != .setting {
            self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
            self.buttonAction = .setting
            self.lblMessage.isHidden = true
            self.showSearchType(false)
            self.tblOthersType.reloadData()
        }
    }
    
    @IBAction func actionBookmarkType(_ sender: UIButton) {
        if self.buttonAction != .bookmark {
            self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
            self.buttonAction = .bookmark
            self.showSearchType(false)
            self.offset = 0
            let param = ["take": self.limit,
                         "skip": self.offset,
                         "deviceId": self.device.id ?? "" ] as [String : Any]
            self.getAllAlertsFromServer(param: param)
        }
    }
    
    @IBAction func actionTimeType(_ sender: UIButton) {
        if self.buttonAction != .firmware {
            self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
            self.buttonAction = .firmware
            self.showSearchType(false)
            self.tblOthersType.reloadData()
        }
    }
    
    @IBAction func actionPeopleCounting(_ sender: UIButton) {
        if self.buttonAction != .peopleCount {
            self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
            self.buttonAction = .peopleCount
            self.showSearchType(false)
            self.tblOthersType.reloadData()
        }
    }
    
    @IBAction func actionManageSubscription(_ sender: UIButton) {
        let objVC : ManageSubscriptionVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ManageSubscriptionVC.nameOfClass)
        objVC.delegate = self
        objVC.device = self.device
        objVC.transitioningDelegate = self
        objVC.modalTransitionStyle = .coverVertical
        objVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(objVC, animated: true)
    }
    
    @IBAction func actionImageTap(_ sender: UIButton){
        
        //alertId: String
        let data = self.arrAlerts[sender.tag]
        
        var url: [String] = []
        url.append((data.imageUrl) ?? "")
        url.append((data.videoUrl) ?? "")
        
        let objVC : HomeImageVideoVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeImageVideoVC.nameOfClass)
        objVC.url = url
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
        DispatchQueue.main.async {
            self.acknowledgeFromServer(alertId: data.alertId!, index: sender.tag)
        }
    }
    
    @IBAction func actionRenewSubcription(_ sender: UIButton){
        self.view.endEditing(true)
        
        if stringCondition == "renewSubscription" {
            guard let url = URL(string: urlRenewSubscription) else { return }
            UIApplication.shared.open(url)
        } else {
            guard let url = URL(string: urlExpiredSubscription) else { return }
            UIApplication.shared.open(url)
        }
        
        guard let url = URL(string: "") else { return }
        UIApplication.shared.open(url)
        
        self.viewRenewSubscription.isHidden = true
        self.viewSubscriptionInfo.isHidden = false
        self.tblSubscribeType.isHidden = false
    }
    
    @IBAction func actionEdit(_ sender: UIButton) {
        
        if device.deviceType == "WSD" {
            
            let objVC : EditWSDDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: EditWSDDeviceVC.nameOfClass)
            objVC.delegate = self
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
            
        }else{
            let objVC : EditDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: EditDeviceVC.nameOfClass)
            objVC.delegate = self
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
        }
      
        
        
    }
}


//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension DevicesDetailsVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblSubscribeType {
            return arrSubscribeFeatures.count
        } else {
            switch buttonAction {
            case .setting:
                return arrSettingType.count
            case .bookmark:
                return arrAlerts.count
            case .firmware:
                return arrFirmware.count
            case .peopleCount:
                return arrPeopleCount.count
            default:
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblOthersType {
            switch buttonAction {
            case .setting:
                return settingsCell(indexPath: indexPath)
            case .bookmark:
                return bookmarkCell(indexPath: indexPath)
            case .firmware:
                return firmwareCell(indexPath: indexPath)
            case .peopleCount:
                return peopleCountCell(indexPath: indexPath)
            default:
                return UITableViewCell()
            }
        } else {
            return subscriptionCell(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch buttonAction {
        
        case .setting: do {
            let title = (arrSettingType[indexPath.row]) ["title"] as! String
            switch title {
            case "Feature configuration" : self.showViewFeatureConfiguration()
            case "Update software": self.showConfirmationView(parentViewType: .updateFirmware)
            case "Delete device": self.showConfirmationView(parentViewType: .deleteDevice)
            case "Factory reset": self.showFactoryResetView()
            default: break
            }
        }; break
        
        case .firmware:  do {
            let title = (arrFirmware[indexPath.row]) ["title"] as! String
            switch title {
            case "Software Version History" : self.navigateToFirmwareVersionHistory()
            case "Emergency Call History": self.navigateToEmergencyCallHistory()
            case "Device Health Details": self.navigateToDeviceHealthDetails()
            default: break
            }
        }; break
        
        case .peopleCount:  do {
            let title = (arrPeopleCount[indexPath.row]) ["title"] as! String
            switch title {
            case "People Counting" : self.openPeopleCount()
            default: break
            }
        }; break
        
        default:
            break
        }
    }
}

//MARK: - Custom Delegate
@available(iOS 13.0, *)
extension DevicesDetailsVC: ConfirmationVCDelegate, ManageSubscriptionVCDelegate {
    func makeAPICallFor(parentViewType: ParentViewType) {
        switch parentViewType {
        case .subscription:
            self.cancelSubscriptionFromServer()
        case .reactivate:
            self.reactivateSubscriptionFromServer()
        case .updateFirmware:
            self.firmwareUpdateFromServer()
            self.dismissController()
        case .deviceRestore:
            self.dismissController()
        //            self.deviceRestoreFromServer()
        case .deleteDevice:
            self.deleteDeviceFromServer()
        case .factoryReset:
            print("factoryReset")
        }
    }
}

//MARK: - API Calls
@available(iOS 13.0, *)
extension DevicesDetailsVC {
    
    private func subscriptionDetailFromServer(subscriptionId:String) {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.subscriptionDetail + subscriptionId, andParam: [:], showHud: false) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [String: Any],
                   let addOns = arrTempData["addOns"] as? [[String: Any]]  {
                    self.arrSubscribeFeatures = addOns
                    self.tblSubscribeType.reloadData()
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    private func getAllAlertsFromServer(param:[String:Any])  {
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.getAllAlerts, andParam: param, showHud: false) { (responseDict) in
            
            Helper.UI {
                self.isloadMore = true
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]],
                   !arrTempData.isEmpty {
                    if self.offset == 0 {
                        self.arrAlerts.removeAll()
                    }
                    for dictData in arrTempData{
                        let objData = HomeModel.init(alertId: dictData["alertId"] as? String, alertType: dictData["alertType"] as? String, imageUrl: dictData["imageUrl"] as? String, occurenceTimeStamp: dictData["occurenceTimeStamp"] as? Int, deviceName: dictData["deviceName"] as? String, deviceId: dictData["deviceId"] as? String, locationName: dictData["locationName"] as? String, locationId: dictData["locationId"] as? String, acknowledged: dictData["acknowledged"] as? Bool ?? false, bookMarked: dictData["bookMarked"] as? Bool ?? false, isCorrect: dictData["isCorrect"] as? Bool ?? false, comment: dictData["comment"] as? String, videoUrl: dictData["videoUrl"] as? String, device: dictData["device"] as? String, smokeValue: dictData["smokeValue"] as? Int)
                        self.arrAlerts.append(objData)
                    }
                    self.reload()
                } else {
                    if self.offset == 0 {
                        self.arrAlerts.removeAll()
                    }
                    self.isloadMore = false
                    self.reload()
                }
            }
        }
    }
    
    private func acknowledgeFromServer(alertId:String, index:Int) {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getAcknowledge+alertId, andParam: [:], showHud: false) { (responseDict) in
            Helper.UI {
                self.arrAlerts[index].acknowledged = true
            }
        }
    }
    
    private func cancelSubscriptionFromServer() {
        let param = ["subscriptionId": device.subscriptionId ?? "",
                     "deviceId": device.id ?? "",
                     "deviceName": device.name ?? ""] as [String : Any]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.cancelSubscription, andParam: param , showHud: true) { (responseDict) in
            Helper.UI {
                if let message = responseDict["responseBody"] as? String {
                    Helper.sharedInstance.showToast(isError: true, title: message)
                    self.device.subscriptionStatus = "non_renewing"
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.didUpdateDevices?()
                self.dismissController()
            }
        }
    }
    
    private func reactivateSubscriptionFromServer() {
        let param = ["subscriptionId": device.subscriptionId ?? "",
                     "deviceId": device.id ?? "",
                     "deviceName": device.name ?? ""] as [String : Any]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.reactivateSubscription, andParam: param , showHud: true) { (responseDict) in
            Helper.UI {
                if let message = responseDict["responseBody"] as? String {
                    Helper.sharedInstance.showToast(isError: true, title: message)
                    self.device.subscriptionStatus = "live"
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.didUpdateDevices?()
                self.dismissController()
            }
        }
    }
    
    private func firmwareUpdateFromServer() {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.firmwareUpdate + (device.id ?? ""), andParam: [:] , showHud: true) { (responseDict) in
            Helper.UI {
                if let _ = responseDict["responseBody"] as? [String: Any] {
                    //                    Helper.sharedInstance.showToast(isError: true, title: msg)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.dismissController()
            }
        }
    }
    
    private func deviceRestoreFromServer() {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.deviceRestore + (device.id ?? ""), andParam: [:] , showHud: true) { (responseDict) in
            Helper.UI {
                if let _ = responseDict["responseBody"] as? [[String: Any]] {
                    let msg = "Device scheduled for factory reset, you will get a notification after the update."
                    Helper.sharedInstance.showToast(isError: true, title: msg)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.dismissController()
            }
        }
    }
    
    private func deleteDeviceFromServer() {
        var hardFactoryReset = false
        if device.deviceType == "TEV" {
            hardFactoryReset = true }
        
        APIManagerApp.shared.makeAPIRequest(ofType: DELETE, withEndPoint: APIEndPoint.deleteDevice(deviceId: (device.id ?? ""), hardFactoryReset: hardFactoryReset), andParam: [:] , showHud: true) { (responseDict) in
            Helper.UI {
                if let successMessage = responseDict["successMessage"] as? String {
                    let msg = "Device marked for deletion. The device will be deleted immediately if device is online else it will be deleted when device comes back online."
                    Helper.sharedInstance.showToast(isError: true, title: msg)
                    self.delegate?.reloadDeviceVC()
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                
                self.dismissController()
            }
        }
    }
}

@available(iOS 13.0, *)
extension DevicesDetailsVC : EditDeviceVCDelegate {
    func dismiss(VC: String) {
        
        if VC == "EditDeviceReadyVC" {
            let objVC : EditDeviceReadyVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: EditDeviceReadyVC.nameOfClass)
            objVC.device = device
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.navigationController?.present(objVC, animated: true)
            
        }else{
            let objVC : UpdateDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: UpdateDeviceVC.nameOfClass)
            objVC.device = device
            objVC.modalPresentationStyle = .custom
            objVC.transitioningDelegate = self
            self.present(objVC, animated: true)
        }
    }
}

@available(iOS 13.0, *)
extension DevicesDetailsVC : FactoryReset_1VCDelegate {
    
    func dismissView() {
        
        let objVC : FactoryResetVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: FactoryResetVC.nameOfClass)
        objVC.delegate = self
        // objVC.device = self.device
        // objVC.parentViewType = parentViewType
        objVC.modalTransitionStyle = .crossDissolve
        objVC.modalPresentationStyle = .overFullScreen
        objVC.transitioningDelegate = self
        self.navigationController?.present(objVC, animated: true)
        
    }
    
}


@available(iOS 13.0, *)
extension DevicesDetailsVC : EditWSDDeviceVCDelegate {
    
    func dismissVC() {
        
        let objVC : EditWSDDeviceReadyVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: EditWSDDeviceReadyVC.nameOfClass)
        //objVC.device = device
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        self.present(objVC, animated: true)
        
    }
    
}


@available(iOS 13.0, *)
extension DevicesDetailsVC : FactoryResetVCDelegate {
    
    func dismiss() {
        
        let objVC : SacnDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: SacnDeviceVC.nameOfClass)
        objVC.device = device
        objVC.nameVC = "DevicesDetailsVC"
        self.navigationController?.pushViewController(objVC, animated: false)
        
    }
    
}



