//
//  CancelSubscriptionVC.swift
//  Impact
//
//  Created by Mohd Ali Khan on 28/05/21.
//

import UIKit


@available(iOS 13.0, *)
class CancelSubscriptionVC: BaseVC {
    
    @IBOutlet weak var viewBottom : UIView!
    
    var device: DeviceModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    
    func setInitialData(){
        self.tapToDismiss()
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension CancelSubscriptionVC {
    
    @IBAction func actionYes(_ sender: UIButton){
        cancelSubscriptionFromServer()
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.dismissController()
    }
}




//MARK: - API Calls
@available(iOS 13.0, *)
extension CancelSubscriptionVC {
    
    private func cancelSubscriptionFromServer() {
        let param = ["subscriptionId": device.subscriptionId,
                     "deviceId": device.id,
                     "deviceName": device.name] as [String : Any]

        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.cancelSubscription, andParam: param , showHud: true) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]] {
                    print("subscriptionDetailFromServer ---> \(arrTempData)")
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}
