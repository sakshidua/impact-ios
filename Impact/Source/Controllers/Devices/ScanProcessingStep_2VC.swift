import UIKit

//protocol EditDeviceVCDelegate:AnyObject {
//    func dismiss(VC:String)
//}

@available(iOS 13.0, *)
class ScanProcessingStep_2VC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDone : UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnDone : UIButton!
    @IBOutlet weak var lblProcessing : UILabel!
    @IBOutlet weak var imgProcessing : UIImageView!
    @IBOutlet weak var topConstant: NSLayoutConstraint!
    
    var arrImage : [String] = ["final_1", "final_2"]
    var device: DeviceModel!
    var param:[String:Any] = [:]
    var nameVC: String = ""
    var responceMessage: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if nameVC == "DevicesDetailsVC" {
            lblProcessing.isHidden = true
            imgProcessing.isHidden = true
            lblDone.text = "Click 'Done' to complete factory reset."
            lblTitle.text = ""
        }else if nameVC == "EditDeviceVC" {
            topConstant.constant = 70
            lblProcessing.isHidden = true
            imgProcessing.isHidden = true
            lblTitle.text = ""
            lblDone.text = "Click 'Done' to complete this activity.\nYour device will be online shortly."
        }else{
            
            let url = Bundle.main.url(forResource: "loading", withExtension: "gif")
            DispatchQueue.main.async {
            self.imgProcessing.sd_setImage(with: url!)
            }
            
            self.isHidden(hidden: true)
            self.addDevice()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ScanProcessingStep_2VC {
    
    func isHidden (hidden : Bool) {
        
        lblProcessing.isHidden = !hidden
        imgProcessing.isHidden = !hidden
        btnDone.isHidden = hidden
        lblDone.isHidden = hidden
        lblTitle.isHidden = hidden
        
    }
    
    @IBAction func action_Home(_ sender: UIButton){
        
        if nameVC == "DevicesDetailsVC" {
            Helper.sharedInstance.showToast(isError: false, title: "Please click 'Done' to complete this activity.")
        }else if nameVC == "EditDeviceVC" {
            let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: false)
        }else{
            if lblProcessing.isHidden == false {
                Helper.sharedInstance.showToast(isError: false, title: "Please wait add device in progress.")
            }else{
                let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
                self.navigationController?.pushViewController(objVC, animated: false)
            }
        }
    }
    
    @IBAction func actionDone(_ sender: UIButton){
        
        if nameVC == "DevicesDetailsVC" {
            self.deleteDeviceFromServer()
        }else{
            let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: false)
        }
    }
}


//MARK: UICollectionView DataSource & Delegate Methods
@available(iOS 13.0, *)
extension ScanProcessingStep_2VC:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as? CollectionViewCell {
            cell.imgScan.image = UIImage.init(named: "final_\(indexPath.row+1)")
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    private func deleteDeviceFromServer() {
        var hardFactoryReset = false
        if device.deviceType == "TEV" {
            hardFactoryReset = true }
        
        APIManagerApp.shared.makeAPIRequest(ofType: DELETE, withEndPoint: APIEndPoint.deleteDevice(deviceId: (device.id ?? ""), hardFactoryReset: hardFactoryReset), andParam: [:] , showHud: true) { (responseDict) in
            Helper.UI {
                if let successMessage = responseDict["successMessage"] as? String {
                    let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
                    self.navigationController?.pushViewController(objVC, animated: false)
                     let msg = "Device marked for deletion. The device will be deleted immediately if device is online else it will be deleted when device comes back online."
                     Helper.sharedInstance.showToast(isError: true, title: msg)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.dismissController()
            }
        }
    }
}



//MARK:
@available(iOS 13.0, *)

extension ScanProcessingStep_2VC {
    
    func addDevice()  {
        
        let param = ["deviceId":ApplicationPreference.getUuidString() ?? "",
                     "isNewSite": false,
                     "siteName":param["ln"] ?? "",
                     "siteId":param["li"] ?? "",
                     "application":"TEV"] as [String : Any]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.addDevice, andParam: param as [String : Any], showHud: true) { (responseDict) in
            
            Helper.UI {
                var msg = ""
                if let errorMessage = responseDict["errorMessage"] as? String {
                    msg = errorMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                    self.dismiss(animated: true)
                } else if let successMessage = responseDict["successMessage"] as? String {
                    msg = successMessage
                    DispatchQueue.main.asyncAfter(deadline: .now() + 30.0) {
                        self.getDeviceSetupStatus()
                    }
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func getDeviceSetupStatus()  {
        //call API
        let param = ["logicalDeviceId":ApplicationPreference.getUuidString() ?? ""] as [String : Any]
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.deviceSetupStatus, andParam: param, showHud: true, changeBaseUrl: true) { (responseDict) in
            Helper.UI {
                print(responseDict)
               // Spinner.hide()
                var msg = ""
                if let errorMessage = responseDict["errorMessage"] as? String {
                    self.isHidden(hidden: false)
                    msg = errorMessage
                    self.lblTitle.text = "Oops! setup did not complete  Please try again later."
                    
                } else if let successMessage = responseDict["successMessage"] as? String {
                    self.isHidden(hidden: false)
                    //Spinner.hide()
                    msg = successMessage
                    self.lblTitle.text = "Congratulations !! Setup completed."
                    
                }else{
                    //Spinner.show("")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.10) {
                        self.getDeviceSetupStatus()
                    }
                }
            }
        }
    }
}
