//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

@available(iOS 13.0, *)
class DevicesVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewFilter : UIView!
    @IBOutlet weak var tblDeviceType : UITableView!
    
    var filterLocationId = ""
    var filterDeviceType = ""
    let cellIdentifier = "TableViewCell"
    var arrDevices = [DeviceModel]()
    var arrDevicesCopy = [DeviceModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
}

//MARK: - Instance Method
@available(iOS 13.0, *)
extension DevicesVC {
    
    func setup() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToAnotherVC), name: Notification.Name("MoveVC"), object:nil )
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToWSDVC), name: Notification.Name("MoveWSD_VC"), object: nil)
        
        tblDeviceType.delegate = self
        tblDeviceType.dataSource = self
        getDevicesFromServer()
    }
    
    private func reload() {
        if filterDeviceType != "" {
            arrDevices = arrDevicesCopy.filter { model in
                model.deviceType == filterDeviceType
            }
        }
        tblDeviceType.isHidden = false
        tblDeviceType.reloadData()
        if arrDevices.isEmpty {
            tblDeviceType.isHidden = true
        }
    }
    

    private func parseDeviceData(_ arrTempData:[[String: Any]]) {
        for dictData in arrTempData {
            let objData = DeviceModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, connected: dictData["connected"] as? Bool ?? false, locationId: dictData["locationId"] as? String, locationName: dictData["locationName"] as? String, macAddress: dictData["macAddress"] as? String, firmwareVersion: dictData["firmwareVersion"] as? String, wifiName: dictData["wifiName"] as? String, subscriptionId: dictData["subscriptionId"] as? String, availableFeatures: dictData["availableFeatures"] as? [String], subscriptionExpiryDate: dictData["subscriptionExpiryDate"] as? String, currentUserPermission: dictData["currentUserPermission"] as? String, devicePermissions: dictData["devicePermissions"] as? [[String:Any]], deviceType: dictData["deviceType"] as? String, subscriptionStatus: dictData["subscriptionStatus"] as? String, disabled: dictData["disabled"] as? Bool ?? false, unacknowledgedAlert: dictData["unacknowledgedAlert"] as? Int ?? 0)
            self.arrDevicesCopy.append(objData)
            self.arrDevices.append(objData)
        }
        
        if !self.arrDevices.isEmpty {
            self.getUnacknowledgedAlertsCountFromServer()
        } else {
            self.reload()
        }
    }
    
    private func didUpdateDevices() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.getDevicesFromServer()
        }
    }
    
    @objc func moveToAnotherVC(notification: Notification) {
        
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if let device = dict["device"] as? DeviceModel{
                let objVC : ConnectDeviceVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: ConnectDeviceVC.nameOfClass)
                objVC.device = device
                objVC.nameVC = "EditDeviceVC"
                self.navigationController?.pushViewController(objVC, animated: false)
            }
        }
        
       
    }
    
    @objc func moveToWSDVC(notification: Notification) {
        let objVC : WSDSetupDevice = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.setupDevice, viewControllerName: WSDSetupDevice.nameOfClass)
        objVC.nameVC = "EditWSDDeviceVC"
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
}

//MARK: - Button Action
@available(iOS 13.0, *)
extension DevicesVC {
    @IBAction func actionFilter(_ sender: UIButton) {
        let objVC : DeviceFilterVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DeviceFilterVC.nameOfClass)
        objVC.modalPresentationStyle = .custom
        objVC.transitioningDelegate = self
        objVC.delegate = self
        objVC.locationId = filterLocationId
        objVC.deviceId = filterDeviceType
        self.navigationController?.present(objVC, animated: true)
    }
    
    @IBAction func actionAddDevice(_ sender: UIButton) {
        self.view.endEditing(true)
    }
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension DevicesVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDevices.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell {
            let item = arrDevices[indexPath.row]
            cell.lblTitle.text = item.name
            cell.lblCount.text = "(\(item.unacknowledgedAlert ?? 0))"
            cell.lblLocationName.text = item.locationName
            cell.lblSubscription.isHidden = true
            cell.bgView.backgroundColor = .white
            cell.viewOnline.backgroundColor = (item.connected == true) ? .green :  .red
            
            if let deviceType = item.deviceType {
                if deviceType == "TEV" {
                    cell.imgIcon.image = UIImage.init(named: "video_cam")
                }else{
                    cell.imgIcon.image = UIImage.init(named: "WSD_logo")
                }
            }
            if let expiryDate = item.subscriptionExpiryDate {
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                let date = dateFormatter.date(from: expiryDate)
                
                if item.subscriptionId == nil {
                    cell.lblSubscription.isHidden = false
                    cell.bgView.backgroundColor = UIColor.appGrayBackground()
                } else if (Date() > date!) {
                    cell.lblSubscription.isHidden = false
                    cell.bgView.backgroundColor = UIColor.appGrayBackground()
                } else {
                    cell.lblSubscription.isHidden = true
                    cell.bgView.backgroundColor = UIColor.white
                }
            }
            return cell
        } else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objVC : DevicesDetailsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: DevicesDetailsVC.nameOfClass)
        objVC.delegate = self
        objVC.device = self.arrDevices[indexPath.row]
        objVC.didUpdateDevices = didUpdateDevices
        self.navigationController?.pushViewController(objVC, animated: true)
    }
}


//MARK: - Custom Delegate
@available(iOS 13.0, *)
extension DevicesVC: DeviceFilterVCDelegate {
    func didSelectFilter(location: String, type: String) {
        if location != "" {
            filterLocationId = location
            getDeviceByLocation(id: location)
        } else {
          //  arrDevicesCopy = arrDevices
            filterDeviceType = type
            reload()
        }
    }
    
    func didClearFilter() {
        filterDeviceType = ""
        arrDevices = arrDevicesCopy
        arrDevicesCopy.removeAll()
        reload()
    }
}

//MARK: - API Call
@available(iOS 13.0, *)
extension DevicesVC {
    func getDevicesFromServer()  {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getDevices, andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]] {
                    self.arrDevices.removeAll()
                    self.parseDeviceData(arrTempData)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func getUnacknowledgedAlertsCountFromServer()  {
        let ids = arrDevices.compactMap { $0.id }
        let param = ["ids": ids] as [String:Any]
        
        APIManagerApp.shared.makeAPIRequest(ofType: POST, withEndPoint: APIEndPoint.unacknowledgedAlertsCount, andParam: param, showHud: false) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]] {
                    for temp in arrTempData {
                        let device = self.arrDevices.filter { model in
                            model.id == temp["deviceId"] as? String
                        }
                        if !device.isEmpty {
                            device.first?.unacknowledgedAlert = temp["count"] as? Int
                        }
                    }
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
                self.reload()
            }
        }
    }
    
    func getDeviceByLocation(id:String)  {
        
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getDevicesByLocation + id, andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]] {
                    self.arrDevicesCopy.removeAll()
                    self.arrDevicesCopy = self.arrDevices
                    self.arrDevices.removeAll()
                    self.parseDeviceData(arrTempData)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    self.tblDeviceType.isHidden = true
                    self.viewFilter.isHidden = true
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}


//MARK: - API Call
@available(iOS 13.0, *)
extension DevicesVC: DevicesDetailsVCDelegate {
    func reloadDeviceVC() {
        self.setup()
    }
}
