//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit
import DropDown

protocol DeviceFilterVCDelegate: AnyObject {
    func didSelectFilter(location:String, type:String)
    func didClearFilter()
}

@available(iOS 13.0, *)
class DeviceFilterVC: BaseVC {
    
    @IBOutlet weak var lblDevice : UILabel!
    @IBOutlet weak var lblLocation : UILabel!

    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var viewContainer : UIView!
    
    var deviceId = ""
    var locationId = ""
    weak var delegate: DeviceFilterVCDelegate?
    
    var arrLocation = [LocationModel]()
    var arrDeviceType = [DeviceTypeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if deviceId != "" {
            self.lblDevice.text = deviceId
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Instance Methods
@available(iOS 13.0, *)
extension DeviceFilterVC {
    private  func setup() {
        // get Location
        self.getLocationFromServer()
        // get Device Types
        self.getDevicesFromServer()
        self.tapToDismiss()
    }
    
    func setupDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 34
        appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        appearance.animationduration = 0.15
        appearance.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    }
    
    private func parseLocation(_ arrTempData: [[String: Any]]) {
        for dictData in arrTempData {
            let objData = LocationModel.init(id: dictData["id"] as? String, name: dictData["name"] as? String, isSelected: false)
            self.arrLocation.append(objData)
        }
        
        if !self.arrLocation.isEmpty {
            self.arrLocation = self.arrLocation.sorted(by: {$1.name ?? "" > $0.name ?? ""})
            if let location = (self.arrLocation.filter { $0.id == self.locationId }).first {
                self.lblLocation.text = location.name
            }
        }
    }
}

//MARK:- Button Action
@available(iOS 13.0, *)
extension DeviceFilterVC {
    @IBAction func actionClose(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
    
    @IBAction func actionSearch(_ sender: UIButton) {
        self.view.endEditing(true)
        if locationId != "" || deviceId != "" {
            self.delegate?.didSelectFilter(location: locationId, type: deviceId)
        }
        self.dismiss(animated: true)
    }
    
    @IBAction func actionClearAll(_ sender: UIButton) {
        self.view.endEditing(true)
        self.deviceId = ""
        self.locationId = ""
        self.delegate?.didClearFilter()
        self.dismiss(animated: true)
    }
    
    @IBAction func actionLocation(_ sender: UIButton) {
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrLocation.map { $0.name } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblLocation.text = item
            self.locationId = self.arrLocation[index].id ?? ""
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
    
    @IBAction func actionDevice(_ sender: UIButton) {
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.dataSource = self.arrDeviceType.map { $0.name } as? [String] ?? []
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.deviceId = item
            self.lblDevice.text = item
        }
        self.setupDropDownAppearance()
        dropDown.show()
    }
}

//MARK:- API Call
@available(iOS 13.0, *)
extension DeviceFilterVC {
    
    func getLocationFromServer()  {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getLocation, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    self.parseLocation(arrTempData)
                }else if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else {
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func getDevicesFromServer()  {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.getDeviceTypes, andParam: [:], showHud: true) { (responseDict) in
            
            Helper.UI {
                if let arrTempData = responseDict["responseBody"] as? [[String: Any]]{
                    self.arrDeviceType.removeAll()
                    for dictData in arrTempData{
                        let objData = DeviceTypeModel.init(name: dictData["name"] as? String, longName: dictData["longName"] as? String, deviceDescription: dictData["description"] as? String, isSelected: false)
                        self.arrDeviceType.append(objData)
                    }
                }else  if let errorMessage = responseDict["errorMessage"] as? String {
                    Helper.sharedInstance.showToast(isError: false, title: errorMessage)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
}

