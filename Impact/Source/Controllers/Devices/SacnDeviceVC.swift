import UIKit

//protocol EditDeviceVCDelegate:AnyObject {
//    func dismiss(VC:String)
//}

@available(iOS 13.0, *)
class SacnDeviceVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var QRimage : UIImageView!
    @IBOutlet weak var arrowImage : UIImageView!
    @IBOutlet weak var Scanimage : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    // weak var delegate : EditDeviceVCDelegate?
    var device: DeviceModel!
    var nameVC: String = ""
    var uuidString: String = ""
    
    var param:[String:Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let modelName = UIDevice.modelName
//        if modelName == "iPhone 5" || modelName == "iPhone 6" || modelName == "iPhone 7" || modelName == "iPhone 8"{
//            QRimage.contentMode = UIView.ContentMode.scaleAspectFill
//        }
//        print(modelName)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let url = Bundle.main.url(forResource: "87", withExtension: "gif")
        DispatchQueue.main.async {
            self.arrowImage.sd_setImage(with: url!)
        }
        
        let urlScan = Bundle.main.url(forResource: "qr3", withExtension: "gif")
        DispatchQueue.main.async {
            self.Scanimage.sd_setImage(with: urlScan!)
        }
        
        if nameVC == "DevicesDetailsVC" {
            //Factory Reset
            self.getQRAuthCodeFromServer(purpose: "factoryreset", deviceId: device.id ?? "")
        }else{
            //Setup Device
            if self.param["li"] != nil {
                print(self.param["ldi"] as Any)
                let ldi = self.param["ldi"]
                self.getQRAuthCodeFromServer(purpose: "setup", deviceId: ldi as! String)
            }else{
                //Edit Device
                self.getQRAuthCodeFromServer(purpose: "edit", deviceId: device.id ?? "")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension SacnDeviceVC {
    
    @IBAction func actionReady(_ sender: UIButton){
        
        let objVC : ScanProcessingStep_1VC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.devices, viewControllerName: ScanProcessingStep_1VC.nameOfClass)
        objVC.device = device
        objVC.param = param
        objVC.nameVC = nameVC
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    
    @IBAction func action_Home(_ sender: UIButton){
//        if nameVC == "DevicesDetailsVC" {
//            Helper.sharedInstance.showToast(isError: false, title: "Click ‘Done’ to complete factory reset.")
//        }else{
            let objVC : HomeVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.home, viewControllerName: HomeVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: false)
        //}
    }
    
    @IBAction func actionNavigation_Back(_ sender: Any) {
//        if nameVC == "DevicesDetailsVC" {
//            Helper.sharedInstance.showToast(isError: false, title: "Click ‘Done’ to complete factory reset.")
//        }else{
            self.navigationController?.popViewController(animated: true)
       // }
        
    }
    
    @IBAction func actionNavigation_No(_ sender: Any) {
//        if nameVC == "DevicesDetailsVC" {
//            Helper.sharedInstance.showToast(isError: false, title: "Please scan the QR code.")
//        }else{
//            Helper.sharedInstance.showToast(isError: false, title: "Please scan the QR code.")
//        }
        Helper.sharedInstance.showToast(isError: false, title: "Please scan the QR code.")
    }
    
}


extension UIImageView {
    static func fromGif(frame: CGRect, resourceName: String) -> UIImageView? {
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else {
            print("Gif does not exist at that path")
            return nil
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
              let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        let gifImageView = UIImageView(frame: frame)
        gifImageView.animationImages = images
        return gifImageView
    }
}


//MARK:- API Call
@available(iOS 13.0, *)
extension SacnDeviceVC {
    
    func getQRAuthCodeFromServer(purpose:String, deviceId: String) {
        print("deviceId",deviceId)
        print("purpose",purpose)
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.QRAuthCode+purpose+"&deviceId=\(deviceId)", andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                print("responseDict",responseDict)
                var msg = ""
                if let errorMessage = responseDict["errorMessage"] as? String {
                    msg = errorMessage
                    Helper.sharedInstance.showToast(isError: false, title: msg)
                } else if let code = responseDict["responseBody"] {
                    print(code)
                    self.genretQRcode(code: code as? String, deviceId: deviceId)
                }else{
                    Helper.sharedInstance.showToast(isError: false, title: "Something went wrong")
                }
            }
        }
    }
    
    func genretQRcode(code:String?, deviceId:String = "") {
        
        if nameVC == "DevicesDetailsVC" {
            //Factory Reset
            lblTitle.text = "Scan QR code with smart Al camera.\nWait for device to reboot and Red light to turn on."
            DispatchQueue.main.async {
                let image = self.generateQRCode(from: "{\"a\":\"r\",\"t\":\"\(code ?? "")\",\"ldi\":\"\(deviceId)\"}")
                self.QRimage.image = image
            }
        }else{
            //Setup Device
            lblTitle.text = "Scan QR code with Smart AI camera and wait for Green light to turn on."
            let urlParams = param.compactMap({ (key, value) -> String in
                return "\(key):\(value)"
            }).joined(separator: ",")
            print(urlParams)
            
            if self.param["li"] != nil {
                DispatchQueue.main.async {
                    let dn = self.param["dn"]
                    let li = self.param["li"]
                    let ln = self.param["ln"]
                    let oi = self.param["oi"]
                    let ldi = self.param["ldi"]
                    let s = self.param["s"]
                    let p = self.param["p"]
                    
                    let image = self.generateQRCode(from: "{\"a\":\"s\",\"t\":\"\(code ?? "")\",\"dn\":\"\(dn ?? "")\",\"li\":\"\(li ?? "")\",\"ln\":\"\(ln ?? "")\",\"oi\":\"\(oi ?? "")\",\"ldi\":\"\(ldi ?? "")\",\"s\":\"\(s ?? "")\",\"p\":\"\(p ?? "")\"}" )
                    self.QRimage.image = image
                }
            }else{
                //Edit Device
                DispatchQueue.main.async {
                    let ldi = self.param["ldi"]
                    let s = self.param["s"]
                    let p = self.param["p"]
                    
                    let image = self.generateQRCode(from: "{\"a\":\"e\",\"t\":\"\(code ?? "")\",\"ldi\":\"\(ldi ?? "")\",\"s\":\"\(s ?? "")\",\"p\":\"\(p ?? "")\"}" )
                    self.QRimage.image = image
                }
            }
        }

        
    }
}
