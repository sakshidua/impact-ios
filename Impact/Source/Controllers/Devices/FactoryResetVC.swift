import UIKit

protocol FactoryResetVCDelegate:AnyObject {
    func dismiss()
}

@available(iOS 13.0, *)
class FactoryResetVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewBottom : UIView!
    weak var delegate : FactoryResetVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewBottom.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension FactoryResetVC {
    
    @IBAction func actionReady(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
        delegate?.dismiss()
    }
    
    @IBAction func actionClose(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismissController()
    }

}

