//
//  UITableViewHeaderFooterView.swift
//  Impact
//
//  Created by Hemraj on 08/07/21.
//

import UIKit

class ProductHeaderFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lineView: UIView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
