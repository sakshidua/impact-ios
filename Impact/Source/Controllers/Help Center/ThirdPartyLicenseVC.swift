//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit

@available(iOS 13.0, *)
class ThirdPartyLicenseVC: BaseVC {
    
    @IBOutlet var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setInitialData(){
        self.getThirdPartyLicenceFromServer()
    }
    
    func loadHTML(_ html:String) {
        txtView.attributedText = html.htmlToAttributedString
        let font = UIFont.init(name: "HoneywellSansWeb-Book", size: 18)
        txtView.font = font
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ThirdPartyLicenseVC {
    
    @IBAction func actionYes(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)
    }
}

//MARK:- API Call
@available(iOS 13.0, *)
extension ThirdPartyLicenseVC {
    
    func getThirdPartyLicenceFromServer() {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.thirdPartyLicence, andParam: [:], showHud: true) { (responseDict) in
            Helper.UI {
                if let html = responseDict["html"] as? String {
                    self.loadHTML(html)
                }
            }
        }
    }
}
