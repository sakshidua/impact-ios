//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit


@available(iOS 13.0, *)
class FindProVC: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setInitialData(){
       
    }

}

//MARK:- Action Method
@available(iOS 13.0, *)
extension FindProVC {
    
    @IBAction func actionYes(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func actionNo(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true)

    }
}



