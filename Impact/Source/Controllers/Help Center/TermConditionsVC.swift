//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit

@available(iOS 13.0, *)
class TermConditionsVC: BaseVC {
    
    @IBOutlet var txtView: UITextView!
    @IBOutlet var btnHome: UIButton!
    var vcName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setInitialData() {
        
        if(vcName == "Help") {
            self.btnHome.isHidden = false
        }else{
            self.btnHome.isHidden = true
        }
        
        self.termsAndConditionFromServer()
    }
    
    func loadHTML(_ html:String) {
        txtView.attributedText = html.htmlToAttributedString
        let font = UIFont.init(name: "HoneywellSansWeb-Book", size: 18)
        txtView.font = font
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension TermConditionsVC {
    
    @IBAction override func actionHome(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popToRootViewController(animated: false)
    }
}

//MARK:- API Call
@available(iOS 13.0, *)
extension TermConditionsVC {
    
    func termsAndConditionFromServer() {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.termsAndCondition, andParam: [:], showHud: true, changeBaseUrl: false) { (responseDict) in
            Helper.UI {
                if let html = responseDict["html"] as? String {
                    self.loadHTML(html)
                }
            }
        }
    }
}


