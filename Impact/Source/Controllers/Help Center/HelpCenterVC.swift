//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

@available(iOS 13.0, *)
class HelpCenterVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    var arrDeviceType = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        var dict = [String:Any]()
        dict.updateValue("Third Party License", forKey: "title")
        dict.updateValue("third_party_license", forKey: "icon")
        self.arrDeviceType.append(dict)
        
//        dict.updateValue("Find a Pro", forKey: "title")
//        dict.updateValue("smoke_detector", forKey: "icon")
//        self.arrDeviceType.append(dict)
        
        dict.updateValue("About Us", forKey: "title")
        dict.updateValue("about_us", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Term & Conditions", forKey: "title")
        dict.updateValue("terms", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Legal", forKey: "title")
        dict.updateValue("legal", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Products Tour", forKey: "title")
        dict.updateValue("product_tour", forKey: "icon")
        self.arrDeviceType.append(dict)
      
        self.tblDeviceType.reloadData()
        
    }


}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension HelpCenterVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeviceType.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrDeviceType[indexPath.row]
            
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
       
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        let obj = self.arrDeviceType[indexPath.row]
        
       if obj["title"] as? String ?? "" == "Third Party License" {
           let objVC : ThirdPartyLicenseVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: ThirdPartyLicenseVC.nameOfClass)
           self.navigationController?.pushViewController(objVC, animated: true)
           
       }else if obj["title"] as? String ?? "" == "Find a Pro" {
           
           let objVC : FindProVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: FindProVC.nameOfClass)
           self.navigationController?.pushViewController(objVC, animated: true)
          
       }else if obj["title"] as? String ?? "" == "About Us" {
           
           let objVC : AboutUsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: AboutUsVC.nameOfClass)
           self.navigationController?.pushViewController(objVC, animated: true)
           
       }else if obj["title"] as? String ?? "" == "Term & Conditions" {
           
           let objVC : TermConditionsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: TermConditionsVC.nameOfClass)
           objVC.vcName = "Help"
           self.navigationController?.pushViewController(objVC, animated: true)
           
       }else if obj["title"] as? String ?? "" == "Legal" {
        
        let objVC : LegalVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: LegalVC.nameOfClass)
        objVC.vcName = "Help"
        self.navigationController?.pushViewController(objVC, animated: true)
                
       }else if obj["title"] as? String ?? "" == "Products Tour" {
        
        let objVC : ProductsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: ProductsVC.nameOfClass)
        self.navigationController?.pushViewController(objVC, animated: true)
 
       }
            
    }
    
}



