//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

@available(iOS 13.0, *)
class ProductsDetailsVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    
    var arrDeviceType = [[String:Any]]()
    var isReload : Bool = false
    var section : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        
        let headerNib = UINib.init(nibName: "ProductHeaderFooterView", bundle: Bundle.main)
        tblDeviceType.register(headerNib, forHeaderFooterViewReuseIdentifier: "ProductHeaderFooterView")
        
        var dict = [String:Any]()
        dict.updateValue("Info and Specs", forKey: "title")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Features", forKey: "title")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("How To Videos", forKey: "title")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("FAQ", forKey: "title")
        self.arrDeviceType.append(dict)
        
        self.tblDeviceType.reloadData()
        
    }
}

//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension ProductsDetailsVC : UITableViewDelegate,UITableViewDataSource {
    
    //UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDeviceType.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isReload == true{
            if self.section == section {
                return 1
            }
            else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProductHeaderFooterView") as! ProductHeaderFooterView
        
        let obj = self.arrDeviceType[section]
        headerView.lblTitle.text = obj["title"] as? String ?? ""
        
        if self.section == section{
            headerView.lineView.isHidden = true
            headerView.img.image = UIImage.init(named: "minus")
        }else{
            headerView.lineView.isHidden = false
            headerView.img.image = UIImage.init(named: "plus")
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(headerTapped(_:))
        )
        
        headerView.tag = section
        headerView.addGestureRecognizer(tapGestureRecognizer)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "TableViewCell"
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else { return }
        if self.section != section{
            self.section = section
        }else{
            self.section = -1
        }
        
        isReload = true
        tblDeviceType.reloadData()
        
    }
    
}

