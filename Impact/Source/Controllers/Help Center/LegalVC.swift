//
//  WifiStepsVC.swift
//  Impact
//
//  Created by Apple on 05/05/21.
//

import UIKit

@available(iOS 13.0, *)
class LegalVC: BaseVC {
    
    var strEULA: String = ""
    var strPrivacy: String = ""
    var vcName = ""
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var cnsLineViewLeading: NSLayoutConstraint!
    @IBOutlet var btnHome: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

//MARK:- Instane Method
@available(iOS 13.0, *)
extension LegalVC {
    
    private func setInitialData(){
        if(vcName == "Help") {
            self.btnHome.isHidden = false
        }else{
            self.btnHome.isHidden = true
        }
        self.eulaFromServer()
    }
    
    private func loadHTML(_ html:String) {
        txtView.attributedText = html.htmlToAttributedString
        let font = UIFont.init(name: "HoneywellSansWeb-Book", size: 18)
        txtView.font = font
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension LegalVC {
    
    @IBAction override func actionHome(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func actionEULA(_ sender: UIButton) {
        self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)
        if self.strEULA == "" {
            self.eulaFromServer()
        } else {
            self.loadHTML(self.strEULA)
        }
    }
    
    @IBAction func actionPrivacy(_ sender: UIButton){
        self.animateLineView(view: self.lineView, onButton: sender, withConstraint: self.cnsLineViewLeading)

        if self.strPrivacy == "" {
            self.privacyPolicyFromServer()
        } else {
            self.loadHTML(self.strPrivacy)
        }
    }
}

//MARK:- API Call
@available(iOS 13.0, *)
extension LegalVC {
    
    private func eulaFromServer() {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.eula, andParam: [:], showHud: true, changeBaseUrl: false) { (responseDict) in
            Helper.UI {
                if let html = responseDict["html"] as? String {
                    self.strEULA = html
                    self.loadHTML(html)
                }
            }
        }
    }
    
    private func privacyPolicyFromServer() {
        APIManagerApp.shared.makeAPIRequest(ofType: GET, withEndPoint: APIEndPoint.privacyPolicy, andParam: [:], showHud: true, changeBaseUrl: false) { (responseDict) in
            Helper.UI {
                if let html = responseDict["html"] as? String {
                    self.strPrivacy = html
                    self.loadHTML(html)
                }
            }
        }
    }
}




