//
//  SetupDeviceVC.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

@available(iOS 13.0, *)
class ProductsVC: BaseVC, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var tblDeviceType : UITableView!
    var arrDeviceType = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitialData()
        // Do any additional setup after loading the view.
    }
    
    func setInitialData(){
        var dict = [String:Any]()
        dict.updateValue("Smart AI Camera", forKey: "title")
        dict.updateValue("video_cam", forKey: "icon")
        self.arrDeviceType.append(dict)
        
        dict.updateValue("Wifi Smoke Detector", forKey: "title")
        dict.updateValue("WSD_logo", forKey: "icon")
        self.arrDeviceType.append(dict)
        
//        dict.updateValue("InTym", forKey: "title")
//        dict.updateValue("smoke_detector", forKey: "icon")
//        self.arrDeviceType.append(dict)
      
        self.tblDeviceType.reloadData()
        
    }
}

//MARK:- Action Method
@available(iOS 13.0, *)
extension ProductsVC {

}



//MARK: - Tableview DataSource & Delegate Methods
@available(iOS 13.0, *)
extension ProductsVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeviceType.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "TableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TableViewCell{
            
            let obj = self.arrDeviceType[indexPath.row]
            
            cell.lblTitle.text = obj["title"] as? String ?? ""
            cell.imgIcon.image = UIImage.init(named: obj["icon"] as? String ?? "")
       
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let obj = self.arrDeviceType[indexPath.row]
        if obj["title"] as? String ?? "" == "Smart AI Camera" {
            let objVC : ProductsDetailsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: ProductsDetailsVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
            
        }else{
            let objVC : ProductsDetailsVC = Helper.sharedInstance.getViewController(storyboardName: AppConstant.Storyboard.helpCenter, viewControllerName: ProductsDetailsVC.nameOfClass)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
            
    }
    
}



