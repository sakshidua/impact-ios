//
//  TableViewCell.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblCount : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblDevice : UILabel!
    @IBOutlet weak var lblComment : UILabel!
    @IBOutlet weak var lblVideoUrl : UILabel!
    @IBOutlet weak var lblDateTime : UILabel!
    @IBOutlet weak var lblAlertType : UILabel!
    @IBOutlet weak var lblBookMarked : UILabel!
    @IBOutlet weak var lblDeviceName : UILabel!
    @IBOutlet weak var lblLocationName : UILabel!
    @IBOutlet weak var lblSubscription : UILabel!
    @IBOutlet weak var lblReplacementId : UILabel!
    
    @IBOutlet weak var btnImage : UIButton!
    @IBOutlet weak var btnShare : UIButton!
    @IBOutlet weak var btnReport : UIButton!
    @IBOutlet weak var btnSetect : UIButton!
    @IBOutlet weak var btnBookmark : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnDeviceName : UIButton!
    
    @IBOutlet weak var imgUrl : UIImageView!
    @IBOutlet weak var imgIcon : UIImageView!
    
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var lineView : UIView!
    @IBOutlet weak var viewOnline : UIView!
    
    // Account Cell
    @IBOutlet weak var imgIconSubscription : UIImageView!
    @IBOutlet weak var lblSubscriptionTitle : UILabel!
    @IBOutlet weak var lblPurchasedDate : UILabel!
    @IBOutlet weak var lblPlan : UILabel!
    @IBOutlet weak var lblFrequency : UILabel!
    @IBOutlet weak var switchOnOff : UISwitch!    
    
    // Location Cell
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var txtEdit : UITextField!
    @IBOutlet weak var btnRight : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


//acknowledged = 0;
//           alertId = "fb850aa7-c38b-4c2b-b501-0a1f70d99f5a";
//           alertType = Mask;
//           bookMarked = 0;
//           comment = "<null>";
//           device = TEV;
//           deviceId = "1d8fd440-9a7c-4e4f-b988-a0fffb077684";
//           deviceName = Ont93;
//           imageUrl = "https://tevstaging.blob.core.windows.net/violations/STAG_alerts/fb850aa7-c38b-4c2b-b501-0a1f70d99f5a.jpg?sv=2020-04-08&se=2021-05-05T21%3A14%3A14Z&sr=c&sp=r&sig=voY9eO73jV6iEIZT%2BUpxr%2BMbu3MJqvDFxuNabuHyO6Y%3D";
//           isCorrect = 1;
//           locationId = "42ef14ac-086d-408c-b284-fb3776f617ae";
//           locationName = Toronto;
//           occurenceTimeStamp = 1620206421;
//           smokeValue = "<null>";
//           videoUrl = "https://tevstaging.blob.core.windows.net/violations/STAG_alerts/fb850aa7-c38b-4c2b-b501-0a1f70d99f5a.mp4?sv=2020-04-08&se=2021-05-05T21%3A14%3A14Z&sr=c&sp=r&sig=voY9eO73jV6iEIZT%2BUpxr%2BMbu3MJqvDFxuNabuHyO6Y%3D";
//       }
