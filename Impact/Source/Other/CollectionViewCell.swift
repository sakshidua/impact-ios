//
//  CollectionViewCell.swift
//  Impact
//
//  Created by Apple on 03/05/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var btnplay: UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgDeviceType : UIImageView!
    
    @IBOutlet weak var viewBG : UIView!
    @IBOutlet weak var viewPlay : UIView!
    @IBOutlet weak var viewOnline : UIView!
    @IBOutlet weak var image : UIImageView!
    
    @IBOutlet weak var imgScan : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
