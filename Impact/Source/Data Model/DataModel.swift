//
//  DataModel.swift
//  Impact
//
//  Created by Apple on 07/05/21.
//

import Foundation


//MARK: -  Home viewModel
class HomeModel: NSObject {

    var alertId : String?
    var alertType : String?
    var imageUrl : String?
    var occurenceTimeStamp : Int?
    var deviceName : String?
    var deviceId : String?
    var locationName : String?
    var locationId : String?
    var acknowledged : Bool?
    var bookMarked : Bool?
    var isCorrect : Bool?
    var comment : String?
    var videoUrl : String?
    var device : String?
    var smokeValue : Int?
   
    
    init(alertId : String?,alertType: String?,imageUrl: String?,occurenceTimeStamp: Int?,deviceName: String?,deviceId: String?,locationName: String?,locationId: String?,acknowledged: Bool?,bookMarked: Bool,isCorrect: Bool,comment: String?,videoUrl: String?,device: String?,smokeValue: Int?) {
        
        if let alertId = alertId {
            self.alertId = alertId
        }
        
        if let alertType = alertType {
            self.alertType = alertType
        }
      
        if let imageUrl = imageUrl {
            self.imageUrl = imageUrl
        }
        
        if let occurenceTimeStamp = occurenceTimeStamp {
            self.occurenceTimeStamp = occurenceTimeStamp
        }
        
        if let deviceName = deviceName {
            self.deviceName = deviceName
        }
        
        if let deviceId = deviceId {
            self.deviceId = deviceId
        }
        
        if let locationName = locationName {
            self.locationName = locationName
        }
        
        if let locationId = locationId {
            self.locationId = locationId
        }
        
        if let acknowledged = acknowledged {
            self.acknowledged = acknowledged
        }
        
        self.acknowledged = acknowledged
        
        self.bookMarked = bookMarked
        
        self.isCorrect = isCorrect
        
        if let comment = comment {
            self.comment = comment
        }
        
        if let videoUrl = videoUrl {
            self.videoUrl = videoUrl
        }
        
        if let device = device {
            self.device = device
        }
        
        if let smokeValue = smokeValue {
            self.smokeValue = smokeValue
        }
    }
}

//MARK: -  AlertType viewModel
class AlertTypeModel: NSObject{
    var name : String?
    var id : Int?
    var isSelected : Bool?
    
    
    init(id: Int?, name: String?, isSelected:Bool?) {
        
        if let id = id {
            self.id = id
        }
        
        if let name = name {
            self.name = name
        }
        
        self.isSelected = isSelected
    }
}


//MARK: -  Location viewModel
class LocationModel: NSObject{
    var name : String?
    var id : String?
    var isSelected : Bool?
    
    
    init(id: String?, name: String?, isSelected:Bool?) {
        
        if let id = id {
            self.id = id
        }
        
        if let name = name {
            self.name = name
        }
      
        self.isSelected = isSelected
   }
}

//MARK: -  Device viewModel
class DeviceTypeModel: NSObject {
    var name : String?
    var longName : String?
    var deviceDescription : String?
    var isSelected : Bool?
    
    init(name: String?, longName: String?, deviceDescription: String?, isSelected:Bool?) {
        
        if let name = name {
            self.name = name
        }
        
        if let longName = longName {
            self.longName = longName
        }
        
        if let deviceDescription = deviceDescription {
            self.deviceDescription = deviceDescription
        }
        
        self.isSelected = isSelected
    }
}


//MARK: -  Device viewModel
class DeviceModel: NSObject {

    var id : String?
    var name : String?
    var connected : Bool?
    var locationId : String?
    var locationName : String?
    var macAddress : String?
    var firmwareVersion : String?
    var wifiName : String?
    var subscriptionId : String?
    var availableFeatures : [String]?
    var subscriptionExpiryDate : String?
    var currentUserPermission : String?
    var deviceType : String?
    var subscriptionStatus : String?
    var disabled : Bool?
    var unacknowledgedAlert: Int?
    
    var devicePermissions = [DevicePermissions]()
   
    
    init(id : String?,name: String?,connected: Bool?,locationId: String?,locationName: String?,macAddress: String?,firmwareVersion: String?,wifiName: String?,subscriptionId: String?,availableFeatures: [String]?,subscriptionExpiryDate: String?,currentUserPermission: String?,devicePermissions: [[String:Any]]?,deviceType: String?,subscriptionStatus: String?,disabled: Bool?, unacknowledgedAlert:Int?) {
        
        if let id = id {
            self.id = id
        }
        
        if let name = name {
            self.name = name
        }
      
        self.connected = connected
        
        if let locationId = locationId {
            self.locationId = locationId
        }
        
        if let locationName = locationName {
            self.locationName = locationName
        }
        
        if let macAddress = macAddress {
            self.macAddress = macAddress
        }
        
        if let firmwareVersion = firmwareVersion {
            self.firmwareVersion = firmwareVersion
        }
        
        if let wifiName = wifiName {
            self.wifiName = wifiName
        }
        
        if let subscriptionId = subscriptionId {
            self.subscriptionId = subscriptionId
        }
     
        if let availableFeatures = availableFeatures {
            self.availableFeatures = availableFeatures
        }
        
        if let subscriptionExpiryDate = subscriptionExpiryDate {
            self.subscriptionExpiryDate = subscriptionExpiryDate
        }
        
        if let currentUserPermission = currentUserPermission {
            self.currentUserPermission = currentUserPermission
        }
    
        if let deviceType = deviceType {
            self.deviceType = deviceType
        }
        
        if let subscriptionStatus = subscriptionStatus {
            self.subscriptionStatus = subscriptionStatus
        }
        
        if let unacknowledgedAlert = unacknowledgedAlert {
            self.unacknowledgedAlert = unacknowledgedAlert
        }
        
        for obj in devicePermissions ?? []{
            let objData = DevicePermissions.init(userEmail: obj["userEmail"] as? String, permission: obj["permission"] as? String)
            self.devicePermissions.append(objData)
        }
        
        self.disabled = disabled
    }
}

//MARK: -  Device Permissions viewModel
class DevicePermissions: NSObject {
    var userEmail : String?
    var permission : String?
    
    init(userEmail: String?, permission: String?) {
        
        if let userEmail = userEmail {
            self.userEmail = userEmail
        }
        
        if let permission = permission {
            self.permission = permission
        }
    }
}

//MARK: -  Device Replacement Model
class DeviceReplacementModel: NSObject {
    
    var email : String?
    var orgId : String?
    var comments : String?
    var deviceId : String?
    var replaceStatus : String?
    var deviceReplacementId : Int?
    
    init(email: String?, orgId: String?, comments: String?, deviceId: String?, replaceStatus: String?, deviceReplacementId: Int?) {
        
        if let email = email {
            self.email = email
        }
        
        if let orgId = orgId {
            self.orgId = orgId
        }
        
        if let comments = comments {
            self.comments = comments
        }
        
        if let deviceId = deviceId {
            self.deviceId = deviceId
        }
        
        if let replaceStatus = replaceStatus {
            self.replaceStatus = replaceStatus
        }
        
        if let deviceReplacementId = deviceReplacementId {
            self.deviceReplacementId = deviceReplacementId
        }
    }
}


//MARK: -  SignalR viewModel
class SignalRModel: NSObject {
    var IsError : String?
    var iHLSUrld : String?
    var ErrorMessage : String?
    var SuccessMessage : String?
    var LogicalDeviceId : String?
    var TerminateConnection : Bool?
    
    
    init(IsError: String?, iHLSUrld: String?, ErrorMessage: String?, SuccessMessage: String?, LogicalDeviceId: String?, TerminateConnection:Bool?) {
        
        if let IsError = IsError {
            self.IsError = IsError
        }
        
        if let iHLSUrld = iHLSUrld {
            self.iHLSUrld = iHLSUrld
        }
        
        if let ErrorMessage = ErrorMessage {
            self.iHLSUrld = ErrorMessage
        }
        
        if let SuccessMessage = SuccessMessage {
            self.iHLSUrld = SuccessMessage
        }
        
        if let LogicalDeviceId = LogicalDeviceId {
            self.LogicalDeviceId = LogicalDeviceId
        }
        
        self.TerminateConnection = TerminateConnection
    }
}

struct SignalRClientModel: Codable {
    var type: Int?
    let target: String?
    let arguments: [Argument]?
}

// MARK: - Argument
struct Argument: Codable {
    let hlsURL, errorMessage: String?
    let successMessage: String?
    let isError: Bool?
    let logicalDeviceID: String?
    let terminateConnection: Bool?

    enum CodingKeys: String, CodingKey {
        case hlsURL = "HLSUrl"
        case errorMessage = "ErrorMessage"
        case successMessage = "SuccessMessage"
        case isError = "IsError"
        case logicalDeviceID = "LogicalDeviceId"
        case terminateConnection = "TerminateConnection"
    }
}

// MARK: - Encode/decode helpers
class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}


//MARK: -  SubscriptionHistory viewModel
class SubscriptionHistoryModel: NSObject{
    var id : Int?
    var subscriptionId : String?
    var productName : String?
    var deviceId : String?
    var deviceName : String?
    var status : String?
    var createdTime : String?
    var eventType : String?
    var planCode : String?
    var planName : String?
    var planPrice : String?
    var descriptions : String?
    var subTotal : String?
    var amount : Int?
    var cgstName : String?
    var sgstName : String?
    var cgstAmount : String?
    var sgstAmount : String?
    var taxPercentage : String?
    var email : String?
    var invoiceId : Int?
    var interval : Int?
    var intervalUnit : String?
    var currency : String?
    var orgId : String?
    var nextBillingAt : String?
    var createdDate : Int?
    var createdBy : String?
    var modifiedDate : String?
    var modifiedBy : String?
    var successMessage : String?
    var errorMessage : String?
    var features = [Features]()
    
    init(id: Int?, subscriptionId: String?, productName: String?, deviceId: String?, deviceName: String?, status: String?, createdTime: String?, eventType: String?, planCode: String?, planName: String?, planPrice: String?, descriptions: String?, subTotal: String?, amount: Int?, cgstName: String?, sgstName: String?, cgstAmount: String?, taxPercentage: String?, email: String?, invoiceId: Int?, interval: Int?, intervalUnit: String?, currency: String?, orgId: String?, nextBillingAt: String?, createdDate: Int?, createdBy: String?, modifiedDate: String?, modifiedBy: String?, successMessage: String?, errorMessage: String?,features:[[String:Any]]?) {
        
        if let id = id {
            self.id = id
        }
        
        if let subscriptionId = subscriptionId {
            self.subscriptionId = subscriptionId
        }
        
        if let productName = productName {
            self.productName = productName
        }
        
        if let deviceId = deviceId {
            self.deviceId = deviceId
        }
        
        if let deviceName = deviceName {
            self.deviceName = deviceName
        }
        
        if let status = status {
            self.status = status
        }
        
        if let createdTime = createdTime {
            self.createdTime = createdTime
        }
        
        if let eventType = eventType {
            self.eventType = eventType
        }
        
        if let planCode = planCode {
            self.planCode = planCode
        }
        
        if let planName = planName {
            self.planName = planName
        }
        
        if let planPrice = planPrice {
            self.planPrice = planPrice
        }
        
        if let descriptions = descriptions {
            self.descriptions = descriptions
        }
        
        if let subTotal = subTotal {
            self.subTotal = subTotal
        }
        
        if let amount = amount {
            self.amount = amount
        }
        
        if let cgstName = cgstName {
            self.cgstName = cgstName
        }
        
        if let sgstName = sgstName {
            self.sgstName = sgstName
        }
        
        if let cgstAmount = cgstAmount {
            self.cgstAmount = cgstAmount
        }
        
        if let sgstAmount = sgstAmount {
            self.sgstAmount = sgstAmount
        }
        
        if let taxPercentage = taxPercentage {
            self.taxPercentage = taxPercentage
        }
        
        if let email = email {
            self.email = email
        }
        
        if let invoiceId = invoiceId {
            self.invoiceId = invoiceId
        }
        
        if let interval = interval {
            self.interval = interval
        }
        
        if let intervalUnit = intervalUnit {
            self.intervalUnit = intervalUnit
        }
        
        if let currency = currency {
            self.currency = currency
        }
        
        if let orgId = orgId {
            self.orgId = orgId
        }
        
        if let nextBillingAt = nextBillingAt {
            self.nextBillingAt = nextBillingAt
        }
        
        if let createdDate = createdDate {
            self.createdDate = createdDate
        }
        
        if let createdBy = createdBy {
            self.createdBy = createdBy
        }
        
        if let modifiedDate = modifiedDate {
            self.modifiedDate = modifiedDate
        }
        
        if let modifiedBy = modifiedBy {
            self.modifiedBy = modifiedBy
        }
        
        if let successMessage = successMessage {
            self.successMessage = successMessage
        }
        
        if let errorMessage = errorMessage {
            self.errorMessage = errorMessage
        }
        
        for obj in features ?? []{
            let objData = Features.init(code: obj["code"] as? String, name: obj["name"] as? String, price: obj["price"] as? String, createdDate: obj["createdDate"] as? String, createdBy: obj["createdBy"] as? String, modifiedDate: obj["modifiedDate"] as? String, modifiedBy: obj["modifiedBy"] as? String)
            self.features.append(objData)
        }
    }
}


//MARK: -  Device Permissions viewModel
class Features: NSObject {
    var code : String?
    var name : String?
    var price : String?
    var createdDate : String?
    var createdBy : String?
    var modifiedDate : String?
    var modifiedBy : String?
    
    
    init(code: String?, name: String?, price: String?, createdDate: String?, createdBy: String?, modifiedDate: String?, modifiedBy: String?) {
        
        if let code = code {
            self.code = code
        }
        
        if let name = name {
            self.name = name
        }
        
        if let price = price {
            self.price = price
        }
        
        if let createdDate = createdDate {
            self.createdDate = createdDate
        }
        
        if let createdBy = createdBy {
            self.createdBy = createdBy
        }
        
        if let modifiedDate = modifiedDate {
            self.modifiedDate = modifiedDate
        }
        
        if let modifiedBy = modifiedBy {
            self.modifiedBy = modifiedBy
        }
    }
}
