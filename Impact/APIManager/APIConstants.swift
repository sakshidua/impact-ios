//
//  APIConstants.swift
//  Impact
//
//  Created by Apple on 08/05/21.
//

import Foundation


let SERVICE_URL : String = "https://tev-api-staging.azurewebsites.net/api" // Development
let SERVICE_URL_ONBOARD = "https://mms-identity-staging.azurewebsites.net/api" //Development

struct APIEndPoint {
    // onBoard
    static let login : String = "/auth/token"
    static let forgotPassword = "/password/EmailResetLink"
    static let userDetails = "/user/UserDetails"
    static let NotificationDeviceSetup = "/Notification/deviceSetup"
    static let register = "/user/register"
    static let resendConfirmationEmail = "/user/resendConfirmationEmail?email="
    
    // home
    static let getAllAlerts : String = "/v1/Alerts"
    static let getAlertType : String = "/v1/Alerts/types?devices="
    static let getLocation : String = "/v1/Location"
    static let bookmarkAlert : String = "/v1/Alerts/bookmark/"
    static let reportIncorrect : String = "/v1/Alerts/reportIncorrect"
    static let getAcknowledge : String = "/v1/Alerts/acknowledge/"
    
    static func startVideoStraming(deviceId: String) -> String {
        return "/v1/Video/Start/\(deviceId)"
    }
    
    static func stopVideoStraming(deviceId: String, seqNumber:Int) -> String {
        return "/v1/Video/Stop/\(deviceId)/\(seqNumber)"
    }
    
    // devices
    static let getDevices : String = "/v1/Devices"
    static let getDevicesByLocation : String = "/v1/Devices/location"
    static let getDeviceTypes : String = "/v1/Devices/getAllDeviceTypes"
    static let unacknowledgedAlertsCount : String = "/v1/Devices/unacknowledgedAlertsCount"
    static let subscriptionDetail : String = "/v1/ZohoSubscription/SubscriptionDetail?subscriptionId="
    static let cancelSubscription : String = "/v1/ZohoSubscription/CancelSubscription"
    static let reactivateSubscription : String = "/v1/ZohoSubscription/ReactivateSubscription"
    static let firmwareUpdate : String = "/v1/Firmware/update/"
    static let deviceReplacement : String = "/v1/DeviceReplacement/getDeviceReplacementByDeviceId?deviceId="
    static let firmwareHistory : String = "/v1/Firmware/history/"
    static let deviceRestore : String = "/v1/DeviceReplacement/getDeviceReplacementByDeviceId?deviceId="
    static let createDeviceReplacement : String = "/v1/DeviceReplacement/createDeviceReplacement"
    static let getDeviceConfiguration : String = "/v1/Devices/GetDeviceConfiguration/"
    static let updateDeviceConfiguration : String = "/v1/Devices/UpdateDeviceConfiguration/"
    static let updateDeviceNameOrLocation : String = "/v1/Devices/UpdateDeviceNameOrLocation/"
    static let setResetZoneFencing : String = "/v1/Devices/SetResetZoneFencing/"
    static let alertsDelete : String = "/v1/Alerts/Delete/"
    static let addDevice : String = "/v1/Devices/addDevice/"
    static let deviceSetupStatus : String = "/Notification/deviceSetupStatus/"
    static let QRAuthCode : String = "/v1/Devices/QRAuthCode?purpose="
    
    
    
    
    //Account
    static let updateProfile : String = "/user/UpdateProfile"
    static let passwordChange : String = "/password/Change"
    static let locationUpdate : String = "/v1/Location/update"
    static let locationAdd : String = "/v1/Location/add"
    static let subscriptionHistory : String = "/v1/ZohoSubscription/SubscriptionHistory"
    static let notificationUnsubscribe : String = "/Notification/unsubscribe"
    static let uploadProfilePicture : String = "/user/UploadProfilePicture"
    static let downloadInvoice : String = "/v1/ZohoSubscription/DownloadInvoice/"
    static let subscribe : String = "/Notification/subscribe"
    static let unsubscribe : String = "/Notification/unsubscribe"
    
    
    static func deleteDevice(deviceId: String, hardFactoryReset:Bool) -> String {
        return "/v1/Devices/DeleteDevice/\(deviceId)?hardFactoryReset=\(hardFactoryReset)"
    }
    
    
    // Help Center
    static let eula : String = "/v1/Static/EULA"
    static let aboutUs : String = "/v1/Static/aboutUs"
    static let privacyPolicy : String = "/v1/Static/PrivacyPolicy"
    static let thirdPartyLicence : String = "/v1/ThirdPartyLicence"
    static let termsAndCondition : String = "/v1/Static/TermsAndCondition"
}



struct AppConstants {
    
    static let NOT_EMPTY : String = "NOT_EMPTY"
    static let OPEN_CONNECTION : String = "openConnection"
    static let GET_CIPHER : String = "get_cipher"
    static let DECIPHER_LENGTH : String = "decipher_length"
    static let DECIPHER_MESSAGE : String = "decipher_message"
    static let NETWORK_CREDENTIALS : String = "network_credentials"
    static let SSID_LENGTH : String = "ssid_length"
    static let SSID : String = "ssid" // Wifi ssid
    static let PSK_LENGTH : String = "psk_length"
    static let PSK : String = "psk" // wifi password
    //static let EMAIL_ID_LENGTH : String = "email_id_length"
    static let DEVICE_NAME_LENGTH : String = "device_name_length"
    static let DEVICE_LOCATION_LENGTH : String = "device_location_length"
    static let DEVICE_TYPE_LENGTH : String = "device_type_length"//tev type
    static let ORG_ID_LENGTH : String = "org_id_length"
    static let LOGICAL_DEVICE_ID_LENGTH : String = "logical_device_id_length"
    static let ORG_ID : String = "ORG_ID"// login resopnce
    static let DEVICE_LOCATION_NAME_LENGTH : String = "device_location_name_length"
    // static let EMAIL_ID : String = "EMAIL_ID" // login
    static let DEVICE_NAME : String = "DEVICE_NAME" // manually device
    static let DEVICE_LOCATION : String = "DEVICE_LOCATION"// choose filed id
    static let DEVICE_LOCATION_NAME : String = "DEVICE_LOCATION_NAME"// location name
    static let LOGICAL_DEVICE_ID : String = "LOGICAL_DEVICE_ID"// api call
    static let RECEIVED_CIPHER : String = "received_cipher" // socket responce
    static let VERIFY_DECIPHER : String = "verify_decipher"
    static let DEVICE_TYPE : String = "DEVICE_TYPE"//tev type
    static let TEV : String = "TEV"
    static let WIRELESS : String = "WSD"
    static let WIRELESS1 : String = "WSD"
    
}
